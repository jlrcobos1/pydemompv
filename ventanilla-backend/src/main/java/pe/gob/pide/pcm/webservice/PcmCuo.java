/**
 * PcmCuo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pe.gob.pide.pcm.webservice;

public interface PcmCuo extends javax.xml.rpc.Service {
    public java.lang.String getPcmCuoHttpsSoap11EndpointAddress();

    public pe.gob.pide.pcm.webservice.PcmCuoPortType getPcmCuoHttpsSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public pe.gob.pide.pcm.webservice.PcmCuoPortType getPcmCuoHttpsSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
