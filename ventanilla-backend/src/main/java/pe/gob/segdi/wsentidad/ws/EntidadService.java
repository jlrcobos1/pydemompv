/**
 * EntidadService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pe.gob.segdi.wsentidad.ws;

public interface EntidadService extends javax.xml.rpc.Service {
    public java.lang.String getEntidadPortAddress();

    public pe.gob.segdi.wsentidad.ws.Entidad getEntidadPort() throws javax.xml.rpc.ServiceException;

    public pe.gob.segdi.wsentidad.ws.Entidad getEntidadPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
