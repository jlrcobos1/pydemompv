package pe.gob.pcm.sweb.pide.service;

import pe.gob.segdi.wsiopidetramite.ws.RecepcionTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaTramite;

public interface PideService {

	String getCuo(String ruc);

	RespuestaTramite despachar(RecepcionTramite recepcionTramite);
}
