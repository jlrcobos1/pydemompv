package pe.gob.pcm.sweb.core.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Pageable;

import pe.gob.pcm.domain.core.Recepcion;
import pe.gob.pcm.model.recepcion.RecepcionFiltroRequest;
import pe.gob.pcm.model.recepcion.RecepcionRequest;

public interface RecepcionService {

	List<Recepcion> find(RecepcionFiltroRequest request, Pageable pageable);

	Recepcion get(BigDecimal recepcionId);

	void delete(BigDecimal recepcionId);

	void saveOrUpdate(RecepcionRequest recepcion);

}
