package pe.gob.pcm.sweb.core.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.CollectionResponse;
import pe.gob.pcm.model.despacho.DespachoComboResponse;
import pe.gob.pcm.model.despacho.DespachoFiltroRequest;
import pe.gob.pcm.model.despacho.DespachoRequest;
import pe.gob.pcm.model.despacho.DespachoResponse;
import pe.gob.pcm.sweb.core.facade.DespachoFacade;

@CrossOrigin(origins = "*", maxAge = 360)
@RestController
@RequestMapping("/api/despacho")
public class DespachoRestController {

	@Autowired
	private DespachoFacade despachoFacade;

	@CrossOrigin("*")
	@PostMapping("/find")
	public CollectionResponse<DespachoResponse> find(@RequestBody DespachoFiltroRequest request) {
		return despachoFacade.find(request);
	}

	@CrossOrigin("*")
	@PostMapping("/saveOrUpdate")
	public BaseOperacionResponse saveOrUpdate(@RequestBody DespachoRequest request) {
		return despachoFacade.saveOrUpdate(request);
	}

	@CrossOrigin("*")
	@DeleteMapping("/delete/{despachoId}")
	public BaseOperacionResponse delete(@PathVariable BigDecimal despachoId) {
		return despachoFacade.delete(despachoId);
	}

	@CrossOrigin("*")
	@GetMapping("/get/{despachoId}")
	public DespachoResponse get(@PathVariable BigDecimal despachoId) {
		return despachoFacade.get(despachoId);
	}

	@CrossOrigin("*")
	@GetMapping("/init")
	public DespachoComboResponse init() {
		return despachoFacade.init();
	}

	@CrossOrigin("*")
	@GetMapping("/initForm")
	public DespachoResponse initForm() {
		return despachoFacade.initForm();
	}
	
	//para sincronizar SITD - BD PIDE
	@CrossOrigin("*")
	@GetMapping("/sync")
	public BaseOperacionResponse sync() {
		return despachoFacade.sync();
	}
	@CrossOrigin("*")
	@GetMapping("/send/{documentoId}")
	public BaseOperacionResponse send(@PathVariable BigDecimal documentoId) {
		return despachoFacade.send(documentoId);
	}

}
