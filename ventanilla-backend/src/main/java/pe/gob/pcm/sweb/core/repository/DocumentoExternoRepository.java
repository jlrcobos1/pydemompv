package pe.gob.pcm.sweb.core.repository;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import pe.gob.pcm.domain.core.DocumentoExterno;

public interface DocumentoExternoRepository
		extends JpaRepository<DocumentoExterno, BigDecimal>, JpaSpecificationExecutor<DocumentoExterno> {

	@Query("SELECT t FROM DocumentoExterno t WHERE t.sidemiext =:sidemiext ")
	Optional<DocumentoExterno> getByDespacho(BigDecimal sidemiext);

	@Query("SELECT t FROM DocumentoExterno t WHERE t.vnumdoc =:vnumdoc and t.ccodtipdoc =:tipoDocumentoCodigo  ")
	Optional<DocumentoExterno> validar(String vnumdoc, String tipoDocumentoCodigo);

}
