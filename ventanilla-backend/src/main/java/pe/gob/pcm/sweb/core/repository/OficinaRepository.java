package pe.gob.pcm.sweb.core.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import pe.gob.pcm.domain.std.Oficina;

public interface OficinaRepository extends JpaRepository<Oficina, BigDecimal>, JpaSpecificationExecutor<Oficina> {

	 
}
