package pe.gob.pcm.sweb.core.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import pe.gob.pcm.domain.std.TramiteDigital;

public interface TramiteDigitalRepository extends JpaRepository<TramiteDigital, BigDecimal>, JpaSpecificationExecutor<TramiteDigital> {

	@Query("SELECT t FROM TramiteDigital t where t.tramiteId =:tramiteId")
	List<TramiteDigital> findTramite(BigDecimal tramiteId);

	 
}
