package pe.gob.pcm.sweb.core.service.impl;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import lombok.extern.slf4j.Slf4j;
import pe.gob.pcm.core.exception.InternalException;
import pe.gob.pcm.core.service.ServiceBase;
import pe.gob.pcm.domain.core.Anexo;
import pe.gob.pcm.domain.core.Despacho;
import pe.gob.pcm.domain.core.DocumentoExterno;
import pe.gob.pcm.domain.core.DocumentoPrincipal;
import pe.gob.pcm.domain.std.Oficina;
import pe.gob.pcm.domain.std.Remitente;
import pe.gob.pcm.domain.std.Trabajador;
import pe.gob.pcm.domain.std.Tramite;
import pe.gob.pcm.domain.std.TramiteDigital;
import pe.gob.pcm.helper.Util;
import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.despacho.DespachoFiltroRequest;
import pe.gob.pcm.model.despacho.DespachoRequest;
import pe.gob.pcm.model.despacho.DespachoSyncRequest;
import pe.gob.pcm.model.visor.VisorRequest;
import pe.gob.pcm.sweb.core.repository.AnexoRepository;
import pe.gob.pcm.sweb.core.repository.DespachoRepository;
import pe.gob.pcm.sweb.core.repository.DocumentoExternoRepository;
import pe.gob.pcm.sweb.core.repository.DocumentoPrincipalRepository;
import pe.gob.pcm.sweb.core.repository.OficinaRepository;
import pe.gob.pcm.sweb.core.repository.RemitenteRepository;
import pe.gob.pcm.sweb.core.repository.TrabajadorRepository;
import pe.gob.pcm.sweb.core.repository.TramiteDigitalRepository;
import pe.gob.pcm.sweb.core.repository.TramiteRepository;
import pe.gob.pcm.sweb.core.service.DespachoService;
import pe.gob.pcm.sweb.pide.service.PideService;
import pe.gob.pcm.util.Constantes;
import pe.gob.pcm.util.DateUtil;
import pe.gob.pcm.util.GenericUtil;
import pe.gob.segdi.wsiopidetramite.ws.DocumentoAnexo;
import pe.gob.segdi.wsiopidetramite.ws.RecepcionTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaTramite;

@Service
@Slf4j
public class DespachoServiceImpl  extends ServiceBase implements DespachoService {

	private  static final String WS_IOPIDE_TRAMITE_RESPONSE_OK = "0000";
	
	@Value("${url.consulta.documento}")
	private String urlConsultaDocumento;
	
	
	@Value("${path.STD.ALMACEN}")
	private String path_STD_ALMACEN;
	
	@Value("${path.documento.std}")
	private String url_documento_std;
	
	
	
	@Value("${ruc.pcm.osinfor}")
	private String rucOsinfor;
 
	@Value("${rz.pcm.osinfor}")
	private String razonSocialOsinfor;
	
	@Autowired
	private PideService pideService;
	
	
	@Autowired
	private DespachoRepository despachoRepository;
	@Autowired
	private DocumentoExternoRepository documentoExternoRepository;
	@Autowired
	private DocumentoPrincipalRepository documentoPrincipalRepository;
	@Autowired
	private AnexoRepository anexoRepository;
	
	@Autowired
	private TramiteRepository tramiteRepository;
	@Autowired
	private TramiteDigitalRepository tramiteDigitalRepository;
	@Autowired
	private RemitenteRepository remitenteRepository;
	@Autowired
	private OficinaRepository oficinaRepository;
	@Autowired
	private TrabajadorRepository trabajadorRepository;
	
	 
	@Override
	public List<Despacho> find(DespachoFiltroRequest t,Pageable pageable) {
		Page<Despacho> page = despachoRepository.findAll(new Specification<Despacho>() {
			private static final long serialVersionUID = 2502949831020987898L;
			@Override
			public javax.persistence.criteria.Predicate toPredicate(Root<Despacho> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (!GenericUtil.isEmptyWithTrim(t.getCuo())) {
					predicates.add(criteriaBuilder .and(criteriaBuilder.like(root.get("cuo"), "%".concat(t.getCuo()).concat("%"))));
				} 
				 
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		}, pageable);
		t.setTotalCount(page.getTotalElements());
		return page.getContent();
		 
	}

	@Override
	public Despacho get(BigDecimal despachoId) {

		Optional<Despacho> optional = despachoRepository.findById(despachoId);
		if (optional.isPresent()) {
			return optional.get();
		}
		throw new InternalException("El documento no se encuentra registrado en Despacho");

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
	public void delete(BigDecimal despachoId) {
		Despacho despacho = this.get(despachoId);
		despachoRepository.save(despacho);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
	public void saveOrUpdate(DespachoRequest t) {
		log.info("::::::::::::::::::saveOrUpdate:::");
		if (GenericUtil.isNotEmpty(t.getSidemiext()) && t.getSidemiext().intValue() > 0) {
			Despacho despacho = this.get(t.getSidemiext());
			despachoRepository.save(despacho);
		} else {
			Despacho despacho = modelMapper.map(t, Despacho.class);
			despachoRepository.save(despacho);
		}
	}
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED,  rollbackFor = Exception.class)
	public void sync() {
		try {		
			
			
			DespachoSyncRequest request=new DespachoSyncRequest();
			request.setHasta(DateUtil.addDaysDate(DateUtil.getCurrentLocalDateTime(), 2).toLocalDate());
			request.setDesde(DateUtil.addDaysDate(LocalDateTime.now(), -7).toLocalDate());
			List<Tramite> tramites=	tramiteRepository.findAll(new Specification<Tramite>() {
				private static final long serialVersionUID = 2502949831020987898L;
				@Override
				public javax.persistence.criteria.Predicate toPredicate(Root<Tramite> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					predicates.add(criteriaBuilder .and(criteriaBuilder.greaterThanOrEqualTo(root.get("fechaDocumento"), request.getDesde())));
					predicates.add(criteriaBuilder .and(criteriaBuilder.lessThanOrEqualTo(root.get("fechaDocumento"), request.getHasta())));
					predicates.add(criteriaBuilder .and(criteriaBuilder.ge(root.get("remitenteId"), BigDecimal.ZERO)));
					predicates.add(criteriaBuilder .and(criteriaBuilder.ge(root.get("flagEnvioPide"), BigDecimal.ONE)));
					predicates.add(criteriaBuilder .and(criteriaBuilder.isNotNull(root.get("codificacion"))));
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			},Sort.by(Direction.DESC, "tramiteId"));
			if(tramites.isEmpty()) {
				throw new InternalException("No se ha encontrados documentos en los ultimos 7 días");
			}
			for (Tramite tt : tramites) {
			Optional<Despacho> optional =	despachoRepository.validar(tt.getTramiteId().toString());
			if(!optional.isPresent()) {
				Despacho despacho=new Despacho();
				despacho.setVnumregstd(tt.getTramiteId().toString());
				despacho.setVanioregstd(DateUtil.getCurrentYear().toString());
				Optional<Trabajador> trabajador= trabajadorRepository.findById(tt.getTrabajadorId());
				if(!trabajador.isPresent()) {
					StringBuilder builder=new StringBuilder();
					builder.append("El documento :");
					builder.append(GenericUtil.emptyIfStringNull(tt.getCodificacion()));
					builder.append(" no cuenta con trabajador Registrado.");
					throw new InternalException(builder.toString());
				}
				despacho.setCtipdociderem(trabajador.get().getTipoDocumentoId().toString());//TIPO DE DOCUMENTO 1 DNI
				despacho.setVnumdociderem(trabajador.get().getNumeroDocumento());//NUMERO DOCUMENTO DEL USUARIO QUE DESPCHA EL DOCUMENTO
				Optional<Oficina> oficina= oficinaRepository.findById(tt.getOficinaId());
				if(!oficina.isPresent()) {
					StringBuilder builder=new StringBuilder();
					builder.append("El documento :");
					builder.append(GenericUtil.emptyIfStringNull(tt.getCodificacion()));
					builder.append(" no cuenta con oficina.");
					throw new InternalException(builder.toString());
				}
				despacho.setVcoduniorgrem(oficina.get().getOficinaId().toString());//ID UNIDAD ORIGEN
				despacho.setVuniorgrem(oficina.get().getNombre());//ID UNIDAD ORIGEN 
				Optional<Remitente> remitente= remitenteRepository.findById(tt.getRemitenteId());
				if(!remitente.isPresent()) {
					StringBuilder builder=new StringBuilder();
					builder.append("El documento :");
					builder.append(GenericUtil.emptyIfStringNull(tt.getCodificacion()));
					builder.append(" no cuenta con Remitente.");
					throw new InternalException(builder.toString());
				}
				despacho.setVnomentrec(remitente.get().getNombre());
				despacho.setVrucentrec(remitente.get().getNumeroDocumento());
				despacho.setCflgest("P");
				despacho.setCflgenv("N");
				despacho.setVusureg("RMARCA");
				despacho.setDfecreg(DateUtil.getCurrentLocalDateTime());
				
				despachoRepository.save(despacho);
				DocumentoExterno documentoExterno=new DocumentoExterno();
				documentoExterno.setSidemiext(despacho.getSidemiext());
				documentoExterno.setVnomentemi(remitente.get().getNombre());
				//documentoExterno.setCcodtipdoc("01");//POR
				documentoExterno.setCcodtipdoc(Util.getTipoDocumentoPIDE(tt.getTipoDocumentoCodigo().trim()));
				documentoExterno.setVnumdoc(GenericUtil.emptyIfStringNull(tt.getCodificacion()));
				documentoExterno.setDfecdoc(DateUtil.getCurrentLocalDateTime());
				documentoExterno.setVuniorgdst(remitente.get().getNombre());
				documentoExterno.setVnomdst(tt.getRemitente());//nombre del directivo revisar(esta guardando nullo)
				documentoExterno.setVnomcardst("Jefe de Oficina");//NO PERMITE NULO
				documentoExterno.setVasu(tt.getAsunto());
				documentoExterno.setCindtup("N");//??
				documentoExterno.setSnumanx(BigDecimal.ONE);//actualizad con anexos
				documentoExterno.setSnumfol(BigDecimal.ONE);//Actualizad con Anexos
				documentoExterno.setVurldocanx(urlConsultaDocumento);
				documentoExternoRepository.save(documentoExterno);
				List<TramiteDigital> tramiteDigitales= tramiteDigitalRepository.findTramite(tt.getTramiteId());
				if(tramiteDigitales.isEmpty()) {
					StringBuilder builder=new StringBuilder();
					builder.append("El documento :");
					builder.append(GenericUtil.emptyIfStringNull(tt.getCodificacion()));
					builder.append(" no cuenta con Documento Principal (Adjunte el Archivo en formato PDF).");
					throw new InternalException(builder.toString());
				}
				DocumentoPrincipal documentoPrincipal=new DocumentoPrincipal();
				documentoPrincipal.setSiddocext(documentoExterno.getSiddocext());
				documentoPrincipal.setCcodest("A");
				byte[] buffer = new byte[1000000]; //10 MB -10485760
				try {
					// String url = "smb://10.10.8.136/web/SITDM/cAlmacenArchivos/OFICIO-D00035-OFICIO-OSINFOR-05.1-SALIDA.pdf";
					  String url = "smb://10.10.8.136/web/SITDM/cAlmacenArchivos/"+tramiteDigitales.get(0).getNombreNuevo().trim();
					    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, "OSINFOR\\locadoroti10", "*2020*jlr");
					    SmbFile file = new SmbFile(url, auth);
					    try (SmbFileInputStream in = new SmbFileInputStream(file)) {
					        int bytesRead = 0;
					        do {
					            bytesRead = in.read(buffer);
					            // here you have "bytesRead" in buffer array
					        } 
					        while (bytesRead > 0);
					    }
				} catch(Exception e) {
					throw new InternalException("No se ha encontrado el archivo"+tramiteDigitales.get(0).getNombreNuevo().trim());
				}
				/*
				File file=new File(pathStd.concat(tramiteDigitales.get(0).getNombreNuevo().trim()));				
				if(!file.exists()) {
					StringBuilder builder=new StringBuilder();
					builder.append("El documento :");
					builder.append(GenericUtil.emptyIfStringNull(tt.getCodificacion()));
					builder.append(" no tiene Archivo en el directorio compartido.");	
					throw new InternalException(builder.toString());
				}
				InputStream inputStream=new FileInputStream(file);
				documentoPrincipal.setBpdfdoc(IOUtils.toByteArray(inputStream));*/
				documentoPrincipal.setBpdfdoc(buffer);
				
				documentoPrincipal.setVnomdoc(tramiteDigitales.get(0).getNombreNuevo());				
				documentoPrincipal.setDfecreg(DateUtil.getCurrentLocalDateTime());
				documentoPrincipalRepository.save(documentoPrincipal);
			}
			}
			
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public BaseOperacionResponse send(BigDecimal despachoId) {
		String errorEnvio=""; 
		try {
			 	Despacho despacho =   this.get(despachoId);
			 	
			 	Optional<DocumentoExterno> externo  =documentoExternoRepository.getByDespacho(despacho.getSidemiext());
			 	if(!externo.isPresent()) {
			 		StringBuilder builder=new StringBuilder();
			 		builder.append("El documento");
			 		builder.append(despacho.getVnumdociderem());
			 		builder.append(" debe registrar el documento a emitirse");
			 		throw new InternalException(builder.toString());
			 	}
			 	
			 	Optional<DocumentoPrincipal> principal=documentoPrincipalRepository.getByExterno(externo.get().getSiddocext());
			 	if(!principal.isPresent()) {
			 		StringBuilder builder=new StringBuilder();
			 		builder.append("El documento");
			 		builder.append(externo.get());
			 		builder.append(" no cuenta con el archivo principal a remitirse");
			 		throw new InternalException(builder.toString());
			 	}
			 	
			 	List<Anexo> anexos=anexoRepository.findByExterno(externo.get().getSiddocext());
				 
				String cuo = pideService.getCuo(rucOsinfor);
				if(GenericUtil.isEmptyWithTrim(cuo)) {
					throw new InternalException("No se ha generado el CUO");
				}
				RecepcionTramite recepcionTramite = new RecepcionTramite();
				recepcionTramite.setVrucentrem(rucOsinfor);
				recepcionTramite.setVrucentrec(GenericUtil.emptyIfStringNull(despacho.getVrucentrec()));
				recepcionTramite.setVnomentemi(razonSocialOsinfor);
				recepcionTramite.setCcodtipdoc(GenericUtil.emptyIfStringNull(externo.get().getCcodtipdoc()));
				recepcionTramite.setVnumdoc(GenericUtil.emptyIfStringNull(externo.get().getVnumdoc()));
				recepcionTramite.setCtipdociderem(GenericUtil.emptyIfStringNull(despacho.getCtipdociderem()));
				recepcionTramite.setVnumdociderem(GenericUtil.emptyIfStringNull(despacho.getVnumdociderem()));
				recepcionTramite.setDfecdoc(DateUtil.convertToXMLGregorianCalendar(despacho.getDfecreg()));
				recepcionTramite.setVuniorgrem(GenericUtil.emptyIfStringNull(despacho.getVuniorgrem()));
				
				recepcionTramite.setVuniorgdst(GenericUtil.emptyIfStringNull(externo.get().getVuniorgdst()));
				recepcionTramite.setVnomdst(GenericUtil.emptyIfStringNull(externo.get().getVnomdst()));
				recepcionTramite.setVnomcardst(GenericUtil.emptyIfStringNull(externo.get().getVnomcardst()));
				
				recepcionTramite.setVasu(GenericUtil.emptyIfStringNull(externo.get().getVasu()));
				recepcionTramite.setBpdfdoc(GenericUtil.toBase64(principal.get().getBpdfdoc()));
				recepcionTramite.setVnomdoc(GenericUtil.emptyIfStringNull(principal.get().getVnomdoc()));
				recepcionTramite.setSnumfol(externo.get().getSnumfol().intValue());
				recepcionTramite.setSnumanx(BigDecimal.ZERO.intValue());
				if(!anexos.isEmpty()) {
					recepcionTramite.setSnumanx(anexos.size());
					anexos.forEach(tt->{
						DocumentoAnexo documentoAnexo = new DocumentoAnexo();
						documentoAnexo.setVnomdoc(GenericUtil.emptyIfStringNull(tt.getVnomdoc()));
						recepcionTramite.getLstanexos().add(documentoAnexo);
					});
				}
				recepcionTramite.setVurldocanx(urlConsultaDocumento);
				recepcionTramite.setVcuo(cuo);
				recepcionTramite.setVcuoref(GenericUtil.emptyIfStringNull(despacho.getVcuoref()));
				log.info(new Gson().toJson(recepcionTramite));
				//enviando a la PIDE
				RespuestaTramite result=pideService.despachar(recepcionTramite);
				
				if (WS_IOPIDE_TRAMITE_RESPONSE_OK.equals(result.getVcodres())) {
					log.info("Registro correcto en la PIDE"+result.getVcodres());
					despacho.setVcuo(cuo);
					despacho.setDfecenv(DateUtil.getCurrentLocalDateTime());
					despacho.setCflgest("E");
					despachoRepository.save(despacho);
					//revisar 
					return new BaseOperacionResponse(Constantes.SUCCESS, result.getVdesres());
				}else {
					errorEnvio=result.getVdesres();
					throw new InternalException(result.getVdesres());
				}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		// return new BaseOperacionResponse(Constantes.ERROR, "Se ha producido un error al momento de enviar");
		 return new BaseOperacionResponse(Constantes.ERROR, errorEnvio);
	}
	
	@Override
	public DocumentoExterno validar(VisorRequest t) {
		Optional<DocumentoExterno> optional= documentoExternoRepository.validar(t.getNumeroDocumento(),t.getTipoDocumentoCodigo());
		if(optional.isPresent()) {
			return optional.get();
		}
		 throw new InternalException("No se ha encotrado el documento");
	}
	
	@Override
	public List<Anexo> loadAnexo(BigDecimal siddocext) {
		return anexoRepository.findByExterno(siddocext);
	}

	@Override
	public DocumentoPrincipal getDocumentoPrincipal(BigDecimal siddocext) {
		Optional<DocumentoPrincipal> optional=  documentoPrincipalRepository.getByExterno(siddocext);
		if(optional.isPresent()) {
			return optional.get();
		}
		throw new InternalException("El documento no cuenta con Adjunto");
	}
}
