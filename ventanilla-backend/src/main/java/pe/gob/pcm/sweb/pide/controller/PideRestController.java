package pe.gob.pcm.sweb.pide.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.pide.PideRequest;
import pe.gob.pcm.sweb.pide.facade.PideFacade;

@RestController
@RequestMapping("/api/pide")
public class PideRestController {

	@Autowired
	private PideFacade pideFacade;

	@CrossOrigin(origins = "*")
	@GetMapping("/validar/{ruc}")
	public BaseOperacionResponse validar(@PathVariable String ruc) {
		return pideFacade.validar(ruc);
	}
	

	@CrossOrigin(origins = "*")
	@PostMapping("/validarRuc")
	public BaseOperacionResponse validarRuc(@RequestBody PideRequest request) {
		return pideFacade.validarRuc(request);
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/getCuo/{ruc}")
	public BaseOperacionResponse getCuo(@PathVariable String ruc) {
		return pideFacade.getCuo(ruc);
	}
	
}
