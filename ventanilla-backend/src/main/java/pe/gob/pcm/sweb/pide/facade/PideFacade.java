package pe.gob.pcm.sweb.pide.facade;

import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.pide.PideRequest;

public interface PideFacade {

	BaseOperacionResponse validar(String ruc);

	BaseOperacionResponse validarRuc(PideRequest request);

	BaseOperacionResponse getCuo(String ruc);


}
