package pe.gob.pcm.sweb.core.facade.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import pe.gob.pcm.core.exception.InternalException;
import pe.gob.pcm.core.facade.FacadeBase;
import pe.gob.pcm.domain.core.Anexo;
import pe.gob.pcm.domain.core.DocumentoExterno;
import pe.gob.pcm.domain.core.DocumentoPrincipal;
import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.despacho.AnexoResponse;
import pe.gob.pcm.model.visor.FileDto;
import pe.gob.pcm.model.visor.VisorRequest;
import pe.gob.pcm.model.visor.VisorResponse;
import pe.gob.pcm.sweb.core.facade.VisorFacade;
import pe.gob.pcm.sweb.core.service.DespachoService;
import pe.gob.pcm.util.GenericUtil;
import pe.gob.pcm.helper.Util;
import pe.gob.pcm.helper.Constants;

@Component
@Slf4j
public class VisorFacadeImpl extends FacadeBase implements VisorFacade {

	@Autowired
	private DespachoService despachoService;
	@Value("${path.documento.std}")
	private String url_documento_std;
	@Override
	public VisorResponse validar(VisorRequest request) {
		VisorResponse visorResponse=new VisorResponse();
		try {
			validarRequest(request);
			DocumentoExterno  despacho=  despachoService.validar(request);
			DocumentoPrincipal documentoPrincipal=	despachoService.getDocumentoPrincipal(despacho.getSiddocext());
//			visorResponse.setDocumentPdf(documentoPrincipal.getBpdfdoc());
			/*File filePrincipal=new File(pathStd.concat(documentoPrincipal.getVnomdoc()));
			if(!filePrincipal.exists()) {
				throw new InternalException("No se ha Encontrado el Documento Adjunto");
			}
			InputStream inputStream =new FileInputStream(filePrincipal);
			visorResponse.setDocumentPdf(IOUtils.toByteArray(inputStream));
			*/
			visorResponse.setNameDocument(GenericUtil.emptyIfStringNull(despacho.getVnumdoc()));
			List<Anexo> anexos=despachoService.loadAnexo(despacho.getSiddocext());
			List<AnexoResponse> collections=new ArrayList<>();
			anexos.forEach(tt->{
				AnexoResponse anexoResponse= modelMapper.map(tt, AnexoResponse.class);
				/*File file=new File(pathStd.concat(tt.getVnomdoc()));
				if(!file.exists()) {
					StringBuilder stringBuilder=new StringBuilder();
					stringBuilder.append("El documento: ");
					stringBuilder.append(tt.getVnomdoc());
					stringBuilder.append(" no Existe.");
					throw new InternalException(stringBuilder.toString());
				}
				try {
					InputStream inputStreamAnexo = new FileInputStream(file);
					anexoResponse.setFileContent(IOUtils.toByteArray(inputStreamAnexo));
				} catch (IOException e) {
					log.error(e.getMessage());
				}*/
				collections.add(anexoResponse);
			});
			visorResponse.setAnexos(collections);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return visorResponse;
	}

	private void validarRequest(VisorRequest t) {
		if(GenericUtil.isEmptyWithTrim(t.getNumeroDocumento())) {
			throw new InternalException("Ingrese el numero de documento");
		}
		if(GenericUtil.isEmptyWithTrim(t.getTipoDocumentoCodigo())) {
			throw new InternalException("Seleccione el Tipo de documento");
		}

		if(GenericUtil.isEmptyWithTrim(t.getClave())) {
			throw new InternalException("Ingrese la clave de verificación");
		}
		
	}

	@Override
	public BaseOperacionResponse validarDocumento(VisorRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Optional<FileDto> obtenerDocumento(VisorRequest request) {
		// TODO Auto-generated method stub
		FileDto fileDto;
		validarRequest(request);
		DocumentoExterno  despacho=  despachoService.validar(request);
		DocumentoPrincipal documentoPrincipal=	despachoService.getDocumentoPrincipal(despacho.getSiddocext());		
		try {
			fileDto=new FileDto();
			//File file = Util.createTempFile(documentoPrincipal.getBpdfdoc(),Constants.PDF_EXTENSION);
			//********************
			final Path path = Files.createTempFile("mytemp", Constants.PDF_EXTENSION);
			Files.write(path, documentoPrincipal.getBpdfdoc());
		    File	file= path.toFile();
			//*******************
			String filename = Util.getFilename(documentoPrincipal.getVnomdoc(), Constants.PDF_EXTENSION);
			String content = "inline; filename=\"" + filename + "\"";
			fileDto.setFile(file);
			fileDto.setFileName(filename);
			fileDto.setContent(content);
			return Optional.of(fileDto);
		}catch (Exception e) {
			// TODO: handle exception
			throw new InternalException(e.getMessage());
		}	
		// Optional.empty();
	}

}
