package pe.gob.pcm.sweb.core.repository;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import pe.gob.pcm.domain.core.Despacho;

public interface DespachoRepository extends JpaRepository<Despacho, BigDecimal>, JpaSpecificationExecutor<Despacho> {

	@Query("SELECT t FROM Despacho t WHERE t.vnumregstd=:id")
	Optional<Despacho> validar(String id);
	

	 
}
