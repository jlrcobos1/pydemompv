package pe.gob.pcm.sweb.core.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.CollectionResponse;
import pe.gob.pcm.model.recepcion.RecepcionComboResponse;
import pe.gob.pcm.model.recepcion.RecepcionFiltroRequest;
import pe.gob.pcm.model.recepcion.RecepcionRequest;
import pe.gob.pcm.model.recepcion.RecepcionResponse;
import pe.gob.pcm.sweb.core.facade.RecepcionFacade;

@CrossOrigin(origins = "*", maxAge = 360)
@RestController
@RequestMapping("/api/recepcion")
public class RecepcionRestController {

	@Autowired
	private RecepcionFacade recepcionFacade;

	@CrossOrigin("*")
	@PostMapping("/find")
	public CollectionResponse<RecepcionResponse> find(@RequestBody RecepcionFiltroRequest request) {
		return recepcionFacade.find(request);
	}

	@CrossOrigin("*")
	@PostMapping("/saveOrUpdate")
	public BaseOperacionResponse saveOrUpdate(@RequestBody RecepcionRequest request) {
		return recepcionFacade.saveOrUpdate(request);
	}

	@CrossOrigin("*")
	@DeleteMapping("/delete/{recepcionId}")
	public BaseOperacionResponse delete(@PathVariable BigDecimal recepcionId) {
		return recepcionFacade.delete(recepcionId);
	}

	@CrossOrigin("*")
	@GetMapping("/get/{recepcionId}")
	public RecepcionResponse get(@PathVariable BigDecimal recepcionId) {
		return recepcionFacade.get(recepcionId);
	}

	@CrossOrigin("*")
	@GetMapping("/init")
	public RecepcionComboResponse init() {
		return recepcionFacade.init();
	}

	@CrossOrigin("*")
	@GetMapping("/initForm")
	public RecepcionResponse initForm() {
		return recepcionFacade.initForm();
	}

}
