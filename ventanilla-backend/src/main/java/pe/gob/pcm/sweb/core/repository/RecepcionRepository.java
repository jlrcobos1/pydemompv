package pe.gob.pcm.sweb.core.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import pe.gob.pcm.domain.core.Recepcion;

public interface RecepcionRepository extends JpaRepository<Recepcion, BigDecimal>, JpaSpecificationExecutor<Recepcion> {

	 
}
