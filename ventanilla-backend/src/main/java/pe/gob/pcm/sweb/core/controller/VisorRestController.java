package pe.gob.pcm.sweb.core.controller;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.pcm.model.visor.FileDto;
import pe.gob.pcm.model.visor.VisorRequest;
import pe.gob.pcm.model.visor.VisorResponse;
import pe.gob.pcm.sweb.core.facade.VisorFacade;

@CrossOrigin(origins = "*", maxAge = 360)
@RestController
@RequestMapping("/api/visor")
public class VisorRestController {

	@Autowired
	private VisorFacade visorFacade;

	@CrossOrigin("*")
	@PostMapping("/validar")
	public VisorResponse validar(@RequestBody VisorRequest request) {
		return visorFacade.validar(request);
	}
	
	@CrossOrigin("*")
	@RequestMapping(value = "/downloadPrincipal", method = RequestMethod.POST)
	public ResponseEntity<Object> downloadFile(@RequestBody VisorRequest request) throws IOException
	{
		Optional<FileDto> file=visorFacade.obtenerDocumento(request);	
		//String filename = "C:/OFICIO-D00022-OFICIO-OSINFOR-05.1-SALIDA.pdf";
		//File file = new File(filename);
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file.get().getFile()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition",
				String.format("attachment; filename=\"%s\"", file.get().getFileName()));
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers)
				.contentLength(file.get().getFile().length())
				//.contentType(MediaType.parseMediaType("application/pdf")).body(resource);
		        .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);

		return responseEntity;
	}
	
}
