 package pe.gob.pcm.sweb.core.facade.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import pe.gob.pcm.core.facade.FacadeBase;
import pe.gob.pcm.domain.core.Despacho;
import pe.gob.pcm.helper.FiltroHelper;
import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.CollectionResponse;
import pe.gob.pcm.model.despacho.DespachoComboResponse;
import pe.gob.pcm.model.despacho.DespachoFiltroRequest;
import pe.gob.pcm.model.despacho.DespachoRequest;
import pe.gob.pcm.model.despacho.DespachoResponse;
import pe.gob.pcm.sweb.core.facade.DespachoFacade;
import pe.gob.pcm.sweb.core.service.DespachoService;
import pe.gob.pcm.util.Constantes;
import pe.gob.pcm.util.GenericUtil;

@Component
@Slf4j
public class DespachoFacadeImpl extends FacadeBase implements DespachoFacade {


	@Autowired
	private DespachoService despachoService;

	@Override
	public DespachoComboResponse init() {
		log.info(":::::::::::::::::::::::::::load init::::::::::::::::::::::::::::::::");
		DespachoComboResponse comboResponse=new DespachoComboResponse();
		return comboResponse;
	}
	
	@Override
	public DespachoResponse initForm() {
		DespachoResponse despachoResponse=new DespachoResponse();
		DespachoComboResponse comboResponse=new DespachoComboResponse();
		return despachoResponse;
	}
	
	
	@Override
	public CollectionResponse<DespachoResponse> find(DespachoFiltroRequest t) {
		List<DespachoResponse> collection = new ArrayList<>();
		GenericUtil.toUpperCase(t);
		FiltroHelper.validateFiltro(t);
		List<Despacho> listDTO = despachoService.find(t, PageRequest.of(t.getStart(), t.getLimit(), Sort.by(Direction.DESC, "sidemiext")));
		listDTO.forEach(tt -> {
			collection.add(modelMapper.map(tt, DespachoResponse.class));
		});

		return new CollectionResponse<DespachoResponse>(collection, t.getStart(), t.getLimit(), t.getTotalCount());
	}

	@Override
	public BaseOperacionResponse saveOrUpdate(DespachoRequest request) {
		GenericUtil.toUpperCase(request);
		despachoService.saveOrUpdate(request);
		return new BaseOperacionResponse(Constantes.SUCCESS, messageSave);
	}

	@Override
	public BaseOperacionResponse delete(BigDecimal despachoId) {
		despachoService.delete(despachoId);
		return new BaseOperacionResponse(Constantes.SUCCESS, messageDelete);
	}

	@Override
	public DespachoResponse get(BigDecimal despachoId) {
		Despacho despacho=	despachoService.get(despachoId);
		DespachoResponse despachoResponse= modelMapper.map(despacho, DespachoResponse.class);
		return despachoResponse;
	}
	
	@Override
	public BaseOperacionResponse sync() {
		despachoService.sync();
		return new BaseOperacionResponse(Constantes.SUCCESS, "Se ha sincronizado correctamente");
	}
	
	@Override
	public BaseOperacionResponse send(BigDecimal documentoId) {
		return despachoService.send(documentoId);
	}



}
