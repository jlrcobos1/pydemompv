package pe.gob.pcm.sweb.core.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import pe.gob.pcm.domain.core.Anexo;

public interface AnexoRepository extends JpaRepository<Anexo, BigDecimal>, JpaSpecificationExecutor<Anexo> {

	@Query("SELECT t FROM Anexo t WHERE t.siddocext =:siddocext")
	List<Anexo> findByExterno(BigDecimal siddocext);

}
