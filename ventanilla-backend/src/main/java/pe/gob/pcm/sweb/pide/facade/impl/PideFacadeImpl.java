package pe.gob.pcm.sweb.pide.facade.impl;

import java.awt.geom.GeneralPath;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import pe.gob.pcm.core.exception.InternalException;
import pe.gob.pcm.core.facade.FacadeBase;
import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.pide.PideRequest;
import pe.gob.pcm.sweb.pide.facade.PideFacade;
import pe.gob.pcm.sweb.pide.service.PideService;
import pe.gob.pcm.util.Constantes;
import pe.gob.pcm.util.GenericUtil;
import pe.gob.segdi.wsentidad.ws.EntidadProxy;

@Component
@Slf4j
public class PideFacadeImpl extends FacadeBase implements PideFacade {
	
	@Value("${pide.validar}")
	private String validarEntidad;
	
	@Autowired
	private PideService pideService;
	
	private static final String TIENE_PIDE="0000";
	@Override
	public BaseOperacionResponse validar(String ruc) {
		try {
			EntidadProxy proxy = new EntidadProxy(validarEntidad);
			String resultado=  proxy.validarEntidad(ruc);
			if(!TIENE_PIDE.equals(resultado)) {
				return new BaseOperacionResponse(Constantes.Validar.SIN_PIDE,"La Entidad no cuenta con PIDE");
			}
		} catch (Exception e) {
			throw new InternalException(e.getMessage());
		}
		return new BaseOperacionResponse(Constantes.Validar.TIENE_PIDE, "La entidad si cuenta con PIDE");
	}
	
	@Override
	public BaseOperacionResponse validarRuc(PideRequest t) {
		try {
			EntidadProxy proxy = new EntidadProxy(validarEntidad);
			String resultado=  proxy.validarEntidad(t.getRuc());
			if(!TIENE_PIDE.equals(resultado)) {
				return new BaseOperacionResponse(Constantes.Validar.SIN_PIDE,"La Entidad no cuenta con PIDE");
			}
		} catch (Exception e) {
			throw new InternalException(e.getMessage());
		}
		return new BaseOperacionResponse(Constantes.Validar.TIENE_PIDE, "La entidad si cuenta con PIDE");
	}

	@Override
	public BaseOperacionResponse getCuo(String ruc) {
		String cuo = Constantes.EMPTY;
		try {
			cuo = pideService.getCuo(ruc);
			if (GenericUtil.isEmptyWithTrim(cuo)) {
				throw new InternalException("No se ha podido obtener el CUO");
			}
		} catch (Exception e) {
			throw new InternalException(e.getMessage());
		}
		return new BaseOperacionResponse(Constantes.Validar.TIENE_PIDE, "El Numero de CUO es: ".concat(cuo));
	}
}
