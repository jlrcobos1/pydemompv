package pe.gob.pcm.sweb.core.facade;

import java.math.BigDecimal;

import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.CollectionResponse;
import pe.gob.pcm.model.recepcion.RecepcionComboResponse;
import pe.gob.pcm.model.recepcion.RecepcionFiltroRequest;
import pe.gob.pcm.model.recepcion.RecepcionRequest;
import pe.gob.pcm.model.recepcion.RecepcionResponse;

public interface RecepcionFacade {
	
	CollectionResponse<RecepcionResponse> find(RecepcionFiltroRequest request);

	BaseOperacionResponse saveOrUpdate(RecepcionRequest request);

	BaseOperacionResponse delete(BigDecimal equipoId);

	RecepcionResponse get(BigDecimal equipoId);

	RecepcionResponse initForm();

	RecepcionComboResponse init();

}
