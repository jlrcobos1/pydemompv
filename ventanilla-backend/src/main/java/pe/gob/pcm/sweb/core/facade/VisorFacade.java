package pe.gob.pcm.sweb.core.facade;

import java.io.File;
import java.util.Optional;

import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.visor.FileDto;
import pe.gob.pcm.model.visor.VisorRequest;
import pe.gob.pcm.model.visor.VisorResponse;

public interface VisorFacade {

	VisorResponse validar(VisorRequest request);
	BaseOperacionResponse validarDocumento(VisorRequest request); 
	Optional<FileDto> obtenerDocumento(VisorRequest request);

}
