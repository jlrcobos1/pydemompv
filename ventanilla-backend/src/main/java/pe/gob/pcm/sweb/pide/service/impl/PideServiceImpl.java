package pe.gob.pcm.sweb.pide.service.impl;

import java.net.URL;
import java.util.Random;

import javax.xml.ws.BindingProvider;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pe.gob.pcm.core.service.ServiceBase;
import pe.gob.pcm.sweb.pide.service.PideService;
import pe.gob.segdi.wsiopidetramite.ws.IOTramite;
import pe.gob.segdi.wsiopidetramite.ws.IOTramiteService;
import pe.gob.segdi.wsiopidetramite.ws.RecepcionTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaTramite;

@Service
@Slf4j
public class PideServiceImpl extends ServiceBase implements PideService {

	

	@Override
	public String getCuo(String ruc) {
//			String ip ="10.10.8.36"; //InetAddress.getLocalHost().getHostAddress();
//			PcmCuoPortTypeProxy port = new PcmCuoPortTypeProxy();
//			String cuo = port.getCUOEntidad(ruc, urlPide);
		try {
//			String ip = InetAddress.getLocalHost().getHostAddress();
//			PcmCuoPortTypeProxy port = new PcmCuoPortTypeProxy(urlPide);
//			String cuo = port.getCUO(ip);
//			if (GenericUtil.isEmptyWithTrim(cuo)) {
				Random random = new Random(System.currentTimeMillis());
				int cuoR = Math.abs(1000000000 + random.nextInt(2000000000));
				return String.valueOf(cuoR);
//			}
//			return cuo;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	@Override
	public RespuestaTramite despachar(RecepcionTramite request) {
		 try {
			 	IOTramiteService service = new IOTramiteService(new URL(urlTramite));
				IOTramite tramitePort = service.getIOTramitePort();
				BindingProvider bindingProvider = (BindingProvider) tramitePort;
				bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, urlTramite);
				return tramitePort.recepcionarTramiteResponse(request);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
}
