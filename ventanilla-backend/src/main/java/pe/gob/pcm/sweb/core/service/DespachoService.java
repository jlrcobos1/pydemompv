package pe.gob.pcm.sweb.core.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Pageable;

import pe.gob.pcm.domain.core.Anexo;
import pe.gob.pcm.domain.core.Despacho;
import pe.gob.pcm.domain.core.DocumentoExterno;
import pe.gob.pcm.domain.core.DocumentoPrincipal;
import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.despacho.DespachoFiltroRequest;
import pe.gob.pcm.model.despacho.DespachoRequest;
import pe.gob.pcm.model.visor.VisorRequest;

public interface DespachoService {

	List<Despacho> find(DespachoFiltroRequest request, Pageable pageable);

	Despacho get(BigDecimal despachoId);

	void delete(BigDecimal despachoId);

	void saveOrUpdate(DespachoRequest despacho);

	void sync();

	BaseOperacionResponse send(BigDecimal documentoId);

	DocumentoExterno validar(VisorRequest request);

	List<Anexo> loadAnexo(BigDecimal siddocext);

	DocumentoPrincipal getDocumentoPrincipal(BigDecimal siddocext);

}
