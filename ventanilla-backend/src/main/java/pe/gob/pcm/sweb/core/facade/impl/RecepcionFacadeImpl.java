 package pe.gob.pcm.sweb.core.facade.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import pe.gob.pcm.core.facade.FacadeBase;
import pe.gob.pcm.domain.core.Recepcion;
import pe.gob.pcm.helper.FiltroHelper;
import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.CollectionResponse;
import pe.gob.pcm.model.recepcion.RecepcionComboResponse;
import pe.gob.pcm.model.recepcion.RecepcionFiltroRequest;
import pe.gob.pcm.model.recepcion.RecepcionRequest;
import pe.gob.pcm.model.recepcion.RecepcionResponse;
import pe.gob.pcm.sweb.core.facade.RecepcionFacade;
import pe.gob.pcm.sweb.core.service.RecepcionService;
import pe.gob.pcm.util.Constantes;
import pe.gob.pcm.util.GenericUtil;

@Component
@Slf4j
public class RecepcionFacadeImpl extends FacadeBase implements RecepcionFacade {


	@Autowired
	private RecepcionService recepcionService;

	@Override
	public RecepcionComboResponse init() {
		log.info(":::::::::::::::::::::::::::load init::::::::::::::::::::::::::::::::");
		RecepcionComboResponse comboResponse=new RecepcionComboResponse();
		return comboResponse;
	}
	
	@Override
	public RecepcionResponse initForm() {
		RecepcionResponse recepcionResponse=new RecepcionResponse();
		RecepcionComboResponse comboResponse=new RecepcionComboResponse();
		return recepcionResponse;
	}
	
	
	@Override
	public CollectionResponse<RecepcionResponse> find(RecepcionFiltroRequest t) {
		List<RecepcionResponse> collection = new ArrayList<>();
		GenericUtil.toUpperCase(t);
		FiltroHelper.validateFiltro(t);
		List<Recepcion> listDTO = recepcionService.find(t, PageRequest.of(t.getStart(), t.getLimit(), Sort.by(Direction.DESC, "sidrecext")));
		listDTO.forEach(tt -> {
			collection.add(modelMapper.map(tt, RecepcionResponse.class));
		});

		return new CollectionResponse<RecepcionResponse>(collection, t.getStart(), t.getLimit(), t.getTotalCount());
	}

	@Override
	public BaseOperacionResponse saveOrUpdate(RecepcionRequest request) {
		GenericUtil.toUpperCase(request);
		recepcionService.saveOrUpdate(request);
		return new BaseOperacionResponse(Constantes.SUCCESS, messageSave);
	}

	@Override
	public BaseOperacionResponse delete(BigDecimal recepcionId) {
		recepcionService.delete(recepcionId);
		return new BaseOperacionResponse(Constantes.SUCCESS, messageDelete);
	}

	@Override
	public RecepcionResponse get(BigDecimal recepcionId) {
		Recepcion recepcion=	recepcionService.get(recepcionId);
		RecepcionResponse recepcionResponse= modelMapper.map(recepcion, RecepcionResponse.class);
		return recepcionResponse;

	}



}
