package pe.gob.pcm.sweb.core.service.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import pe.gob.pcm.core.exception.InternalException;
import pe.gob.pcm.core.service.ServiceBase;
import pe.gob.pcm.domain.core.Recepcion;
import pe.gob.pcm.model.recepcion.RecepcionFiltroRequest;
import pe.gob.pcm.model.recepcion.RecepcionRequest;
import pe.gob.pcm.sweb.core.repository.RecepcionRepository;
import pe.gob.pcm.sweb.core.service.RecepcionService;
import pe.gob.pcm.util.GenericUtil;

@Service
@Slf4j
public class RecepcionServiceImpl  extends ServiceBase implements RecepcionService {

	@Autowired
	private RecepcionRepository recepcionRepository;
	 
	@Override
	public List<Recepcion> find(RecepcionFiltroRequest t,Pageable pageable) {
		Page<Recepcion> page = recepcionRepository.findAll(new Specification<Recepcion>() {
			private static final long serialVersionUID = 2502949831020987898L;
			@Override
			public javax.persistence.criteria.Predicate toPredicate(Root<Recepcion> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (!GenericUtil.isEmptyWithTrim(t.getCuo())) {
					predicates.add(criteriaBuilder .and(criteriaBuilder.like(root.get("cuo"), "%".concat(t.getCuo()).concat("%"))));
				} 
				 
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		}, pageable);
		t.setTotalCount(page.getTotalElements());
		return page.getContent();
		 
	}

	@Override
	public Recepcion get(BigDecimal recepcionId) {

		Optional<Recepcion> optional = recepcionRepository.findById(recepcionId);
		if (optional.isPresent()) {
			return optional.get();
		}
		throw new InternalException("El Recepcion no se encuentra registrado");

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
	public void delete(BigDecimal recepcionId) {
		Recepcion recepcion = this.get(recepcionId);
		recepcionRepository.save(recepcion);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
	public void saveOrUpdate(RecepcionRequest t) {
		log.info("::::::::::::::::::saveOrUpdate:::");
		if (GenericUtil.isNotEmpty(t.getSidrecext()) && t.getSidrecext().intValue() > 0) {
			Recepcion recepcion = this.get(t.getSidrecext());
			recepcionRepository.save(recepcion);
		} else {
			Recepcion recepcion = modelMapper.map(t, Recepcion.class);
			recepcionRepository.save(recepcion);
		}
	}

}
