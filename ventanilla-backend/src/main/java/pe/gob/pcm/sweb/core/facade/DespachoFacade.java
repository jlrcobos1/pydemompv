package pe.gob.pcm.sweb.core.facade;

import java.math.BigDecimal;

import pe.gob.pcm.model.BaseOperacionResponse;
import pe.gob.pcm.model.CollectionResponse;
import pe.gob.pcm.model.despacho.DespachoComboResponse;
import pe.gob.pcm.model.despacho.DespachoFiltroRequest;
import pe.gob.pcm.model.despacho.DespachoRequest;
import pe.gob.pcm.model.despacho.DespachoResponse;

public interface DespachoFacade {
	
	CollectionResponse<DespachoResponse> find(DespachoFiltroRequest request);

	BaseOperacionResponse saveOrUpdate(DespachoRequest request);

	BaseOperacionResponse delete(BigDecimal equipoId);

	DespachoResponse get(BigDecimal equipoId);

	DespachoResponse initForm();

	DespachoComboResponse init();

	BaseOperacionResponse sync();

	BaseOperacionResponse send(BigDecimal documentoId);

}
