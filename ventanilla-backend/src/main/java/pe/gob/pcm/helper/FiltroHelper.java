package pe.gob.pcm.helper;

import pe.gob.pcm.core.exception.InternalException;
import pe.gob.pcm.model.BaseRequest;
import pe.gob.pcm.util.GenericUtil;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class FiltroHelper {

	public static void validateFiltro(BaseRequest t) {
		if (GenericUtil.isNull(t.getStart())) {
			throw new InternalException("El parámetro de filtro inicial no se encuentra definido");
		}
		if (GenericUtil.isNull(t.getLimit())) {
			throw new InternalException("El parámetro de filtro final no se encuentra definido");
		}
	}

}
