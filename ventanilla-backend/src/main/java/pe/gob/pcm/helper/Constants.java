package pe.gob.pcm.helper;

public class Constants {
	
	public static String PIDE_TIPO_DOCUMENTO_OFICIO = "01";
	public static String PIDE_TIPO_DOCUMENTO_CARTA = "02";
	
	public static final int STD_TIPO_DOCUMENTO_OFICIO = 20;
	public static final int STD_TIPO_DOCUMENTO_OFICIO_MULTIPLE = 22;
	public static final int STD_TIPO_DOCUMENTO_CARTA = 2;
	public static final int STD_TIPO_DOCUMENTO_CARTA_MULTIPLE = 3;
	public static String PDF_EXTENSION = ".pdf";
}
