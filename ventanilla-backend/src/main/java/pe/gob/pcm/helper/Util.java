package pe.gob.pcm.helper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Util {

	public static String getTipoDocumentoPIDE(String tipo){
		// Debido a un error que se origina al guardar el tipo de documento en el STD
		if( tipo.startsWith("0") ){
			return tipo;
		}
		
		String tipoDocumentoPide = null;
		switch (Integer.valueOf(tipo)) {
		case Constants.STD_TIPO_DOCUMENTO_OFICIO:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_OFICIO;
			break;
		case Constants.STD_TIPO_DOCUMENTO_OFICIO_MULTIPLE:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_OFICIO;
			break;
		case Constants.STD_TIPO_DOCUMENTO_CARTA:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_CARTA;
			break;
		case Constants.STD_TIPO_DOCUMENTO_CARTA_MULTIPLE:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_CARTA;
			break;
		default:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_OFICIO;
			break;
		}
		
		return tipoDocumentoPide;
	}
	public static File createTempFile(byte[] bytes, String extension) throws IOException{
		final Path path = Files.createTempFile("myTempFile", extension);
		Files.write(path, bytes);
		return path.toFile();
	}
	public static String getFilename(String filename, String extension){
		filename = removeCharactersNotAllowed(filename);
		extension = extension.toLowerCase();
		if( filename.toLowerCase().endsWith(extension) ){
			return filename;
		}else{
			return filename + extension;
		}
	}
	public static String removeCharactersNotAllowed(String value){
		value = value.trim();
		value = value.replaceAll("/", "-");
		value = value.replaceAll(":", "-");
		value = value.replaceAll("\\*", "-");
		return value;
	}
}
