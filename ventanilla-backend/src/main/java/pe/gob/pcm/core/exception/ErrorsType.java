package pe.gob.pcm.core.exception;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import pe.gob.pcm.util.GenericUtil;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.util.EnumSet;

@Slf4j
public enum ErrorsType {

    COMMON_E500_REQUEST(InternalException.class,"Error desconocido"),
    COMMON_E400_REQUEST(BadRequestException.class, "Validacion en el request"),
    COMMON_E400_TOKEN(BadRequestException.class, "Token invalido, no se pudo recuperar el userinfo del usuario"),
    COMMON_E500_PARSING_JSON(InternalException.class, "JSON.Error when parsing json");


    public final Class<? extends IntegrationException> exception;
    public final String errorCode;
    private final String systemMessage;
    private String userMessage;
    private Environment environment;

    ErrorsType(Class<? extends IntegrationException> clazz, String systemMessage) {
        exception = clazz;
        this.errorCode = name();
        this.systemMessage = systemMessage;

        try {
            int httpStatus = (int) FieldUtils.readStaticField(clazz, "HTTP_STATUS");
            StringBuilder messageContains = new StringBuilder("E").append(httpStatus).append("_");
            if (!name().contains(messageContains.toString())) {
                throw new RuntimeException("Codigo de error no coincide con la tipificacion del httpStatus");
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public IntegrationException newInstance() {
        return newInstance(null, "Error sin mensaje definido en el properties.");
    }

    public IntegrationException newInstance(Throwable cause) {
        return newInstance(cause, null);
    }

    public IntegrationException newInstance(Throwable cause, String userMessage) {
        try {
            this.userMessage = environment.getProperty("tumi.errors." + errorCode);

            if (GenericUtil.isEmpty(this.userMessage)) {
                this.userMessage = userMessage;
            }

            final IntegrationException integrationException;

            if (cause != null) {
                integrationException = exception.getDeclaredConstructor(String.class, String.class, String.class, Throwable.class).newInstance(buildSystemMessage(systemMessage, cause.getMessage()), this.userMessage, errorCode, cause);
            } else {
                integrationException = exception.getDeclaredConstructor(String.class, String.class, String.class).newInstance(systemMessage, this.userMessage, errorCode);
            }
            return integrationException;

        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.info("error in new instance", e);
        }
        return new InternalException();
    }

    private String buildSystemMessage(String systemMessage, String causeMessage) {
        String msg = systemMessage;
        if (org.springframework.util.StringUtils.hasLength(systemMessage) && org.springframework.util.StringUtils.hasLength(causeMessage)) {
            msg = systemMessage + ". " + causeMessage;
        } else if (org.springframework.util.StringUtils.hasLength(causeMessage)) {
            msg = causeMessage;
        }
        return msg;
    }

    public String getUserMessage() {
        return userMessage;
    }


    void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Component
    public static class ErrorsTypeServiceInjector {

        private final Environment environment;

        public ErrorsTypeServiceInjector(Environment environment) {
            this.environment = environment;
        }

        @PostConstruct
        public void postConstruct() {
            for (ErrorsType errorsType : EnumSet.allOf(ErrorsType.class)) {
                errorsType.setEnvironment(environment);
            }
        }
    }
}
