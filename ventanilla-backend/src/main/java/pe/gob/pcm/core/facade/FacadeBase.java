package pe.gob.pcm.core.facade;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import pe.gob.pcm.domain.audit.UserSesion;
import pe.gob.pcm.sweb.core.facade.ComunFacade;

@Service("facadeBase")
public class FacadeBase {

	@Autowired
	protected ModelMapper modelMapper;

	@Autowired
	protected ComunFacade comunFacade;

	@Autowired
	protected UserSesion userSesion;

	@Value("${message.global.save}")
	protected String messageSave;
	@Value("${message.global.update}")
	protected String messageUpdate;
	@Value("${message.global.delete}")
	protected String messageDelete;
	@Value("${path.documento.std}")
	protected String pathStd;
}
