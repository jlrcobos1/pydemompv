package pe.gob.pcm.core.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import pe.gob.pcm.domain.audit.UserSesion;
import pe.gob.pcm.sweb.core.service.ComunService;

@Service("serviceBase")
public class ServiceBase {

	@Autowired
	protected ModelMapper modelMapper;

	@Autowired
	protected UserSesion userSesion;

	@Autowired
	protected ComunService comunService;
	
	@Value("${pide.tramite}")
	protected String urlTramite;
	
	@Value("${path.documento.std}")
	protected String pathStd;

}
