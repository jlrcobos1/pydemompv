package pe.gob.pcm.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

//import brave.Tracer;
import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@ResponseBody
@Slf4j
public class CoreExceptionHandler {

//    private final Tracer tracer;
//
//    public CoreExceptionHandler(Tracer tracer) {
//        this.tracer = tracer;
//    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BadRequestException badRequestException(BadRequestException exception) {
//        exception.setTraceId(tracer.currentSpan().context().traceIdString());
        log.warn(buildMessageLog(HttpStatus.BAD_REQUEST, exception.toString()), exception);
        return exception;
    }

    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ForbiddenException forbiddenException(ForbiddenException exception) {
//        exception.setTraceId(tracer.currentSpan().context().traceIdString());
        log.info(buildMessageLog(HttpStatus.FORBIDDEN, exception.toString()));
        return exception;
    }

    @ExceptionHandler(InternalException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public InternalException internalException(InternalException exception) {
//        exception.setTraceId(tracer.currentSpan().context().traceIdString());
        log.error(buildMessageLog(HttpStatus.INTERNAL_SERVER_ERROR, exception.toString()), exception);
        return exception;
    }

    @ExceptionHandler(NoContentException.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public NoContentException noContentException(NoContentException exception) {
//        exception.setTraceId(tracer.currentSpan().context().traceIdString());
        log.error(buildMessageLog(HttpStatus.NO_CONTENT, exception.toString()), exception);
        return exception;
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public NotFoundException notFoundException(NotFoundException exception) {
//        exception.setTraceId(tracer.currentSpan().context().traceIdString());
        log.info(buildMessageLog(HttpStatus.NOT_FOUND, exception.toString()));
        return exception;
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public ServiceUnavailableException serviceUnavailableException(ServiceUnavailableException exception) {
//        exception.setTraceId(tracer.currentSpan().context().traceIdString());
        log.error(buildMessageLog(HttpStatus.SERVICE_UNAVAILABLE, exception.toString()), exception);
        return exception;
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public UnauthorizedException unauthorizedException(UnauthorizedException exception) {
//        exception.setTraceId(tracer.currentSpan().context().traceIdString());
        log.error(buildMessageLog(HttpStatus.UNAUTHORIZED, exception.toString()), exception);
        return exception;
    }

    @ExceptionHandler(IntegrationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public IntegrationException integrationException(IntegrationException exception) {
//        exception.setTraceId(tracer.currentSpan().context().traceIdString());
        log.error(buildMessageLog(HttpStatus.INTERNAL_SERVER_ERROR, exception.toString()), exception);
        return exception;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public InternalException buildGenericException(Throwable exception) {
        InternalException internalException = (InternalException) ErrorsType.COMMON_E500_REQUEST.newInstance(exception);
//        internalException.setTraceId(tracer.currentSpan().context().traceIdString());
        internalException.setSystemMessage(exception.getMessage());
        internalException.setUserMessage("Error desconocido");
        log.error(buildMessageLog(HttpStatus.INTERNAL_SERVER_ERROR, "GenericException ".concat(exception.toString())), exception);
        return internalException;
    }

    private String buildMessageLog(HttpStatus status, String message) {
        return String.format("Response API <%d> <%s>", status.value(), message);
    }
}
