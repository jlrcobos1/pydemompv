
package pe.gob.pcm;
//
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//
@SpringBootApplication
public class WebVentanillaApplication extends SpringBootServletInitializer {
//	 
//
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(applicationClass, args);
	}
	private static Class<WebVentanillaApplication> applicationClass = WebVentanillaApplication.class;
	 
}
//usar esto cuando se publica
