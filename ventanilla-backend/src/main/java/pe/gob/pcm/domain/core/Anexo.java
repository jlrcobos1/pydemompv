package pe.gob.pcm.domain.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "IOTDTD_ANEXO", schema = Constantes.SCHEMA, catalog = Constantes.CATALOG)
public class Anexo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1537770824317199822L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "siddocanx", unique = true, nullable = false)
	private BigDecimal siddocanx;
	@Column(name = "vnomdoc")
	private String vnomdoc;
	@Column(name = "siddocext")
	private BigDecimal siddocext;

	@Column(name = "dfecreg")
	private LocalDateTime dfecreg;

}
