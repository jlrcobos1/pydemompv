package pe.gob.pcm.domain.std;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Tra_M_Tramite_Digitales", schema = Constantes.SCHEMA_STD, catalog = Constantes.CATALOG_STD)
public class TramiteDigital implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3734962918360548609L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "icoddigital", unique = true, nullable = false)
	private BigDecimal digitalId;
	@Column(name = "icodtramite")
	private BigDecimal tramiteId;
	@Column(name = "cnombreoriginal")
	private String nombreOriginal;
	@Column(name = "cnombrenuevo")
	private String nombreNuevo;

//	icodmovimiento
}
