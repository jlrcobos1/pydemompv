package pe.gob.pcm.domain.std;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Tra_M_Tramite_Movimientos", schema = Constantes.SCHEMA_STD, catalog = Constantes.CATALOG_STD)
public class TramiteMovimiento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3734962918360548609L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "icodmovimiento", unique = true, nullable = false)
	private BigDecimal movimientoId;
	@Column(name = "icodtramite")
	private BigDecimal tramiteId;
	@Column(name = "icodtrabajadorregistro")
	private BigDecimal trabajadorId;
	@Column(name = "nflgtipodoc")
	private BigDecimal flagTipoDocumento;
	@Column(name = "icodoficinaorigen")
	private BigDecimal oficinaOrigenId;
	@Column(name = "ffecrecepcion")
	private LocalDateTime fechaRecepcion;
	@Column(name = "icodoficinaderivar")
	private BigDecimal oficinaDerivarId;
	@Column(name = "icodtrabajadorderivar")
	private BigDecimal trabajadorDerivarId;
	@Column(name = "ccodtipodocderivar")
	private BigDecimal tipoDocumentoDerivarId;
	@Column(name = "icodindicacionderivar")
	private BigDecimal indicacionDerivarId;
	@Column(name = "casuntoderivar")
	private String asuntoDerivar;
	@Column(name = "cobservacionesderivar")
	private String observacionDerivar;
	@Column(name = "ffecderivar")
	private LocalDateTime fechaDerivar;
	@Column(name = "icodtrabajadordelegado")
	private BigDecimal trabajadorDelegadoId;
	@Column(name = "icodindicaciondelegado")
	private BigDecimal indicacionDelegadoId;
	@Column(name = "cobservacionesdelegado")
	private String observacionDelegado;
	@Column(name = "ffecdelegado")
	private LocalDateTime fechaDelegado;
	@Column(name = "icodtrabajadorfinalizar")
	private BigDecimal trabajadorFinalizarId;
	@Column(name = "cobservacionesfinalizar")
	private String observacionFinalizar;
	@Column(name = "ffecfinalizar")
	private LocalDateTime fechaFinalizar;
	@Column(name = "ffecmovimiento")
	private LocalDateTime fechaMovimiento;
	@Column(name = "nestadomovimiento")
	private BigDecimal estadoMovimiento;
	@Column(name = "nflgenvio")
	private BigDecimal flagEnvio;
	@Column(name = "cflgtipomovimiento")
	private BigDecimal flagTipoMovimiento;
	@Column(name = "ffecdelegadorecepcion")
	private LocalDateTime fechaDelegadoRecepcion;

//	cprioridadderivar
//	  
//	icodtrabajadorenviar
//	cobservacionesenviar
//	ffecenviar
//	
//	nflgenviado
//	icoddigital
//	icodtramiterel
//	cnumdocumentoderivar
//	ccodtipodocresponder
//	icodtrabajadorresponder
//	casuntoresponder
//	cobservacionesresponder
//	icoddigitalresponder
//	ffecresponder
//	creferenciaderivar
//	cflgcopia
//	cflgoficina
//	icodtramitederivar
//	icodmovimientorel
//	icodtramiterespuesta
//	icodtramiteantecesor
//	icodmovasociado
//	icodtrabajadordelegador
//	ccodtipodocdelegar
}
