package pe.gob.pcm.domain.std;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Tra_M_Trabajadores", schema = Constantes.SCHEMA_STD, catalog = Constantes.CATALOG_STD)
public class Trabajador implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3734962918360548609L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "icodtrabajador", unique = true, nullable = false)
	private BigDecimal trabajadorId;
	@Column(name = "icodoficina")
	private BigDecimal oficinaId;
	@Column(name = "icodperfil")
	private BigDecimal perfilId;
	@Column(name = "cnombrestrabajador")
	private String nombre;
	@Column(name = "capellidostrabajador")
	private String apellido;
	@Column(name = "ctipodocidentidad")
	private BigDecimal tipoDocumentoId;
	@Column(name = "cnumdocidentidad")
	private String numeroDocumento;
	@Column(name = "cdirecciontrabajador")
	private String direccion;
	@Column(name = "cmailtrabajador")
	private String email;
	@Column(name = "cusuario")
	private String usuario;

//	ctlftrabajador1
//	ctlftrabajador2
//	ccargo
//	
//	cpassword
//	fcreacion
//	fultimoacceso
//	nflgestado
//	icodcategoria
//	cnomarchivofoto
//	fmodificacion
//	nflgjefe
}
