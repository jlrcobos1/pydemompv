package pe.gob.pcm.domain.audit;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import pe.gob.pcm.core.SesionResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component("userSesion")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserSesion implements Serializable {

	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 4635301402209747347L;
	private SesionResponse registro;

	public UserSesion() {
		registro = new SesionResponse();
	}

}
