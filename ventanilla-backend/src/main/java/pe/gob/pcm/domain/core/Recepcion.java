package pe.gob.pcm.domain.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "IOTDTC_RECEPCION", schema = Constantes.SCHEMA, catalog = Constantes.CATALOG)
public class Recepcion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7575984807928448959L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sidrecext", unique = true, nullable = false)
	private BigDecimal sidrecext;

	@Column(name = "vrucentrem")
	private String vrucentrem;

	@Column(name = "vuniorgrem")
	private String vuniorgrem;

	@Column(name = "ctipdociderem")
	private String ctipdociderem;

	@Column(name = "vnumdociderem")
	private String vnumdociderem;

	@Column(name = "vnumregstd")
	private String vnumregstd;

	@Column(name = "vanioregstd")
	private String vanioregstd;

	@Column(name = "vuniorgstd")
	private String vuniorgstd;

	@Column(name = "ccoduniorgstd")
	private String ccoduniorgstd;
	@Column(name = "vdesanxstd")
	private String vdesanxstd;

	@Column(name = "dfecregstd")
	private LocalDateTime dfecregstd;

	@Column(name = "vusuregstd")
	private String vusuregstd;

	@Column(name = "bcarstd")
	private byte[] bcarstd;

	@Column(name = "vcuo")
	private String vcuo;
	@Column(name = "vcuoref")
	private String vcuoref;

	@Column(name = "vobs")
	private String vobs;

	@Column(name = "cflgestobs")
	private String cflgestobs;
	@Column(name = "cflgest")
	private String cflgest;
	@Column(name = "dfecreg")
	private LocalDateTime dfecreg;

	@Column(name = "vusumod")
	private String vusumod;

	@Column(name = "dfecmod")
	private LocalDateTime dfecmod;
	@Column(name = "cflgenvcar")
	private String cflgenvcar;
	@Column(name = "vidremitentestd")
	private BigDecimal vidremitentestd;

	@Column(name = "vidtramitestd")
	private BigDecimal vidtramitestd;

	@Column(name = "vncarstd")
	private String vncarstd;

}
