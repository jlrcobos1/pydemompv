package pe.gob.pcm.domain.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "IOTDTM_DOC_EXTERNO", schema = Constantes.SCHEMA, catalog = Constantes.CATALOG)
public class DocumentoExterno  implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7474707757581320001L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "siddocext", unique = true, nullable = false)
	private BigDecimal siddocext;
	@Column(name = "vnomentemi")
	private String vnomentemi;
	@Column(name = "ccodtipdoc")
	private String ccodtipdoc;
	

	@Column(name = "vnumdoc")
	private String vnumdoc;
	@Column(name = "dfecdoc")
	private LocalDateTime	 dfecdoc;
	@Column(name = "vuniorgdst")
	private String vuniorgdst;
	@Column(name = "vnomdst")
	private String vnomdst;
	@Column(name = "vnomcardst")
	private String vnomcardst;
	@Column(name = "vasu")
	private String vasu;
	@Column(name = "cindtup")
	private String cindtup;

	@Column(name = "snumanx")
	private BigDecimal snumanx;


	@Column(name = "snumfol")
	private BigDecimal snumfol;

	@Column(name = "vurldocanx")
	private String vurldocanx;
	@Column(name = "sidemiext")
	private BigDecimal sidemiext;
	@Column(name = "sidrecext")
	private BigDecimal sidrecext;
	
	
	
	
}
