package pe.gob.pcm.domain.std;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Tra_M_Oficinas", schema = Constantes.SCHEMA_STD, catalog = Constantes.CATALOG_STD)
public class Oficina implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3734962918360548609L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "icodoficina", unique = true, nullable = false)
	private BigDecimal oficinaId;
	@Column(name = "cnomoficina")
	private String nombre;
	@Column(name = "csiglaoficina")
	private String sigla;
	
//	icodubicacion
//	iflgestado
//	cod_anterior
//	iflgvisible
//	icodparent
//	ccodigooficina
//	idgobregional
}
