package pe.gob.pcm.domain.std;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Tra_M_Remitente", schema = Constantes.SCHEMA_STD, catalog = Constantes.CATALOG_STD)
public class Remitente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3734962918360548609L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "icodremitente", unique = true, nullable = false)
	private BigDecimal remitenteId;
	@Column(name = "ctipopersona")
	private BigDecimal tipoPersonaId;
	@Column(name = "cnombre")
	private String nombre;
	@Column(name = "nnumdocumento")
	private String numeroDocumento;
	@Column(name = "cdireccion")
	private String direccion;
	@Column(name = "cemail")
	private String email;
	@Column(name = "cflag")
	private BigDecimal flag;

//	ntelefono
//	nfax
//	cdepartamento
//	cprovincia
//	cdistrito
//	crepresentante
//	
//	ctipodocidentidad
//	csiglaremitente
//	cobservaciones
//	flagafiliado
//	idafiliado
}
