package pe.gob.pcm.domain.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "IOTDTC_DESPACHO", schema = Constantes.SCHEMA, catalog = Constantes.CATALOG)
public class Despacho implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8738798673722339349L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SIDEMIEXT", unique = true, nullable = false)
	private BigDecimal sidemiext;

	@Column(name = "VNUMREGSTD")
	private String vnumregstd;
	@Column(name = "VANIOREGSTD")
	private String vanioregstd;
	@Column(name = "CTIPDOCIDEREM")
	private String ctipdociderem;

	@Column(name = "VNUMDOCIDEREM")
	private String vnumdociderem;
	@Column(name = "VCODUNIORGREM")
	private String vcoduniorgrem;
	@Column(name = "VUNIORGREM")
	private String vuniorgrem;

	@Column(name = "VCUO")
	private String vcuo;

	@Column(name = "VRUCENTREC")
	private String vrucentrec;
	@Column(name = "VNOMENTREC")
	private String vnomentrec;

	@Column(name = "VNUMREGSTDREC")
	private String vnumregstdrec;

	@Column(name = "VANIOREGSTDREC")
	private String vanioregstdrec;

	@Column(name = "VUNIORGSTDREC")
	private String vuniorgstdrec;
	@Column(name = "vdesanxstdrec")
	private String vdesanxstdrec;

	@Column(name = "dfecregstdrec")
	private LocalDateTime dfecregstdrec;

	@Column(name = "vusuregstdrec")
	private String vusuregstdrec;

	@Column(name = "bcarstdrec")
	private byte[] bcarstdrec;

	@Column(name = "vobs")
	private String vobs;

	@Column(name = "vcuoref")
	private String vcuoref;
	@Column(name = "cflgest")
	private String cflgest;

	@Column(name = "cflgenv")
	private String cflgenv;
	@Column(name = "dfecenv")
	private LocalDateTime dfecenv;

	@Column(name = "vusureg")
	private String vusureg;
	@Column(name = "dfecreg")
	private LocalDateTime dfecreg;
	@Column(name = "vusumod")
	private String vusumod;
	@Column(name = "dfecmod")
	private LocalDateTime dfecmod;

}
