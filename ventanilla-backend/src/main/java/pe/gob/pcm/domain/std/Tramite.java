package pe.gob.pcm.domain.std;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Tra_M_Tramite", schema = Constantes.SCHEMA_STD, catalog = Constantes.CATALOG_STD)
public class Tramite implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3734962918360548609L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "icodtramite", unique = true, nullable = false)
	private BigDecimal tramiteId;
	@Column(name = "nflgtipodoc")
	private BigDecimal tipoDocumento;
	@Column(name = "ccodificacion")
	private String codificacion;
	@Column(name = "icodtrabajadorregistro")
	private BigDecimal trabajadorId;

	@Column(name = "icodoficinaregistro")
	private BigDecimal oficinaId;
	@Column(name = "icodtema")
	private BigDecimal temaId;
	@Column(name = "ccodtipodoc")
	private String tipoDocumentoCodigo;
	@Column(name = "ffecdocumento")
	private LocalDate fechaDocumento;
	@Column(name = "cnrodocumento")
	private String numeroDocumento;
	@Column(name = "icodremitente")
	private BigDecimal remitenteId;
	@Column(name = "casunto")
	private String asunto;
	@Column(name = "cobservaciones")
	private String observacion;
	@Column(name = "creferencia")
	private String referencia;
	@Column(name = "icodindicacion")
	private BigDecimal indicacionId;
	@Column(name = "nnumfolio")
	private String numeroFolio;
	@Column(name = "ntiemporespuesta")
	private BigDecimal tiempoRespuesta;
	@Column(name = "nflgenvio")
	private BigDecimal flagEnvio;
	@Column(name = "ffecregistro")
	private LocalDateTime fechaRegistro;
	@Column(name = "ncodbarra")
	private BigDecimal codigoBarra;
	@Column(name = "cpassword")
	private String password;
	@Column(name = "nflgestado")
	private BigDecimal flagEstado;
	@Column(name = "nflganulado")
	private BigDecimal flagAnulado;
	@Column(name = "icodtrabajadorsolicitado")
	private BigDecimal trabajadorSolicitadoId;
	@Column(name = "nflgrpta")
	private BigDecimal flagRespuesta;
	@Column(name = "cnomremite")
	private String remitente;
	@Column(name = "nflgclasedoc")
	private BigDecimal flagClaseDocumento;
	@Column(name = "csiglaautor")
	private String siglaAutor;
	@Column(name = "nflgenvionoti")
	private BigDecimal flagEnvioNotificacion;
	@Column(name = "nflgelectronico")
	private BigDecimal flagElectronico;

	@Column(name = "idafiliado")
	private BigDecimal afiliadoId;
	@Column(name = "nflgenviopide")
	private BigDecimal flagEnvioPide;

//	ffecplazo
//	crptaok
//	icodtupaclase
//	icodtupa 
//	
//	
//	icodtramiterel
//	ffecfinalizado
//	icodpaquete
//	icodoficinasolicitado
//	ccodconcurso
//	
//	ctit_hab_sigo
//	cadm_sigo
//	cexp_sigo
//	nflgsoada
//	icodoficinanotificado
//	
//	nflgtransferencia
//	cobservaciontransferencia
//	
//	icodtupaopcion

}
