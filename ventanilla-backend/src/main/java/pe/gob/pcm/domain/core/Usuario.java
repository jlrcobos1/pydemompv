package pe.gob.pcm.domain.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "USER_SESSION", schema = Constantes.SCHEMA, catalog = Constantes.CATALOG)
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3764290633851242140L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idRegister", unique = true, nullable = false)
	private BigDecimal id;
	@Column(name = "idUser")
	private BigDecimal idUser;
	@Column(name = "names")
	private BigDecimal nombre;
	@Column(name = "role")
	private String rol;
	@Column(name = "token")
	private String token;
	@Column(name = "expires")
	private LocalDateTime expires;
	@Column(name = "scope")
	private String scope;

}
