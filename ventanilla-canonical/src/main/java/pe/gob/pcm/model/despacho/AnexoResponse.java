package pe.gob.pcm.model.despacho;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AnexoResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1312750114270099906L;
	private BigDecimal siddocanx;
	private String vnomdoc;
	private BigDecimal siddocext;
	private LocalDateTime dfecreg;
	private byte[] fileContent;

}
