package pe.gob.pcm.model.pide;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
public class PideRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1469826411306592937L;
	private String ruc;
}
