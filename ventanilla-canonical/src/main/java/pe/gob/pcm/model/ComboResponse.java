package pe.gob.pcm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ComboResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2243475940401676830L;

	private BigDecimal id;
	private String codigo;
	private String nombre;

	public ComboResponse() {
	}

	public ComboResponse(BigDecimal id, String codigo, String nombre) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public ComboResponse(BigDecimal id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

}
