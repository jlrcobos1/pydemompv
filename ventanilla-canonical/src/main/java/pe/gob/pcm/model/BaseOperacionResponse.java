package pe.gob.pcm.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseOperacionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -819889188656363491L;
	private String codigo;
	private String mensaje;

	public BaseOperacionResponse() {
	}

	public BaseOperacionResponse(String codigo, String mensaje) {
		this.codigo = codigo;
		this.mensaje = mensaje;
	}

}
