package pe.gob.pcm.model.visor;

import java.io.File;
import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FileDto  implements Serializable{
	private static final long serialVersionUID = -4191808194785739255L;
  private File file;
  private String fileName;
  private String content; 
}
