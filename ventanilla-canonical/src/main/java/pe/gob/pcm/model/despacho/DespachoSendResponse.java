package pe.gob.pcm.model.despacho;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DespachoSendResponse  implements Serializable {

	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -8946046502514987514L;
	private LocalDate desde;
	private LocalDate hasta;

}
