package pe.gob.pcm.model.file;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
public class FileResponse implements Serializable {

	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -5007256391208205215L;
	private byte[] fileContent;
	private String nombre;
	private String contentType;
	private String extension;
 
}
