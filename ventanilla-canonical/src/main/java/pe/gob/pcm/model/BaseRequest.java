package pe.gob.pcm.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.util.Constantes;

@Getter
@Setter
@NoArgsConstructor
public class BaseRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4151456517727744637L;
	private Integer start = Constantes.PAGINATION_START;
	private String sort;
	private Integer limit = Constantes.PAGINATION_SIZE;
	private Long totalCount;
}
