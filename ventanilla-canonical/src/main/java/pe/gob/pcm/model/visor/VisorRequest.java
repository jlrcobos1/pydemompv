package pe.gob.pcm.model.visor;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VisorRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4195808194785739255L;
	private String numeroDocumento;
	private String clave;
	private String tipoDocumentoCodigo;
}
