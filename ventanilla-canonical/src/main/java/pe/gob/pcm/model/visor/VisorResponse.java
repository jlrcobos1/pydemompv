package pe.gob.pcm.model.visor;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.model.despacho.AnexoResponse;

@Getter
@Setter
@NoArgsConstructor
public class VisorResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5978841689757051643L;
	private List<AnexoResponse> anexos;
	private String nameDocument;
	private byte[] documentPdf;
}
