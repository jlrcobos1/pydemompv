package pe.gob.pcm.model.recepcion;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.model.ComboBaseResponse;

@Getter
@Setter
@NoArgsConstructor
public class RecepcionComboResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2473666065785720074L;
	private ComboBaseResponse estado;

}
