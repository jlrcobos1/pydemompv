package pe.gob.pcm.model.despacho;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.model.ComboBaseResponse;

@Getter
@Setter
@NoArgsConstructor
public class DespachoComboResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4887694902479009027L;
	private ComboBaseResponse estado;

}
