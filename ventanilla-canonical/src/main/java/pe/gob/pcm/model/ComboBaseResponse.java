package pe.gob.pcm.model;

import java.io.Serializable;
import java.util.List;

import pe.gob.pcm.util.GenericUtil;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ComboBaseResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 757193680506542328L;
	private List<ComboResponse> list;
	private Integer size;

	public ComboBaseResponse() {
	}

	/**
	 * 
	 * @param list
	 */
	public ComboBaseResponse(List<ComboResponse> list) {
		this.list = list;
		if (GenericUtil.isNotNull(list)) {
			this.size = list.size();
		}
	}

}
