package pe.gob.pcm.model.recepcion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RecepcionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6674903836478978349L;
	private BigDecimal sidrecext;
	private String vrucentrem;
	private String vuniorgrem;
	private String ctipdociderem;
	private String vnumdociderem;
	private String vnumregstd;
	private String vanioregstd;
	private String vuniorgstd;
	private String ccoduniorgstd;
	private String vdesanxstd;
	private LocalDateTime dfecregstd;
	private String vusuregstd;
//	private byte[] bcarstd;
	private String vcuo;
	private String vcuoref;
	private String vobs;
	private String cflgestobs;
	private String cflgest;
	private LocalDateTime dfecreg;
	private String vusumod;
	private LocalDateTime dfecmod;
	private String cflgenvcar;
	private BigDecimal vidremitentestd;
	private BigDecimal vidtramitestd;
	private String vncarstd;
	private RecepcionComboResponse combo;

}
