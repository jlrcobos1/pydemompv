package pe.gob.pcm.model.recepcion;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.model.BaseRequest;

@Getter
@Setter
@NoArgsConstructor
public class RecepcionFiltroRequest extends BaseRequest implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 139763942330269331L;
	private String cuo;
	private String ruc;
	private LocalDate desde;
	private LocalDate hasta;
	private String estado;

}
