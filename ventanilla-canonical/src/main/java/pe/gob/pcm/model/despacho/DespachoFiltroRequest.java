package pe.gob.pcm.model.despacho;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.pcm.model.BaseRequest;

@Getter
@Setter
@NoArgsConstructor
public class DespachoFiltroRequest extends BaseRequest implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = -2380969360851257587L;
	private String cuo;
	private String ruc;
	private LocalDate desde;
	private LocalDate hasta;
	private String estado;

}
