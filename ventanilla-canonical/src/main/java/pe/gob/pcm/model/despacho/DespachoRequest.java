package pe.gob.pcm.model.despacho;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DespachoRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1567881047801471341L;
	private BigDecimal sidemiext;
	private String vnumregstd;
	private String vanioregstd;
	private String ctipdociderem;
	private String vnumdociderem;
	private String vcoduniorgrem;
	private String vuniorgrem;
	private String vcuo;
	private String vrucentrec;
	private String vnomentrec;
	private String vnumregstdrec;
	private String vanioregstdrec;
	private String vuniorgstdrec;
	private String vdesanxstdrec;
	private LocalDateTime dfecregstdrec;
	private String vusuregstdrec;
	private byte[] bcarstdrec;
	private String vobs;
	private String vcuoref;
	private String cflgest;
	private String cflgenv;
	private LocalDateTime dfecenv;
	private String vusureg;
	private LocalDateTime dfecreg;
	private String vusumod;
	private LocalDateTime dfecmod;

}
