package pe.gob.pcm.util;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.Collection;
import java.util.Map;

public class GenericUtil {

	private GenericUtil() {
	}

	/**
	 * checks if the object is not empty.
	 * 
	 * @param object
	 * @return true o false
	 */
	public static boolean isNotEmpty(Object object) {
		return !isObjectEmpty(object);
	}

	/**
	 * checks if the collection is empty.
	 * 
	 * @param collection
	 * @return true o false
	 */
	public static <E> boolean isEmpty(Collection<E> collection) {
		return (collection == null) || collection.isEmpty();
	}

	/**
	 * checks if the map is empty.
	 * 
	 * @param map
	 * @return true o false
	 */
	public static <K, E> boolean isEmpty(Map<K, E> map) {
		return (map == null) || (map.isEmpty());
	}

	/**
	 * checks if the character is empty.
	 * 
	 * @param character
	 * @return true o false
	 */
	public static boolean isEmpty(CharSequence character) {
		return (character == null) || (character.length() == 0);
	}

	public static boolean isEmptyWithTrim(String character) {
		return (character == null) || (character.trim().length() == 0);
	}

	public static String emptyIfStringNull(String character) {
		if (isEmptyWithTrim(character)) {
			return Constantes.EMPTY;
		}
		return character.trim();
	}

	/**
	 * 
	 * @param value
	 * @return true o false
	 */
	public static boolean isObjectEmpty(Object value) {
		if (value == null) {
			return true;
		} else if (value instanceof String) {
			return isEmpty((String) value);
		} else if (value instanceof CharSequence) {
			return isEmpty((CharSequence) value);
		} else if (value instanceof Collection || value instanceof Map) {
			return isCollectionEmpty(value);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private static boolean isCollectionEmpty(Object value) {
		if (value instanceof Collection) {
			return isEmpty((Collection<? extends Object>) value);
		} else {
			return isEmpty((Map<? extends Object, ? extends Object>) value);
		}

	}

	/**
	 * checks if the object is null.
	 * 
	 * @param object
	 * @return true o false
	 */
	public static boolean isNull(Object object) {
		return object == null;
	}

	/**
	 * checks if the object is not null.
	 * 
	 * @param object
	 * @return true o false
	 */
	public static boolean isNotNull(Object object) {
		return object != null;
	}

	public static void toUpperCase(Object request) {
		try {
			for (java.lang.reflect.Field field : request.getClass().getDeclaredFields()) {
				if (field.getType().equals(String.class)) {
					if (!field.isAccessible())
						field.setAccessible(true);
					if (field.get(request) != null && !((String) field.get(request)).trim().equals(Constantes.EMPTY)) {
						field.set(request, ((String) field.get(request)).toUpperCase());
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static String fillZero(Integer numero, Integer longuitud) {
		StringBuilder builder = new StringBuilder();
		Integer cantida = longuitud - numero.toString().length();
		for (int i = 0; i < cantida; i++) {
			builder.append(Constantes.CERO);
		}
		builder.append(numero);
		return builder.toString();
	}

	public static String leftZero(String numero) {
		BigDecimal cantida = new BigDecimal(numero);
		return cantida.toString();
	}
	public static byte[] toBase64(byte[] bytes){
		if( bytes != null ){
			return Base64.getEncoder().encode(bytes);
		}
		return null;
	}
	public static String toBase64String(byte[] bytes){
		if( bytes != null ){
			return Base64.getEncoder().encodeToString(bytes);
		}
		return null;
	}
	
	
}
