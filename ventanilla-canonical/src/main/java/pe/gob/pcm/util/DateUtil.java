package pe.gob.pcm.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.time.DateFormatUtils;

public class DateUtil {

	private DateUtil() {
	}

	private static final Locale LOCALE_PE = new Locale("es", "pe");
	public static final String ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT = DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT
			.getPattern();
	public static final String FORMAT01 = "d' de 'MMMM' de 'yyyy";
	public static final String FORMAT02 = "yyyyMMddHHmmss";
	public static final String FORMAT03 = "yyyyMMdd";
	public static final String FORMATO_REPORTE = "dd/MM/yyyy";
	public static final String FORMATO_DATE_TIME = "dd/MM/yyyy HH:mm";
	public static final String FORMATO_DATE_TIME_AP = "dd/MM/yyyy HH:mm a";
	public static final String FORMAT_DATE = "dd-MM-yyyy";

	/**
	 * get current Date.
	 * 
	 * @return Date
	 */
	public static LocalDate getCurrentLocalDate() {
		ZonedDateTime fecha = ZonedDateTime.now(ZoneId.systemDefault());
		return fecha.withZoneSameInstant(ZoneId.of(Constantes.TIME_ZONE)).toLocalDate();
	}

	public static LocalDateTime getCurrentLocalDateTime() {
		ZonedDateTime fecha = ZonedDateTime.now(ZoneId.systemDefault());
		return fecha.withZoneSameInstant(ZoneId.of(Constantes.TIME_ZONE)).toLocalDateTime();
	}

	/**
	 * 
	 * @return
	 */
	public static Integer getCurrentYear() {
		return ZonedDateTime.now(ZoneId.of(Constantes.TIME_ZONE)).getYear();
	}

	/**
	 * 
	 * @return
	 */
	public static Integer getCurrentMonth() {
		return ZonedDateTime.now(ZoneId.of(Constantes.TIME_ZONE)).getMonth().getValue();

	}

	/**
	 * 
	 * @return
	 */
	public static String getCurrentMonthDesc() {
		return ZonedDateTime.now(ZoneId.of(Constantes.TIME_ZONE)).getMonth().getDisplayName(TextStyle.FULL,
				Locale.getDefault());
	}

	/**
	 * 
	 * @return
	 */
	public static Integer getCurrentDay() {
		return ZonedDateTime.now(ZoneId.of(Constantes.TIME_ZONE)).getDayOfMonth();
	}

	/**
	 * 
	 * @return
	 */
	public static Integer getCurrentHour() {
		return ZonedDateTime.now(ZoneId.of(Constantes.TIME_ZONE)).getHour();
	}

	/**
	 * 
	 * @return
	 */
	public static Integer getCurrentMinute() {
		return ZonedDateTime.now(ZoneId.of(Constantes.TIME_ZONE)).getMinute();
	}

	/**
	 * 
	 * @return
	 */
	public static Integer getCurrentSecond() {
		return ZonedDateTime.now(ZoneId.of(Constantes.TIME_ZONE)).getSecond();
	}

	public static Long getCurrentMillSecond() {
		return ZonedDateTime.now(ZoneId.of(Constantes.TIME_ZONE)).toInstant().toEpochMilli();
	}

	public static BigDecimal getCantidadDias(LocalDate inicio, LocalDate fin) {
		if (GenericUtil.isNotNull(inicio) && GenericUtil.isNotNull(fin)) {
			return new BigDecimal(ChronoUnit.DAYS.between(inicio, fin));
		}
		return null;
	}

	public static BigDecimal getCantidadMeses(LocalDate inicio, LocalDate fin) {
		if (GenericUtil.isNotNull(inicio) && GenericUtil.isNotNull(fin)) {
			return new BigDecimal(ChronoUnit.MONTHS.between(inicio, fin));
		}
		return null;
	}

	public static LocalDateTime addDaysDate(LocalDateTime fecha, Integer days) {
		ZonedDateTime zonedDateTime = ZonedDateTime.of(fecha.plusDays(days), ZoneId.systemDefault());
		return zonedDateTime.toLocalDateTime();
	}

	public static LocalDateTime createDate(LocalDate fecha) {
		ZonedDateTime zonedDateTime = ZonedDateTime.of(fecha,LocalTime.now(), ZoneId.systemDefault());
		return zonedDateTime.toLocalDateTime();
	}

	public static String format(LocalDate date, String format) {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withLocale(LOCALE_PE);

		return date != null ? date.format(dateTimeFormatter) : null;
	}
	
	public static Date convertToDate(LocalDateTime dateToConvert) {
	    return java.util.Date.from(dateToConvert.toLocalDate().atStartOfDay()
	      .atZone(ZoneId.systemDefault())
	      .toInstant());
	}
	 public static XMLGregorianCalendar convertToXMLGregorianCalendar(LocalDateTime date) throws DatatypeConfigurationException {
    	 XMLGregorianCalendar result = null;
    	 if( date != null ){
        	 GregorianCalendar calendar = new GregorianCalendar();
        	 calendar.setTime(convertToDate(date));
        	 DatatypeFactory df = DatatypeFactory.newInstance();
        	 result = df.newXMLGregorianCalendar(calendar);
    	 }
    	 return result;
     }
}
