package pe.gob.pcm.util;

import java.math.BigDecimal;

public class Constantes {

	public static final String BLANK_SPACE = " ";
	public static final String GUION = "-";
	public static final String HABILITADO = "S";
	public static final String INHABILITADO = "N";
	public static final String CERO = "0";
	public static final Integer PAGINATION_START = 0;
	public static final Integer PAGINATION_SIZE = 20;
	public static final String EMPTY = "";
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String TIME_ZONE = "America/Bogota";
	public static final String TIME_FORMAT = "HH:mm:ss.SSS";

	public static final String SCHEMA = "dbo";
	public static final String CATALOG = "OSINFOR_TRAMITE_DOC_prueba";
	

	public static final String SCHEMA_STD = "dbo";
	public static final String CATALOG_STD = "std_osinfor_prueba";

	public static final BigDecimal INCREMENTO = BigDecimal.ONE;

	private Constantes() {
	}

	public static class Catalogo {
		public static final String ESTADO_CHECKLIST = "ESTADO_CHECKLIST";

		private Catalogo() {
		}
	}

	public static class ContentType {

		public static final String WORD = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		public static final String PDF = "application/pdf";
		public static final String XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		public static final String XLS = "application/vnd.ms-excel";

		private ContentType() {
		}
	}

	public static class Extension {

		public static final String WORD_DOCX = ".docx";
		public static final String PDF = ".pdf";

		private Extension() {
		}
	}
	public static class Validar {

		public static final String TIENE_PIDE = "S";
		public static final String SIN_PIDE = "N";

		private Validar() {
		}
	}

}
