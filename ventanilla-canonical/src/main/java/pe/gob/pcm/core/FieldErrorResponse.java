package pe.gob.pcm.core;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FieldErrorResponse implements Serializable {

    private static final long serialVersionUID = 2777918813024827202L;

    private String fieldName;
    private String message;
}
