package pe.gob.pcm.core;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class SesionResponse implements Serializable {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 637663734345737072L;
	private String usuario;
	private String nombre;
	private String rolCodigo;

	public SesionResponse() {
	}
 
 
}
