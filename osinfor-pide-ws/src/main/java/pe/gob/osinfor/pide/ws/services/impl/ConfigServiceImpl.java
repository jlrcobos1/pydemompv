package pe.gob.osinfor.pide.ws.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.services.ConfigService;

public class ConfigServiceImpl implements ConfigService {

	public static Logger LOG = LoggerFactory.getLogger(ConfigServiceImpl.class);
	private static final String ENVIRONMENT_SERVER_APP_CONFIG = "SERVER_APP_CONFIG";
	private static final String CONFIG_FILE = "osinfor-pide-ws-config.properties";
	
	@Override 
	public HashMap<String, String> getConfigParameters() {
		HashMap<String,String> listaDatos = new HashMap<>();
		Properties prop = new Properties();
		try {
			String path = System.getenv(ENVIRONMENT_SERVER_APP_CONFIG) + "\\" + CONFIG_FILE;
			LOG.info("Cargando propiedades desde el archivo de configuracion: [{}]", path);
			
			FileInputStream input = new FileInputStream(new File(path));
			if(input != null) {
				prop.load(input);

		    	listaDatos.put(URL_PCM_CUO_WS, prop.getProperty(URL_PCM_CUO_WS));
		    	listaDatos.put(URL_IOPIDE_TRAMITE_WS, prop.getProperty(URL_IOPIDE_TRAMITE_WS));
		    	listaDatos.put(URL_ENTIDAD_WS, prop.getProperty(URL_ENTIDAD_WS));
		    	listaDatos.put(PATH_DOCUMENTO_DESPACHO, prop.getProperty(PATH_DOCUMENTO_DESPACHO));
		    	listaDatos.put(PATH_DOCUMENTO_RECEPCION, prop.getProperty(PATH_DOCUMENTO_RECEPCION));
		    	listaDatos.put(PATH_DOCUMENTO_PUBLICO, prop.getProperty(PATH_DOCUMENTO_PUBLICO));
		    	listaDatos.put(PATH_DOCUMENTO_PUBLICO_SUBCARPETA, prop.getProperty(PATH_DOCUMENTO_PUBLICO_SUBCARPETA));
		    	listaDatos.put(PATH_STD_ALMACEN, prop.getProperty(PATH_STD_ALMACEN));
		    	listaDatos.put(PATH_VISOR_DOCUMENTOS, prop.getProperty(PATH_VISOR_DOCUMENTOS));
		    	
		    	listaDatos.put(ENTIDAD_RUC, prop.getProperty(ENTIDAD_RUC));
		    	listaDatos.put(ENTIDAD_NOMBRE, prop.getProperty(ENTIDAD_NOMBRE));
		    	listaDatos.put(DEPLOY_MODE, prop.getProperty(DEPLOY_MODE));
			}
		} catch (IOException e) {
			LOG.error("El archivo de configuracion no fue encontrado", e);
			throw new ServiceException("El archivo de configuracion no fue encontrado", e);
		} 
		return listaDatos;
	}
}

