package pe.gob.osinfor.pide.ws.dao;

import java.util.List;

import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoFilterDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;

public interface DespachoDAO  {

	List<DocumentoDespachoDTO> listarDespacho() throws DAOException;
	List<DocumentoDespachoDTO> listarDespachoById(String id) throws DAOException;
	List<DocumentoDespachoDTO> listarDespachoByFilter(DocumentoDespachoFilterDTO filterDTO) throws DAOException;
	List<DocumentoExternoDTO> listarDocumentosExternos(String idTramite) throws DAOException;
	DocumentoDespachoDTO getDocumentoDespacho(String idDocument);
	DocumentoPrincipalDTO getDocumentoPrincipal(String idDocumentDespacho);
	DocumentoExternoDTO getDocumentoDespachoExterno(String idDocumentDespacho);
	void updateEstadoDocumento(DocumentoDespachoDTO documento,String cuo,String estado);
	
	void saveDespacho(DocumentoDespachoDTO despacho) throws DAOException;
	List<DocumentoDespachoDTO> findDespachosByCuo(String cuo) throws DAOException;
	DocumentoDespachoDTO findDespachoById(String idTramite) throws DAOException;
}
