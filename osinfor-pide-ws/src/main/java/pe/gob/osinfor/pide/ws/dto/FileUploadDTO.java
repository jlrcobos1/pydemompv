package pe.gob.osinfor.pide.ws.dto;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;


public class FileUploadDTO {
	
	
	private InputStream uploadedInputStream;
	private FormDataContentDisposition fileDetail;
	public InputStream getUploadedInputStream() {
		return uploadedInputStream;
	}
	public void setUploadedInputStream(InputStream uploadedInputStream) {
		this.uploadedInputStream = uploadedInputStream;
	}
	public FormDataContentDisposition getFileDetail() {
		return fileDetail;
	}
	public void setFileDetail(FormDataContentDisposition fileDetail) {
		this.fileDetail = fileDetail;
	}
	
	
	
}
