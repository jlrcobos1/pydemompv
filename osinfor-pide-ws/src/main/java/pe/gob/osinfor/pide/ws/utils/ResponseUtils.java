package pe.gob.osinfor.pide.ws.utils;

import pe.gob.osinfor.pide.ws.dto.ResponseObjectDTO;
import pe.gob.osinfor.pide.ws.exception.ValidationException;

public class ResponseUtils {
	
	private static final String ERROR_TYPE_VALIDATION = "01";
	private static final String ERROR_TYPE_UNEXPECTED = "02";
	
	public static void validationError(ResponseObjectDTO response, ValidationException e){
		response.setMessage(e.getMessage());
		response.setStatus(false);
		response.setErrorType(ERROR_TYPE_VALIDATION);
	}
	
	public static void unexpectedError(ResponseObjectDTO response, String message){
		response.setMessage(message);
		response.setStatus(false);
		response.setErrorType(ERROR_TYPE_UNEXPECTED);
	}
	
	public static void incorrect(ResponseObjectDTO response, String message){
		response.setMessage(message);
		response.setStatus(false);
		response.setErrorType(ERROR_TYPE_VALIDATION);
	}
	
	public static void ok(ResponseObjectDTO response, Object object, String message){
		response.setMessage(message);
		response.setStatus(true);
		response.setObjeto(Utilitarios.convertToJson(object));
	}
}
