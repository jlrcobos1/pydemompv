package pe.gob.osinfor.pide.ws.services.autenticacion.impl;

import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.osinfor.pide.ws.dto.TokenAccessDto;
import pe.gob.osinfor.pide.ws.services.autenticacion.ITokenDropAccess;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.text.ParseException;

public class TokenDropAccessImpl implements ITokenDropAccess {

  public static Logger LOG = LoggerFactory.getLogger(TokenDropAccessImpl.class);

  private EntityManagerFactory factory = Persistence
    .createEntityManagerFactory("OSINFOR-PIDE");


  @Override
  public Integer deleteTokenAccess(TokenAccessDto entity) {
    try {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        //em.detach(entity);
        TokenAccessDto entityToRemove = em.merge(entity);
        em.remove(entityToRemove);
        em.getTransaction().commit();
        em.close();
        return 1;
    } catch (Exception ex) {
        return 0;
    }
  }

    private String getQueryString(String token,String scope) {
        QueryToken queryToken = new QueryToken(token,scope);
        LOG.info(queryToken.buildQuery());
        return queryToken.buildQuery();
    }

    private class QueryToken {
        private String query;

        public QueryToken(String token,String scope) {
            this.query = query = "delete from TokenAccessDto t "
              + " where t.token = '"+token+"'"
              + " and t.token = '"+scope+"'";
        }
        String buildQuery() {
            return query;
        }
    }
}
