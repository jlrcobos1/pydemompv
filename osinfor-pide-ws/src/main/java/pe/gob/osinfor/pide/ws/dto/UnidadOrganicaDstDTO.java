package pe.gob.osinfor.pide.ws.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="VIEW_UNIDAD_ORG_DST_REC")
public class UnidadOrganicaDstDTO {

	@Id
	private String iCodOficina;
	private String cNomOficina;
	
	public String getiCodOficina() {
		return iCodOficina;
	}
	public void setiCodOficina(String iCodOficina) {
		this.iCodOficina = iCodOficina;
	}
	public String getcNomOficina() {
		return cNomOficina;
	}
	public void setcNomOficina(String cNomOficina) {
		this.cNomOficina = cNomOficina;
	}
	
	
	
	
}
