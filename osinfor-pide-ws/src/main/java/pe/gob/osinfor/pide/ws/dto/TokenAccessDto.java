package pe.gob.osinfor.pide.ws.dto;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="USER_SESSION")
public class TokenAccessDto {

  @Id
  Integer idRegister;
  Integer idUser;
  String names;
  String role;
  String token;
  @Temporal(TemporalType.TIMESTAMP)
  Date expires;
  String scope;

  public TokenAccessDto() {

  }

  public TokenAccessDto(Integer idRegister, Integer idUser,
                        String names, String role,
                        String token, Date expires,
                        String scope) {
    this.idRegister = idRegister;
    this.idUser = idUser;
    this.names = names;
    this.role = role;
    this.token = token;
    this.expires = expires;
    this.scope = scope;
  }
}
