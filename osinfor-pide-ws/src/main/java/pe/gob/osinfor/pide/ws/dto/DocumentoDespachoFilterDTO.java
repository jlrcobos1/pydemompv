package pe.gob.osinfor.pide.ws.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DocumentoDespachoFilterDTO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String idDocument;
	private String ruc,state;
	private String numDocumentoStd;
	private String numDocumentoEntidad;
	
	public String getIdDocument() {
		return idDocument;
	}
	public void setIdDocument(String idDocument) {
		this.idDocument = idDocument;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getNumDocumentoStd() {
		return numDocumentoStd;
	}
	public void setNumDocumentoStd(String numDocumentoStd) {
		this.numDocumentoStd = numDocumentoStd;
	}
	public String getNumDocumentoEntidad() {
		return numDocumentoEntidad;
	}
	public void setNumDocumentoEntidad(String numDocumentoEntidad) {
		this.numDocumentoEntidad = numDocumentoEntidad;
	}

	
}
