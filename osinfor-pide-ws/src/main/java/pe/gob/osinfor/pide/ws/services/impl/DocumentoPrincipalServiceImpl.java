package pe.gob.osinfor.pide.ws.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dao.DocumentoPrincipalDAO;
import pe.gob.osinfor.pide.ws.dao.impl.DocumentoPrincipalDAOImpl;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;
import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.services.DocumentoPrincipalService;

public class DocumentoPrincipalServiceImpl implements DocumentoPrincipalService {

	public static Logger LOG = LoggerFactory.getLogger(DocumentoPrincipalServiceImpl.class);

	@Override
	public DocumentoPrincipalDTO findDocumentoPrincipalByExterno(String idExterno) throws ServiceException {
		LOG.info("Buscando documento principal - Documento externo: [{}]", idExterno);
		DocumentoPrincipalDAO dao = new DocumentoPrincipalDAOImpl();
		try{
			return dao.findDocumentoPrincipalByExterno(idExterno);
		}catch(DAOException e){
			throw new ServiceException(e);
		}
	}


}
