package pe.gob.osinfor.pide.ws.services;

import java.util.List;

import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionFilterDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionLista;
import pe.gob.osinfor.pide.ws.dto.RemitenteDTO;
import pe.gob.osinfor.pide.ws.dto.RequestObjectDTO;
import pe.gob.osinfor.pide.ws.dto.ResponsableUniOrgDstDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;

public interface RecepcionService {

	List<DocumentoRecepcionLista> listarRecepcion(DocumentoRecepcionFilterDTO filterDTO) throws ServiceException;
	RequestObjectDTO getListUnidadOrganicaDestino();
	List<ResponsableUniOrgDstDTO> getListResponsableUniOrgDestino(String iCodOficina) throws ServiceException;
	DocumentoRecepcionDTO getDocumentoRecepcionById(String idDocument) throws ServiceException;
	void recibirDocumento(DocumentoRecepcionDTO documento) throws ServiceException;
	List<RemitenteDTO> findRemitenteByDocumento(String numDocumento) throws ServiceException;
	
}
