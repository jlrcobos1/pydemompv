//package pe.gob.osinfor.pide.ws.filter;
//
//import java.io.IOException;
//
//import javax.ws.rs.container.ContainerRequestContext;
//import javax.ws.rs.container.ContainerRequestFilter;
//import javax.ws.rs.core.Context;
//import javax.ws.rs.core.HttpHeaders;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.core.UriInfo;
//import javax.ws.rs.ext.Provider;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import pe.gob.osinfor.pide.ws.exception.UnAuthorizedException;
//import pe.gob.osinfor.pide.ws.utils.Utilitarios;
//
//@Provider
//public class AuthenticationFilter implements ContainerRequestFilter{
//
//	public static Logger LOG = LoggerFactory.getLogger(AuthenticationFilter.class);
//
//    @Context
//    private UriInfo info;
//    private static final String REQUEST_METHOD = "OPTIONS";
//	private static final String REALM = "OsinforRealm";
//    private static final String AUTHENTICATION_SCHEME = "OsinforAuth";
//    private static final String[] excludePath = new String[]{"osinfor/security/authenticate", "osinfor/download-cargo"};
//
//    @Override
//    public void filter(ContainerRequestContext context) throws IOException {
//    	String method = context.getRequest().getMethod();
//
//    	// Si el metodo es el OPTIONS se ignora la peticion y se devuelve el estado OK por defecto
//    	if( !Utilitarios.isBlank(method) && method.equalsIgnoreCase(REQUEST_METHOD)){
//    		return;
//    	}
//
//    	String path = info.getPath();
//     	boolean isExcludePath = isExcludePath(path);
//     	LOG.info("Filtrando solicitud - Metodo: [{}], Request Path: [{}], Exclude Path: [{}]", method, path, isExcludePath);
//
//     	// Si es una URL exlcluida de la evaluacion, se devuelve el estado OK por defecto
//     	if( isExcludePath ){
//     		return;
//     	}
//
//     	try{
//        	String header = context.getHeaderString(HttpHeaders.AUTHORIZATION);
//        	validateScheme(header);
//        	validateToken(header);
//     	}catch(UnAuthorizedException e){
//     		unauthorized(context);
//     	}
//    }
//
//    private static void unauthorized(ContainerRequestContext requestContext) {
//        requestContext.abortWith(
//                Response.status(Response.Status.UNAUTHORIZED)
//                        .header(HttpHeaders.WWW_AUTHENTICATE, AUTHENTICATION_SCHEME + " realm=\"" + REALM + "\"")
//                        .build());
//    }
//
//    private void validateScheme(String header) throws UnAuthorizedException{
//    	LOG.info("Filtrando solicitud - Header: [{}]", header);
//
//    	if( Utilitarios.isBlank(header) ){
//    		throw new UnAuthorizedException("El esquema no fue encontrado");
//    	}
//
//    	boolean isScheme = (header.toLowerCase().startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " "));
//    	if( !isScheme ){
//    		throw new UnAuthorizedException("El esquema no fue encontrado o validado correctamente");
//    	}
//    }
//
//    private void validateToken(String header) throws UnAuthorizedException {
//    	String token = header.substring(AUTHENTICATION_SCHEME.length()).trim();
//
//    	LOG.info("Filtrando solicitud - Token: [{}]", token);
//    	if( Utilitarios.isBlank(token) ){
//    		throw new UnAuthorizedException("El token no fue encontrado");
//    	}
//    }
//
//    private boolean isExcludePath(String path){
//    	boolean result = false;
//    	for( String p: excludePath ){
//    		if( path.contains(p) ){
//    			result = true;
//    			break;
//    		}
//    	}
//    	return result;
//    }
//}
