package pe.gob.osinfor.pide.ws.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="IOTDTC_RECEPCION")
public class DocumentoRecepcionDTO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String SIDRECEXT;
	private String VRUCENTREM;
	private String VUNIORGREM;
	private String CTIPDOCIDEREM;
	private String VNUMDOCIDEREM;
	private String VNUMREGSTD;
	private String VANIOREGSTD;
	private String VUNIORGSTD;
	private String CCODUNIORGSTD;	// Nuevo
	private String VDESANXSTD;		// Nuevo
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DFECREGSTD;
	private String VUSUREGSTD;
	private byte[] BCARSTD;
	private String VCUO;
	private String VCUOREF;
	private String VOBS;
	private String CFLGESTOBS;		// Nuevo
	private String CFLGEST;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DFECREG;
	private String VUSUMOD;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DFECMOD;
	private String CFLGENVCAR;		// Nuevo
	private String VIDREMITENTESTD;		// Nuevo
	private String VIDTRAMITESTD;		// Nuevo
	private String VNCARSTD;		// Nuevo
	
	public String getSIDRECEXT() {
		return SIDRECEXT;
	}
	public void setSIDRECEXT(String sIDRECEXT) {
		SIDRECEXT = sIDRECEXT;
	}
	public String getVRUCENTREM() {
		return VRUCENTREM;
	}
	public void setVRUCENTREM(String vRUCENTREM) {
		VRUCENTREM = vRUCENTREM;
	}
	public String getVUNIORGREM() {
		return VUNIORGREM;
	}
	public void setVUNIORGREM(String vUNIORGREM) {
		VUNIORGREM = vUNIORGREM;
	}
	public String getCTIPDOCIDEREM() {
		return CTIPDOCIDEREM;
	}
	public void setCTIPDOCIDEREM(String cTIPDOCIDEREM) {
		CTIPDOCIDEREM = cTIPDOCIDEREM;
	}
	public String getVNUMDOCIDEREM() {
		return VNUMDOCIDEREM;
	}
	public void setVNUMDOCIDEREM(String vNUMDOCIDEREM) {
		VNUMDOCIDEREM = vNUMDOCIDEREM;
	}
	public String getVNUMREGSTD() {
		return VNUMREGSTD;
	}
	public void setVNUMREGSTD(String vNUMREGSTD) {
		VNUMREGSTD = vNUMREGSTD;
	}
	public String getVANIOREGSTD() {
		return VANIOREGSTD;
	}
	public void setVANIOREGSTD(String vANIOREGSTD) {
		VANIOREGSTD = vANIOREGSTD;
	}
	public String getVUNIORGSTD() {
		return VUNIORGSTD;
	}
	public void setVUNIORGSTD(String vUNIORGSTD) {
		VUNIORGSTD = vUNIORGSTD;
	}
	public String getCCODUNIORGSTD() {
		return CCODUNIORGSTD;
	}
	public void setCCODUNIORGSTD(String cCODUNIORGSTD) {
		CCODUNIORGSTD = cCODUNIORGSTD;
	}
	public String getVDESANXSTD() {
		return VDESANXSTD;
	}
	public void setVDESANXSTD(String vDESANXSTD) {
		VDESANXSTD = vDESANXSTD;
	}
	public Date getDFECREGSTD() {
		return DFECREGSTD;
	}
	public void setDFECREGSTD(Date dFECREGSTD) {
		DFECREGSTD = dFECREGSTD;
	}
	public String getVUSUREGSTD() {
		return VUSUREGSTD;
	}
	public void setVUSUREGSTD(String vUSUREGSTD) {
		VUSUREGSTD = vUSUREGSTD;
	}
	public byte[] getBCARSTD() {
		return BCARSTD;
	}
	public void setBCARSTD(byte[] bCARSTD) {
		BCARSTD = bCARSTD;
	}
	public String getVCUO() {
		return VCUO;
	}
	public void setVCUO(String vCUO) {
		VCUO = vCUO;
	}
	public String getVCUOREF() {
		return VCUOREF;
	}
	public void setVCUOREF(String vCUOREF) {
		VCUOREF = vCUOREF;
	}
	public String getVOBS() {
		return VOBS;
	}
	public void setVOBS(String vOBS) {
		VOBS = vOBS;
	}
	public String getCFLGESTOBS() {
		return CFLGESTOBS;
	}
	public void setCFLGESTOBS(String cFLGESTOBS) {
		CFLGESTOBS = cFLGESTOBS;
	}
	public String getCFLGEST() {
		return CFLGEST;
	}
	public void setCFLGEST(String cFLGEST) {
		CFLGEST = cFLGEST;
	}
	public Date getDFECREG() {
		return DFECREG;
	}
	public void setDFECREG(Date dFECREG) {
		DFECREG = dFECREG;
	}
	public String getVUSUMOD() {
		return VUSUMOD;
	}
	public void setVUSUMOD(String vUSUMOD) {
		VUSUMOD = vUSUMOD;
	}
	public Date getDFECMOD() {
		return DFECMOD;
	}
	public void setDFECMOD(Date dFECMOD) {
		DFECMOD = dFECMOD;
	}
	public String getCFLGENVCAR() {
		return CFLGENVCAR;
	}
	public void setCFLGENVCAR(String cFLGENVCAR) {
		CFLGENVCAR = cFLGENVCAR;
	}
	public String getVIDREMITENTESTD() {
		return VIDREMITENTESTD;
	}
	public void setVIDREMITENTESTD(String vIDREMITENTESTD) {
		VIDREMITENTESTD = vIDREMITENTESTD;
	}
	public String getVIDTRAMITESTD() {
		return VIDTRAMITESTD;
	}
	public void setVIDTRAMITESTD(String vIDTRAMITESTD) {
		VIDTRAMITESTD = vIDTRAMITESTD;
	}
	public String getVNCARSTD() {
		return VNCARSTD;
	}
	public void setVNCARSTD(String vNCARSTD) {
		VNCARSTD = vNCARSTD;
	}
	
}
