package pe.gob.osinfor.pide.ws.services.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dao.DespachoDAO;
import pe.gob.osinfor.pide.ws.dao.DocumentoAnexoDAO;
import pe.gob.osinfor.pide.ws.dao.DocumentoExternoDAO;
import pe.gob.osinfor.pide.ws.dao.DocumentoPrincipalDAO;
import pe.gob.osinfor.pide.ws.dao.impl.DespachoDAOImpl;
import pe.gob.osinfor.pide.ws.dao.impl.DocumentoAnexoDAOImpl;
import pe.gob.osinfor.pide.ws.dao.impl.DocumentoExternoDAOImpl;
import pe.gob.osinfor.pide.ws.dao.impl.DocumentoPrincipalDAOImpl;
import pe.gob.osinfor.pide.ws.dto.DocumentoAnexoSTDDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoFilterDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoLista;
import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.dto.RequestObjectDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;
import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.exception.ValidationException;
import pe.gob.osinfor.pide.ws.services.ConfigService;
import pe.gob.osinfor.pide.ws.services.DespachoService;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaConsultaTramite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class DespachoServiceImpl implements DespachoService {

	public static Logger LOG = LoggerFactory.getLogger(DespachoServiceImpl.class);
	private HashMap<String,String> params = new ConfigServiceImpl().getConfigParameters();

	@Override 
	public List<DocumentoDespachoLista> listarDespacho(DocumentoDespachoFilterDTO filterDTO) throws ServiceException{
		List<DocumentoDespachoLista> listaDetalle  = new ArrayList<>();
		try {
			DespachoDAO dao = new DespachoDAOImpl();
			List<DocumentoDespachoDTO> lista  = null;
			if( !Utilitarios.isBlank(filterDTO.getIdDocument())  ) {
				lista = dao.listarDespachoById(filterDTO.getIdDocument());
			}else if ( !Utilitarios.isBlank(filterDTO.getRuc()) || !Utilitarios.isBlank(filterDTO.getState()) || !Utilitarios.isBlank(filterDTO.getNumDocumentoStd()) || !Utilitarios.isBlank(filterDTO.getNumDocumentoEntidad()) ){
				lista = dao.listarDespachoByFilter(filterDTO);
			}else {	
				lista = dao.listarDespacho();
			}
			for(DocumentoDespachoDTO dp: lista) {
				DocumentoDespachoLista docLista = new DocumentoDespachoLista();
				docLista.setDocumentoDespacho(dp);
				docLista.setDocumentoExterno(dao.getDocumentoDespachoExterno(dp.getSIDEMIEXT()));
				listaDetalle.add(docLista);
			}
			
			return listaDetalle;
		}catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	private String getTokenValue(HttpHeaders headers) {
		String tokenValue = null;
		MultivaluedMap<String, String> listHeader =  headers.getRequestHeaders();
		if(listHeader.get("Authorization")!=null) {
			tokenValue = listHeader.get("Authorization").get(0);
		}
		return tokenValue;
	}
	
	public RequestObjectDTO getDocumentoDespacho(String idDocument) {
		RequestObjectDTO responseResult 		= new RequestObjectDTO();
		try {
			DespachoDAO dao = new DespachoDAOImpl();
			DocumentoDespachoDTO document = dao.getDocumentoDespacho(idDocument);
			JsonObject jsonObject =  new JsonObject();
		    responseResult.setStatus(true);
		    GsonBuilder gsonBuilder = new GsonBuilder();
	        Gson gson = gsonBuilder.create();
	      	String documentString = gson.toJson(document);
		    responseResult.setObjeto(documentString);
			responseResult.setTag(Constants.TAG_DESPACHO);			
		}
		catch (Exception e) {
			responseResult = null;
		}
		return responseResult;
	}
	public DocumentoDespachoDTO getDocumentoDespachoById(String idDocument) {
		DocumentoDespachoDTO document = null;
		try {
			DespachoDAO dao = new DespachoDAOImpl();
			document = dao.listarDespachoById(idDocument).get(0);
		}
		catch (Exception e) {
			document = null;
		}
		return document;
	}
	
	public DocumentoDespachoDTO getDocumentoDespachoObj(String idDocument) {
		DocumentoDespachoDTO document = null;
		try {
			DespachoDAO dao = new DespachoDAOImpl();
			document = dao.getDocumentoDespacho(idDocument);
			
			String PathDocumentDespacho = params.get(ConfigService.PATH_DOCUMENTO_DESPACHO) + "doc_"+document.getVCUO()+".pdf";
			File file = new File(PathDocumentDespacho);
			FileOutputStream fop = new FileOutputStream(file);
			fop.write(document.getBCARSTDREC());
			fop.flush();
			fop.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}
	
	public DocumentoPrincipalDTO getDocumentoPrincipal(String idDocumentDespacho) {
		DocumentoPrincipalDTO document = null;
		try {
			DespachoDAO dao = new DespachoDAOImpl();
			document = dao.getDocumentoPrincipal(idDocumentDespacho);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}
	
	public DocumentoExternoDTO getDocumentoDespachoExterno(String idDocumentDespacho) throws ServiceException{
		DocumentoExternoDTO document = null;
		try {
			DespachoDAO dao = new DespachoDAOImpl();
			document = dao.getDocumentoDespachoExterno(idDocumentDespacho);
		}catch (Exception e) {
			throw new ServiceException(e);
		}
		return document;
	}
	
	public void updateEstadoDocumento(DocumentoDespachoDTO documento,String cuo,String estado) {
		try {
			LOG.info("Modificando el estado del documento - Tramite: [{}], Estado: [{}]", documento.getSIDEMIEXT(), estado);
			DespachoDAO dao = new DespachoDAOImpl();
			dao.updateEstadoDocumento(documento,cuo,estado);
		}catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public void uploadAnexo(String idTramite, InputStream input, FormDataContentDisposition content) throws ServiceException{
		LOG.info("Guardando archivo anexo en la carpeta temporal - Tramite: [{}]", idTramite);
		DocumentoAnexoDAO dao = new DocumentoAnexoDAOImpl();
		try{
			DocumentoAnexoSTDDTO _anexo = dao.findAnexoDespachoByNombre(idTramite, content.getFileName());
			if( _anexo != null ){
				throw new ValidationException("Ya existe un archivo anexo con ese nombre.");
			}
				
			DocumentoExternoDTO externo = getDocumentoDespachoExterno(idTramite);
			List<DocumentoAnexoSTDDTO> anexos = dao.findAnexosByDespacho(idTramite);
			externo.setSNUMANX(anexos.size() + 1);

			DocumentoAnexoSTDDTO anexo = new DocumentoAnexoSTDDTO();
			anexo.setiCodTramite(idTramite);
			anexo.setcNombreDocumento(content.getFileName());
			anexo.setfFecRegistro(new Date());
			anexo.setIdDocumentoExterno(externo.getSIDDOCEXT());
			
			LOG.info("Guardando anexo - Tramite: [{}]", idTramite);
			dao.insertDocumentoAnexo(anexo);
			
			LOG.info("Actualizando documento externo - Externo: [{}]", externo.getSIDDOCEXT());
			DocumentoExternoDAO daoDocExterno = new DocumentoExternoDAOImpl();
			daoDocExterno.updateDocumentoExterno(externo);
			
			Path path = Paths.get(getRepositoryAnexosPath(idTramite), content.getFileName());
			Utilitarios.createDirectoriesIfNotExists(path);
			
			Files.copy(input, path, StandardCopyOption.REPLACE_EXISTING);
			LOG.info("Archivo anexo guardado correctamente - Tramite: [{}]", idTramite);
		}catch(DAOException e){
			throw new ServiceException(e);
		} catch (IOException e) {
			throw new ServiceException(e);
		}
	}
	
	public void removeAnexo(String idTramite, String idAnexo) throws ServiceException{
		LOG.info("Eliminando anexo de la carpeta temporal - Anexo: [{}]", idAnexo);
		DocumentoAnexoDAO dao = new DocumentoAnexoDAOImpl();
		try{
			List<DocumentoAnexoSTDDTO> anexos = dao.findAnexosByDespacho(idTramite);
			if( !anexos.isEmpty() ){
				// Se realiza la busqueda del anexo por su ID
				DocumentoAnexoSTDDTO anexo = null;
				for( DocumentoAnexoSTDDTO a: anexos ){
					if( a.getiCodAnexo().equalsIgnoreCase(idAnexo) ){
						anexo = a;
						break;
					}
				}
				// Si existe es eliminado
				if( anexo != null ){
					LOG.info("Anexo encontrado - Documento externo: [{}], Anexo: [{}]", anexo.getIdDocumentoExterno(), anexo.getcNombreDocumento());
					Path path = Paths.get(getRepositoryAnexosPath(idTramite), anexo.getcNombreDocumento());
					dao.deleteAnexo(anexo.getiCodAnexo(), anexo.getIdDocumentoExterno());
					
					LOG.info("Eliminando archivo - Anexo: [{}], Archivo: [{}]", idAnexo, path.toAbsolutePath().toString());
					Utilitarios.removeFile(path.toAbsolutePath().toString());
				}
			}
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

	@Override
	public String getRepositoryPath(String idTramite) throws ServiceException {
		return params.get(ConfigService.PATH_DOCUMENTO_DESPACHO) + idTramite;
	}

	@Override
	public String getRepositoryAnexosPath(String idTramite) throws ServiceException {
		return getRepositoryPath(idTramite).concat("\\").concat(Constants.SUBCARPETA_ANEXOS);
	}
	
	@Override
	public String getPostPath(String numDocumento) throws ServiceException {
		try{
			String period = Utilitarios.formatterDate(new Date(), params.get(ConfigService.PATH_DOCUMENTO_PUBLICO_SUBCARPETA));
			String targetFolder = Utilitarios.removeCharactersNotAllowed(numDocumento);
			return params.get(ConfigService.PATH_DOCUMENTO_PUBLICO) + "\\" + period + "\\" + targetFolder;
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

	@Override
	public void postDocumentos(String idTramite, String postPath) throws ServiceException{
		LOG.info("Publicando anexos - Tramite: [{}]", idTramite);
		DespachoDAO dao = new DespachoDAOImpl();
		DocumentoPrincipalDAO docPrincipalDao = new DocumentoPrincipalDAOImpl();
		try{
			DocumentoExternoDTO docExterno = dao.getDocumentoDespachoExterno(idTramite);
			DocumentoPrincipalDTO docPrincipal = docPrincipalDao.findDocumentoPrincipalByExterno(docExterno.getSIDDOCEXT());
			
			String targetDocPrincipal = postPath + "\\" + Utilitarios.getFilename(docPrincipal.getVNOMDOC(), Constants.PDF_EXTENSION);
			String sourceAnexos = getRepositoryAnexosPath(idTramite);
			
			Utilitarios.createDirectoriesIfNotExists(Paths.get(postPath));
			
			LOG.info("Copiando documento principal - Tramite: [{}], Destino: [{}]", idTramite, targetDocPrincipal);
			Utilitarios.bytesToFile(docPrincipal.getBPDFDOC(), targetDocPrincipal);
			
			LOG.info("Copiando archivos - Tramite: [{}], Origen: [{}], Destino: [{}]", idTramite, sourceAnexos, postPath);
			Utilitarios.copyFiles(sourceAnexos, postPath);
			
//			LOG.info("Eliminando archivos - Tramite: [{}], Origen: [{}], Destino: [{}]", idTramite, source, target);
//			Utilitarios.removeFile(source);
			docExterno.setVURLDOCANX(postPath);

			DocumentoExternoDAO docExternoDao = new DocumentoExternoDAOImpl();
			docExternoDao.updateDocumentoExterno(docExterno);
			
			LOG.info("Anexos publicados correctamente - Tramite: [{}]", idTramite);
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

	@Override
	public File downloadAnexo(String idAnexo) throws ServiceException {
		DocumentoAnexoDAO anexoDAO = new DocumentoAnexoDAOImpl();
		File file = null;
		try{
			DocumentoAnexoSTDDTO anexo = anexoDAO.findAnexoById(idAnexo);
			if( anexo != null ){
				DocumentoExternoDAO docExternoDAO = new DocumentoExternoDAOImpl();
				DocumentoExternoDTO docExterno = docExternoDAO.findDocumentosExternoById(anexo.getIdDocumentoExterno());
				if( docExterno != null  ){
					String path = getRepositoryAnexosPath(docExterno.getSIDEMIEXT()) + "\\" + anexo.getcNombreDocumento();
					File f = new File(path);
					if ( f != null && f.exists() ){
						file = f;
					}
				}
			}
			return file;
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

	@Override
	public void saveCargo(RespuestaConsultaTramite response) throws ServiceException {
		String cuo = response.getVcuo();
		LOG.info("Guardando cargo - CUO: [{}]", cuo);
		DespachoDAO dao = new DespachoDAOImpl();
		try{
			List<DocumentoDespachoDTO> despachos = dao.findDespachosByCuo(response.getVcuo());
			if( despachos.isEmpty() ){
				throw new ValidationException("No se encontr\u00f3 un documento con el n\u00famero de CUO: " + cuo);
			}else  if( despachos.size() > 1 ){
				throw new ValidationException("Se encontr\u00f3 mas de un documento con el mismo n\u00famero de CUO: " + cuo);
			}

			DocumentoDespachoDTO despacho = despachos.get(0);
			despacho.setVNUMREGSTDREC(response.getVnumregstd());
			despacho.setVANIOREGSTDREC(response.getVanioregstd());
			despacho.setVUNIORGSTDREC(response.getVuniorgstd());
			despacho.setDFECREGSTDREC(Utilitarios.convertToDate(response.getDfecregstd()));
			despacho.setVUSUREGSTDREC(response.getVusuregstd());
			despacho.setBCARSTDREC(response.getBcarstd());
			despacho.setVOBS(response.getVobs());
			despacho.setCFLGEST(Constants.DOCUMENTO_DESPACHO_FLAG_RECIBIDO);
			dao.saveDespacho(despacho);

			LOG.info("Cargo guardado correctamente - Tramite: [{}]", cuo );
		}catch(DAOException e){
			throw new ServiceException(e);
		} catch (ParseException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public DocumentoDespachoDTO findDespachoById(String idTramite) throws ServiceException {
		LOG.info("Buscando despacho - Tramite: [{}]", idTramite);
		DespachoDAO dao = new DespachoDAOImpl();
		try{
			return dao.findDespachoById(idTramite);
		}catch(DAOException e){
			throw new ServiceException(e);
		}
	}

}
