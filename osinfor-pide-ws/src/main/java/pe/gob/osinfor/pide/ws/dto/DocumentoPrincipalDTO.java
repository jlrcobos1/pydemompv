package pe.gob.osinfor.pide.ws.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="IOTDTD_DOC_PRINCIPAL")
public class DocumentoPrincipalDTO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int SIDDOCPRI;
	private String SIDDOCEXT;
	private String VNOMDOC;
	private byte [] BPDFDOC;
	private String CCODEST;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DFECREG;

	public int getSIDDOCPRI() {
		return SIDDOCPRI;
	}

	public void setSIDDOCPRI(int sIDDOCPRI) {
		SIDDOCPRI = sIDDOCPRI;
	}

	public String getSIDDOCEXT() {
		return SIDDOCEXT;
	}

	public void setSIDDOCEXT(String sIDDOCEXT) {
		SIDDOCEXT = sIDDOCEXT;
	}

	public String getVNOMDOC() {
		return VNOMDOC;
	}

	public void setVNOMDOC(String vNOMDOC) {
		VNOMDOC = vNOMDOC;
	}

	public byte[] getBPDFDOC() {
		return BPDFDOC;
	}

	public void setBPDFDOC(byte[] bPDFDOC) {
		BPDFDOC = bPDFDOC;
	}

	public String getCCODEST() {
		return CCODEST;
	}

	public void setCCODEST(String cCODEST) {
		CCODEST = cCODEST;
	}

	public Date getDFECREG() {
		return DFECREG;
	}

	public void setDFECREG(Date dFECREG) {
		DFECREG = dFECREG;
	}
	
}
