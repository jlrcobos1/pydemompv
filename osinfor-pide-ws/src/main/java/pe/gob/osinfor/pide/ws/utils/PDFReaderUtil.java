package pe.gob.osinfor.pide.ws.utils;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class PDFReaderUtil {
	 
	public static String lecturaPDF(String path){
        File file = new File(path);//"C:\\Users\\locadoroti9\\Desktop\\Documentacion\\d_PIDE0001-PlantillaPIDE_2019-01-09.pdf");
        String clave = "";
        
        try {
              PDDocument pd = PDDocument.load(file); 
              PDFTextStripper pdfStripper = new PDFTextStripper();
              String text = pdfStripper.getText(pd);
              int indexInitial =  text.indexOf("clave-std:");
              //System.out.println("indice: "+indexInitial);
              String keyword = text.substring(indexInitial+11,indexInitial+18);
              //System.out.println("clave: "+keyword);
              clave = keyword;
              pd.close();
          } catch (IOException e) {
              clave = "";
        	  e.printStackTrace();
          }      
        return clave;
    }
	 
    /*public static void main(String [] a) {
	   PDFReaderUtil pdfReader = new PDFReaderUtil();
	   pdfReader.lecturaPDF();
    }*/
	 
}
