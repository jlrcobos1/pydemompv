package pe.gob.osinfor.pide.ws.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pe.gob.osinfor.pide.ws.dao.DocumentoExternoDAO;
import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;

public class DocumentoExternoDAOImpl implements DocumentoExternoDAO  {
	
	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("OSINFOR-PIDE");

	@Override
	public void updateDocumentoExterno(DocumentoExternoDTO documento) throws DAOException {
		EntityManager em = factory.createEntityManager();
		EntityTransaction tx = em.getTransaction();
        try {
			tx.begin();
			em.merge(documento);
			em.flush();
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new DAOException(e);
		}
	}

	@Override
	public DocumentoExternoDTO findDocumentosExternoByTramite(String idTramite) throws DAOException {
		try {
			String sql = "select dex from DocumentoExternoDTO dex WHERE de.SIDEMIEXT = :SIDEMIEXT";
			EntityManager em = factory.createEntityManager();

			Query query = em.createQuery(sql);
			query.setParameter("SIDEMIEXT", idTramite);
			return (DocumentoExternoDTO) query.getSingleResult();
		} catch (NoResultException | NonUniqueResultException nre) {
			return null;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public DocumentoExternoDTO findDocumentosExternoById(String idDocumento) throws DAOException {
		try {
			String sql = "select dex from DocumentoExternoDTO dex WHERE dex.SIDDOCEXT = :SIDDOCEXT";
			EntityManager em = factory.createEntityManager();

			Query query = em.createQuery(sql);
			query.setParameter("SIDDOCEXT", idDocumento);

			return (DocumentoExternoDTO) query.getSingleResult();
		} catch (NoResultException | NonUniqueResultException nre) {
			return null;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
}
