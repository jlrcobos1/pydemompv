package pe.gob.osinfor.pide.ws.dto;

import java.io.Serializable;

public class TramiteDigitalDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long iCodDigital;
	private String iCodTramite;
	private String cNombreOriginal;
	private String cNombreNuevo;
	private String iCodMovimiento;
	private String sidrecext;
	
	public Long getiCodDigital() {
		return iCodDigital;
	}
	public void setiCodDigital(Long iCodDigital) {
		this.iCodDigital = iCodDigital;
	}
	public String getiCodTramite() {
		return iCodTramite;
	}
	public void setiCodTramite(String iCodTramite) {
		this.iCodTramite = iCodTramite;
	}
	public String getcNombreOriginal() {
		return cNombreOriginal;
	}
	public void setcNombreOriginal(String cNombreOriginal) {
		this.cNombreOriginal = cNombreOriginal;
	}
	public String getcNombreNuevo() {
		return cNombreNuevo;
	}
	public void setcNombreNuevo(String cNombreNuevo) {
		this.cNombreNuevo = cNombreNuevo;
	}
	public String getiCodMovimiento() {
		return iCodMovimiento;
	}
	public void setiCodMovimiento(String iCodMovimiento) {
		this.iCodMovimiento = iCodMovimiento;
	}
	public String getSidrecext() {
		return sidrecext;
	}
	public void setSidrecext(String sidrecext) {
		this.sidrecext = sidrecext;
	}
	
}
