package pe.gob.osinfor.pide.ws.services.autenticacion;
import pe.gob.osinfor.pide.ws.dto.TokenAccessDto;
import java.text.ParseException;

public interface ITokenDropAccess {
  Integer deleteTokenAccess(TokenAccessDto entity);
}
