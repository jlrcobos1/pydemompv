package pe.gob.osinfor.pide.ws.dto;

import java.io.Serializable;

public class RemitenteDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long iCodRemitente;
	private String cNombre;
	private String nNumDocumento;
	private String cDireccion;
	
	public Long getiCodRemitente() {
		return iCodRemitente;
	}
	public void setiCodRemitente(Long iCodRemitente) {
		this.iCodRemitente = iCodRemitente;
	}
	public String getcNombre() {
		return cNombre;
	}
	public void setcNombre(String cNombre) {
		this.cNombre = cNombre;
	}
	public String getnNumDocumento() {
		return nNumDocumento;
	}
	public void setnNumDocumento(String nNumDocumento) {
		this.nNumDocumento = nNumDocumento;
	}
	public String getcDireccion() {
		return cDireccion;
	}
	public void setcDireccion(String cDireccion) {
		this.cDireccion = cDireccion;
	}

}
