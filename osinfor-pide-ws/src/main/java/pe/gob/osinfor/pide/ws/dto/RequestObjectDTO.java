package pe.gob.osinfor.pide.ws.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RequestJersey")
public class RequestObjectDTO {

	private String tag;
	private Boolean status;
	private String objeto;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getObjeto() {
		return objeto;
	}
	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}
	
	
}
