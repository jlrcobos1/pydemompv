package pe.gob.osinfor.pide.ws.dto;

public class ConsultaTramiteDTO {
	
	private String cuo;
	private String rucRemitente;
	private String rucDestino;
	public String getCuo() {
		return cuo;
	}
	public void setCuo(String cuo) {
		this.cuo = cuo;
	}
	public String getRucRemitente() {
		return rucRemitente;
	}
	public void setRucRemitente(String rucRemitente) {
		this.rucRemitente = rucRemitente;
	}
	public String getRucDestino() {
		return rucDestino;
	}
	public void setRucDestino(String rucDestino) {
		this.rucDestino = rucDestino;
	}
	
	
	
}
