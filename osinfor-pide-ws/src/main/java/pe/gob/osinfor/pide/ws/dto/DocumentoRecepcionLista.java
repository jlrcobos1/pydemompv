package pe.gob.osinfor.pide.ws.dto;

public class DocumentoRecepcionLista {

	private DocumentoRecepcionDTO documentoRecepcion;
	private DocumentoExternoDTO documentoExterno;
	
	
	public DocumentoRecepcionDTO getDocumentoRecepcion() {
		return documentoRecepcion;
	}
	public void setDocumentoRecepcion(DocumentoRecepcionDTO documentoRecepcion) {
		this.documentoRecepcion = documentoRecepcion;
	}
	public DocumentoExternoDTO getDocumentoExterno() {
		return documentoExterno;
	}
	public void setDocumentoExterno(DocumentoExternoDTO documentoExterno) {
		this.documentoExterno = documentoExterno;
	}
	
	
}
