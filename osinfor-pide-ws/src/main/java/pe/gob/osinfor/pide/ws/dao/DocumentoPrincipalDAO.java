package pe.gob.osinfor.pide.ws.dao;

import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;

public interface DocumentoPrincipalDAO {
	DocumentoPrincipalDTO findDocumentoPrincipalByExterno(String idExterno) throws DAOException;
}
