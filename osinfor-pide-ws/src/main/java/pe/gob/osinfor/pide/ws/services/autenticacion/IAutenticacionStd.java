package pe.gob.osinfor.pide.ws.services.autenticacion;

import pe.gob.osinfor.pide.ws.dto.UsuarioDTO;

import java.io.IOException;
import java.net.MalformedURLException;

public interface IAutenticacionStd {

  /**
  * Obtiene datos del usuario autenticado
  * del Sistema de Tramite Documentario
  * @see UsuarioDTO
  */
  UsuarioDTO obtenerUsuarioSesionStd() throws IOException;

}
