package pe.gob.osinfor.pide.ws.utils;

public class Constants {
	public static String TAG_DESPACHO = "WS_DESPACHO";
	public static String TAG_RECEPCION = "WS_RECEPCION";
	public static String TAG_VISOR = "WS_VISOR_DOCUMENTOS";
	public static String TAG_SECURITY = "WS_SECURITY";
	public static String DOCUMENTO_DESPACHO_FLAG_RECIBIDO = "R";
	public static String DOCUMENTO_DESPACHO_FLAG_ENVIADO = "E";
	
	public static String DOCUMENTO_RECEPCION_FLAG_RECIBIDO = "R";
	public static String DOCUMENTO_RECEPCION_FLAG_OBSERVADO = "O";
	
	public static String WS_IOPIDE_TRAMITE_RESPONSE_OK = "0000";
	
	public static String PIDE_TIPO_DOCUMENTO_OFICIO = "01";
	public static String PIDE_TIPO_DOCUMENTO_CARTA = "02";
	
	public static final int STD_TIPO_DOCUMENTO_OFICIO = 20;
	public static final int STD_TIPO_DOCUMENTO_OFICIO_MULTIPLE = 22;
	public static final int STD_TIPO_DOCUMENTO_CARTA = 2;
	public static final int STD_TIPO_DOCUMENTO_CARTA_MULTIPLE = 3;
	
	public static String SUBCARPETA_ANEXOS = "Anexos";
	
	public static String PDF_EXTENSION = ".pdf";
	
	public static String DEPLOY_MODE_PROD = "PROD";
	public static String DEPLOY_MODE_TEST = "TEST";
	public static String DEPLOY_MODE_DEV = "DEV";
}
