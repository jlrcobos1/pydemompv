package pe.gob.osinfor.pide.ws.dto;

public class DocumentoDespachoLista {

	private DocumentoDespachoDTO documentoDespacho;
	private DocumentoExternoDTO documentoExterno;
	public DocumentoDespachoDTO getDocumentoDespacho() {
		return documentoDespacho;
	}
	public void setDocumentoDespacho(DocumentoDespachoDTO documentoDespacho) {
		this.documentoDespacho = documentoDespacho;
	}
	public DocumentoExternoDTO getDocumentoExterno() {
		return documentoExterno;
	}
	public void setDocumentoExterno(DocumentoExternoDTO documentoExterno) {
		this.documentoExterno = documentoExterno;
	}
	
	
}
