package pe.gob.osinfor.pide.ws.services;

import java.util.List;

import pe.gob.osinfor.pide.ws.dto.DocumentoAnexoSTDDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;

public interface DocumentoAnexoService {

	List<DocumentoAnexoSTDDTO> findAnexosByDespacho(String idTramite) throws ServiceException;
	List<DocumentoAnexoSTDDTO> findAnexosByRecepcion(String idTramite) throws ServiceException;

}
