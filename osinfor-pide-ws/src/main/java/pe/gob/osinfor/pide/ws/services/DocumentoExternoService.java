package pe.gob.osinfor.pide.ws.services;

import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;

public interface DocumentoExternoService {
	void updateDocumentoExterno(DocumentoExternoDTO documento) throws ServiceException;
	DocumentoExternoDTO findDocumentosExternoById(String idTramite) throws ServiceException;
}
