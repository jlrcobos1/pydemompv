package pe.gob.osinfor.pide.ws.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import pe.gob.osinfor.pide.ws.exception.DAOException;

public class Transaction {
	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("OSINFOR-PIDE");
	private EntityManager em = null;
	private EntityTransaction tx = null;
	
	public void begin(){
		this.em = factory.createEntityManager();
		this.tx = em.getTransaction();
		this.tx.begin();
	}
	
	public void commit(){
		this.tx.commit();
	}
	
	
	public void rollback(){
		try{
			this.tx.rollback();			
		}catch(Exception e){
			throw new DAOException(e);
		}

	}

	public EntityManager getEntityManager() {
		return em;
	}

}
