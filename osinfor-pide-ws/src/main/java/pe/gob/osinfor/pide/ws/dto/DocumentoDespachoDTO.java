package pe.gob.osinfor.pide.ws.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="IOTDTC_DESPACHO")
public class DocumentoDespachoDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String	SIDEMIEXT;
	private String 	
	VNUMREGSTD,
	VANIOREGSTD,
	CTIPDOCIDEREM,
	VNUMDOCIDEREM,
	VCODUNIORGREM,
	VUNIORGREM,
	VCUO,
	VRUCENTREC,
	VNOMENTREC,
	VNUMREGSTDREC,
	VANIOREGSTDREC,
	VUSUREGSTDREC,
	VUNIORGSTDREC,
	VOBS,
	VCUOREF,
	CFLGEST,
	CFLGENV,
	VUSUREG,
	VUSUMOD;
		
	@Temporal(TemporalType.DATE)
	private Date DFECREGSTDREC;
	@Temporal(TemporalType.DATE)
	private Date DFECENV;
	@Temporal(TemporalType.DATE)
	private Date DFECREG;
	@Temporal(TemporalType.DATE)
	private Date DFECMOD;
	private byte [] BCARSTDREC;
	
	public String getSIDEMIEXT() {
		return SIDEMIEXT;
	}
	public void setSIDEMIEXT(String sIDEMIEXT) {
		SIDEMIEXT = sIDEMIEXT;
	}
	public String getVNUMREGSTD() {
		return VNUMREGSTD;
	}
	public void setVNUMREGSTD(String vNUMREGSTD) {
		VNUMREGSTD = vNUMREGSTD;
	}
	public String getVANIOREGSTD() {
		return VANIOREGSTD;
	}
	public void setVANIOREGSTD(String vANIOREGSTD) {
		VANIOREGSTD = vANIOREGSTD;
	}
	public String getCTIPDOCIDEREM() {
		return CTIPDOCIDEREM;
	}
	public void setCTIPDOCIDEREM(String cTIPDOCIDEREM) {
		CTIPDOCIDEREM = cTIPDOCIDEREM;
	}
	public String getVNUMDOCIDEREM() {
		return VNUMDOCIDEREM;
	}
	public void setVNUMDOCIDEREM(String vNUMDOCIDEREM) {
		VNUMDOCIDEREM = vNUMDOCIDEREM;
	}
	public String getVCODUNIORGREM() {
		return VCODUNIORGREM;
	}
	public void setVCODUNIORGREM(String vCODUNIORGREM) {
		VCODUNIORGREM = vCODUNIORGREM;
	}
	public String getVUNIORGREM() {
		return VUNIORGREM;
	}
	public void setVUNIORGREM(String vUNIORGREM) {
		VUNIORGREM = vUNIORGREM;
	}
	public String getVCUO() {
		return VCUO;
	}
	public void setVCUO(String vCUO) {
		VCUO = vCUO;
	}
	public String getVRUCENTREC() {
		return VRUCENTREC;
	}
	public void setVRUCENTREC(String vRUCENTREC) {
		VRUCENTREC = vRUCENTREC;
	}
	public String getVNOMENTREC() {
		return VNOMENTREC;
	}
	public void setVNOMENTREC(String vNOMENTREC) {
		VNOMENTREC = vNOMENTREC;
	}
	public String getVNUMREGSTDREC() {
		return VNUMREGSTDREC;
	}
	public void setVNUMREGSTDREC(String vNUMREGSTDREC) {
		VNUMREGSTDREC = vNUMREGSTDREC;
	}
	public String getVANIOREGSTDREC() {
		return VANIOREGSTDREC;
	}
	public void setVANIOREGSTDREC(String vANIOREGSTDREC) {
		VANIOREGSTDREC = vANIOREGSTDREC;
	}
	public String getVUSUREGSTDREC() {
		return VUSUREGSTDREC;
	}
	public void setVUSUREGSTDREC(String vUSUREGSTDREC) {
		VUSUREGSTDREC = vUSUREGSTDREC;
	}
	public String getVUNIORGSTDREC() {
		return VUNIORGSTDREC;
	}
	public void setVUNIORGSTDREC(String vUNIORGSTDREC) {
		VUNIORGSTDREC = vUNIORGSTDREC;
	}
	public String getVOBS() {
		return VOBS;
	}
	public void setVOBS(String vOBS) {
		VOBS = vOBS;
	}
	public String getVCUOREF() {
		return VCUOREF;
	}
	public void setVCUOREF(String vCUOREF) {
		VCUOREF = vCUOREF;
	}
	public String getCFLGEST() {
		return CFLGEST;
	}
	public void setCFLGEST(String cFLGEST) {
		CFLGEST = cFLGEST;
	}
	public String getCFLGENV() {
		return CFLGENV;
	}
	public void setCFLGENV(String cFLGENV) {
		CFLGENV = cFLGENV;
	}
	public String getVUSUREG() {
		return VUSUREG;
	}
	public void setVUSUREG(String vUSUREG) {
		VUSUREG = vUSUREG;
	}
	public String getVUSUMOD() {
		return VUSUMOD;
	}
	public void setVUSUMOD(String vUSUMOD) {
		VUSUMOD = vUSUMOD;
	}
	public Date getDFECREGSTDREC() {
		return DFECREGSTDREC;
	}
	public void setDFECREGSTDREC(Date dFECREGSTDREC) {
		DFECREGSTDREC = dFECREGSTDREC;
	}
	public Date getDFECENV() {
		return DFECENV;
	}
	public void setDFECENV(Date dFECENV) {
		DFECENV = dFECENV;
	}
	public Date getDFECREG() {
		return DFECREG;
	}
	public void setDFECREG(Date dFECREG) {
		DFECREG = dFECREG;
	}
	public Date getDFECMOD() {
		return DFECMOD;
	}
	public void setDFECMOD(Date dFECMOD) {
		DFECMOD = dFECMOD;
	}
	public byte[] getBCARSTDREC() {
		return BCARSTDREC;
	}
	public void setBCARSTDREC(byte[] bCARSTDREC) {
		BCARSTDREC = bCARSTDREC;
	}
	
	
}
	