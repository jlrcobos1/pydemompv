package pe.gob.osinfor.pide.ws.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pe.gob.osinfor.pide.ws.dao.DocumentoPrincipalDAO;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;

public class DocumentoPrincipalDAOImpl implements DocumentoPrincipalDAO  {
	
	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("OSINFOR-PIDE");

	@Override
	public DocumentoPrincipalDTO findDocumentoPrincipalByExterno(String idExterno) throws DAOException {
		try{
	        String sql = "select dp from DocumentoPrincipalDTO dp WHERE dp.SIDDOCEXT = :SIDDOCEXT";
	        EntityManager em = factory.createEntityManager();
	        
	        Query query = em.createQuery(sql);
	        query.setParameter("SIDDOCEXT", idExterno);
	        return (DocumentoPrincipalDTO) query.getSingleResult();
		}catch(NoResultException e){
			return null;
		}catch(Exception e){
			throw new DAOException(e);
		}
	}
	
}
