package pe.gob.osinfor.pide.ws.services.autenticacion.impl;

import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.osinfor.pide.ws.dto.TokenAccessDto;
import pe.gob.osinfor.pide.ws.exception.DAOException;
import pe.gob.osinfor.pide.ws.services.autenticacion.ITokenAccess;
import pe.gob.osinfor.pide.ws.services.impl.DocumentoAnexoServiceImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TokenAccessImpl implements ITokenAccess {

  public static Logger LOG = LoggerFactory.getLogger(TokenAccessImpl.class);

  private EntityManagerFactory factory = Persistence
          .createEntityManagerFactory("OSINFOR-PIDE");

  @Override
  public TokenAccessDto getTokenAccessUser(String token) throws DAOException, ParseException {
    EntityManager em = factory.createEntityManager();
    Query q = em.createQuery(getQueryString(token,"OSINFOR-PIDE-MESAPARTE",new Date()));
    List resultList = q.getResultList();
    em.close();
    return (resultList.size()>0? (TokenAccessDto) resultList.get(0) :null);
  }

  private String getQueryString(String token,String scope,Date expirationDate) {
    QueryToken queryToken = new QueryToken();
    queryToken.addToken(token);
    queryToken.addScope(scope);
    queryToken.addTimeExpiration(expirationDate);
    LOG.info(queryToken.buildQuery());
    return queryToken.buildQuery();
  }

  private class QueryToken {
    private String query;

    public QueryToken() {
      this.query = query = "select t from TokenAccessDto t " +
          " where 1=1 ";
    }

    void addScope(String scope) {
      query += " and t.scope = '"+ scope+"'";
    }

    void addTimeExpiration(Date date) {
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
      query += " and t.expires >= '"+sdf.format(date)+"'";
    }

    void addToken(String token) {
       query += " and t.token = '"+token+"'";
    }

    String buildQuery() {
     return query;
    }
  }

}
