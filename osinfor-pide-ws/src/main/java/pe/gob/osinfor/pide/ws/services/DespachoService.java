package pe.gob.osinfor.pide.ws.services;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.inject.Named;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoFilterDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoLista;
import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.dto.RequestObjectDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaConsultaTramite;

@Named
public interface DespachoService {

	List<DocumentoDespachoLista> listarDespacho(DocumentoDespachoFilterDTO filterDTO) throws ServiceException;
	RequestObjectDTO getDocumentoDespacho(String idDocument);
	DocumentoDespachoDTO getDocumentoDespachoObj(String idDocument);
	DocumentoDespachoDTO getDocumentoDespachoById(String idDocument);
	DocumentoPrincipalDTO getDocumentoPrincipal(String idDocumentDespacho);
	void updateEstadoDocumento(DocumentoDespachoDTO documento,String cuo,String estado) throws ServiceException;
	DocumentoExternoDTO getDocumentoDespachoExterno(String idDocumentDespacho) throws ServiceException;
	
	void uploadAnexo(String idTramite, InputStream input, FormDataContentDisposition content) throws ServiceException;
	String getPostPath(String numDocumento) throws ServiceException;
	String getRepositoryPath(String idTramite) throws ServiceException;
	String getRepositoryAnexosPath(String idTramite) throws ServiceException;
	void postDocumentos(String idTramite, String postPath) throws ServiceException;
	void removeAnexo(String idTramite, String idAnexo) throws ServiceException;
	File downloadAnexo(String idAnexo) throws ServiceException;
	void saveCargo(RespuestaConsultaTramite response) throws ServiceException;
	DocumentoDespachoDTO findDespachoById(String idTramite) throws ServiceException;
		
}
