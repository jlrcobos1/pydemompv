package pe.gob.osinfor.pide.ws.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dto.ResponseObjectDTO;
import pe.gob.osinfor.pide.ws.services.WsService;
import pe.gob.osinfor.pide.ws.services.impl.WsServiceImpl;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.osinfor.pide.ws.utils.ResponseUtils;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;
import pe.gob.segdi.wsentidad.ws.EntidadBean;

@Path("/")
public class PideController {

	public static Logger LOG = LoggerFactory.getLogger(DespachoController.class);
	private WsService wsService = new WsServiceImpl();
	
	@POST
	@Path("osinfor/listar-entidades-pide")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO listarEntidadPide(@Context UriInfo uriInfo) {
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_DESPACHO);
		LOG.info("Buscando entidades");
		try {
			EntidadBean[] lista = wsService.findEntidades();
			response.setObjeto(Utilitarios.convertToJson(lista));	
			response.setStatus(true);
		} catch (Exception e) {
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al buscar las entidades");
			LOG.error("Ocurrio un error inesperado al buscar las entidades", e);
		}
		return response;
	}
}
