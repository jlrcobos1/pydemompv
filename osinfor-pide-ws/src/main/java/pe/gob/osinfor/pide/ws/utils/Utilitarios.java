package pe.gob.osinfor.pide.ws.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/**
* Resumen.
* Objeto : Utilitarios.
* Descripcion : Contiene los metodos de uso comun para todas las clases que forman parte del proyecto.
* Fecha de Creacion : 07/02/2019.
* ------------------------------------------------------------------------
* Modificaciones
* Fecha            Nombre                     Descripcion
* ------------------------------------------------------------------------
* 
*/

public class Utilitarios {

    
    /**
     * Obtiene el valor de un archivo properties.
     * @param archivo Nombre del archivo properties, tipo String.
     * @param dato Identificador de la linea del properties, tipo String.
     * @return variable Cadena obtenida del propwerties, tipo String.
     * @throws Exception Retorna mensaje de errror.
     */
     public static String ObtenerDatosProperties(String archivo, String dato) {
        String variable = "";
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("pe.gob.segdi.wsiotramite.messageresource."+archivo);
            variable = bundle.getString(dato);
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return variable;
     }
     
     

     public static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
    }
     
     public static String verifyNullObjectToString(Object data) {
    	 String result = "";
    	 if(data != null) {
    		 result = data.toString();
    	 }
    	 return result;
     }
     public static int verifyNullObjectToInt(Object data) {
    	 int result = 0;
    	 if(data != null) {
    		 result = Integer.parseInt(data.toString());
    	 }
    	 return result;
     }
     public static String getDateString(Date date) {
         if (date == null) {
             return "";
         }
         SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
         //dateFormat.parse()
         return dateFormat.format(date);
     }
     
     public static String getDateString2(Date date) {
         if (date == null) {
             return "";
         }
         SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
         //dateFormat.parse()
         return dateFormat.format(date);
     }
     
     public static void writeToFile(InputStream uploadedInputStream,
             String uploadedFileLocation) {
         try {
             OutputStream out = new FileOutputStream(new File(
                     uploadedFileLocation));
             int read = 0;
             byte[] bytes = new byte[1024];

             out = new FileOutputStream(new File(uploadedFileLocation));
             while ((read = uploadedInputStream.read(bytes)) != -1) {
                 out.write(bytes, 0, read);
             }
             out.flush();
             out.close();
         } catch (IOException e) {

             e.printStackTrace();
         }

     }
     
     public static String trim(String param) {
    	 if( param != null ) {
    		 return param.trim();
    	 }
    	 return param;
     }
     
     public static String toEmptyIfNull(String param) {
    	 if( param != null ) {
    		 return param.trim();
    	 }
    	 return "";
     }

     public static Date convertToDate(XMLGregorianCalendar calendar) throws ParseException {
//    	 Date date = calendar.toGregorianCalendar().getTime();
//    	 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:MM:ss");
//    	 String dateString = format.format(date);
//    	 return format.parse(dateString); 
    	 return calendar.toGregorianCalendar().getTime();
     }
     
     public static XMLGregorianCalendar convertToXMLGregorianCalendar(Date date) throws DatatypeConfigurationException {
    	 XMLGregorianCalendar result = null;
    	 if( date != null ){
        	 GregorianCalendar calendar = new GregorianCalendar();
        	 calendar.setTime(date);
        	 
        	 DatatypeFactory df = DatatypeFactory.newInstance();
        	 result = df.newXMLGregorianCalendar(calendar);
    	 }
    	 return result;
     }

	public static String convertToJson(Object obj) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		return gson.toJson(obj);
	}
	
	public static void createDirectoriesIfNotExists(Path path) throws IOException{
        boolean dirExists = Files.exists(path);
        if( !dirExists ) {
            Files.createDirectories(path);
        }
	}
	
	public static void copyFiles(String source, String target) throws IOException{
		File folder = new File(source);
		if( folder != null && folder.listFiles() != null){
		    for (File entry: folder.listFiles()) {
		        if ( !entry.isDirectory() ) {
		        	Path sourcePath = Paths.get(source, entry.getName());
		        	Path targetPath = Paths.get(target, entry.getName());
		        	createDirectoriesIfNotExists(targetPath);
		        	Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
		        }
		    }
		}
	}
	
	public static void removeFile(String source){
	    File element = new File(source);
	    if( element != null ){
		    if (element.isDirectory()) {
		        for (File sub : element.listFiles()) {
		        	removeFile(sub.getAbsolutePath());
		        }
		    }
		    element.delete();
	    }
	}
	
	public static String removeCharactersNotAllowed(String value){
		value = value.trim();
		value = value.replaceAll("/", "-");
		value = value.replaceAll(":", "-");
		value = value.replaceAll("\\*", "-");
		return value;
	}
	
	public static String getFilename(String filename, String extension){
		filename = removeCharactersNotAllowed(filename);
		extension = extension.toLowerCase();
		if( filename.toLowerCase().endsWith(extension) ){
			return filename;
		}else{
			return filename + extension;
		}
	}
	
	public static String toString(Object value){
		if( value != null ){
			return value.toString();
		}
		return null;
	}
	
	public static Long toLong(Object value){
		if( value != null ){
			return Long.valueOf(value.toString());
		}
		return null;
	}
	
	public static String formatterDate(Date date, String format){
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}
	
	public static boolean isBlank(String value){
		if( value == null || value.trim().equals("") ){
			return true;
		}
		return false;
	}

	public static boolean isNumberBlank(String value){
		if( value == null || value.trim().equalsIgnoreCase("")  || value.trim().equalsIgnoreCase("0")){
			return true;
		}
		return false;
	}
	
	public static String getTipoDocumentoPIDE(String tipo){
		// Debido a un error que se origina al guardar el tipo de documento en el STD
		if( tipo.startsWith("0") ){
			return tipo;
		}
		
		String tipoDocumentoPide = null;
		switch (Integer.valueOf(tipo)) {
		case Constants.STD_TIPO_DOCUMENTO_OFICIO:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_OFICIO;
			break;
		case Constants.STD_TIPO_DOCUMENTO_OFICIO_MULTIPLE:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_OFICIO;
			break;
		case Constants.STD_TIPO_DOCUMENTO_CARTA:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_CARTA;
			break;
		case Constants.STD_TIPO_DOCUMENTO_CARTA_MULTIPLE:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_CARTA;
			break;
		default:
			tipoDocumentoPide = Constants.PIDE_TIPO_DOCUMENTO_OFICIO;
			break;
		}
		
		return tipoDocumentoPide;
	}
	
	public static File createTempFile(byte[] bytes, String extension) throws IOException{
		final Path path = Files.createTempFile("myTempFile", extension);
		Files.write(path, bytes);
		return path.toFile();
	}
	
	public static byte[] encodeBase64(byte[] bytes){
		byte[] result = null;
		if( bytes != null ){
			return Base64.getEncoder().encode(bytes);
		}
		return result;
	}
	
	public static void downloadFile(String source, String target) throws MalformedURLException, IOException{
		if( source.startsWith("C:") || source.startsWith("D:") ){
			Files.copy(Paths.get(source), Paths.get(target), StandardCopyOption.REPLACE_EXISTING);
		}else{
			FileUtils.copyURLToFile(new URL(source), new File(target));
		}		
	}
	
	public static void bytesToFile(byte[] bytes, String target) throws IOException{
		Files.write(Paths.get(target), bytes);
	}
	
	public static int booleanToInt(String value){
		int result = 0;
		if( value == null || value.equals("") ){
			result = 0;
		}else{
			result = value.equalsIgnoreCase("true") ? 1: 0;
		}
		return result;
	}
}
