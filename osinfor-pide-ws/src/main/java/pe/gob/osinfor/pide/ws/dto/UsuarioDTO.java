package pe.gob.osinfor.pide.ws.dto;

import lombok.Data;


@Data
public class UsuarioDTO implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private String id;
	private String usuario;
	private String nombres;
	private String token;
	private String rol;

	public UsuarioDTO() {
	}

	public UsuarioDTO(String id, String usuario,
					  String nombres, String token,
					  String rol) {
		this.id = id;
		this.usuario = usuario;
		this.nombres = nombres;
		this.token = token;
		this.rol = rol;
	}
}
