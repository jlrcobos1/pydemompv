package pe.gob.osinfor.pide.ws.dao;

import java.util.List;

import pe.gob.osinfor.pide.ws.dto.DocumentoAnexoSTDDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;

public interface DocumentoAnexoDAO {
	void insertDocumentoAnexo(DocumentoAnexoSTDDTO documento) throws DAOException;
	void deleteAnexo(String idAnexo, String idDocumentoExterno) throws DAOException;
	DocumentoAnexoSTDDTO findAnexoById(String idAnexo) throws DAOException;
	List<DocumentoAnexoSTDDTO> findAnexosByDespacho(String idTramite) throws DAOException;
	List<DocumentoAnexoSTDDTO> findAnexosByRecepcion(String idTramite) throws DAOException;
	DocumentoAnexoSTDDTO findAnexoDespachoByNombre(String idTramite, String nombre) throws DAOException;
}
