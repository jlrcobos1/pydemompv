package pe.gob.osinfor.pide.ws.rest.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.osinfor.pide.ws.dto.ResponseObjectDTO;
import pe.gob.osinfor.pide.ws.dto.TokenAccessDto;
import pe.gob.osinfor.pide.ws.services.autenticacion.IAutenticacionStd;
import pe.gob.osinfor.pide.ws.services.autenticacion.ITokenAccess;
import pe.gob.osinfor.pide.ws.services.autenticacion.ITokenDropAccess;
import pe.gob.osinfor.pide.ws.services.autenticacion.impl.AutenticacionStd;
import pe.gob.osinfor.pide.ws.services.autenticacion.impl.TokenAccessImpl;
import pe.gob.osinfor.pide.ws.services.autenticacion.impl.TokenDropAccessImpl;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.osinfor.pide.ws.utils.ResponseUtils;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

@Path("/osinfor/security")
public class LoginUserController {

	public static Logger LOG = LoggerFactory.getLogger(LoginUserController.class);
	private final IAutenticacionStd autenticacionStd = new AutenticacionStd();
	private final ITokenAccess tokenAccess = new TokenAccessImpl();
	private final ITokenDropAccess tokenDropAccess = new TokenDropAccessImpl();


	@POST
	@Path("authenticate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO authenticate(@QueryParam("token") String token) {
		LOG.info("Autenticando usuario - Token: [{}]", token);
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_SECURITY);
		try{
			response.setStatus(false);
			if( Utilitarios.isBlank(token) ){
				ResponseUtils.incorrect(response, "El token no es valido");
				LOG.info("Autenticacion fallida - Token: [{}]", token);
			} else{
				TokenAccessDto tokenAccessUser = tokenAccess.getTokenAccessUser(token);
				LOG.info("Autenticacion realizada correctamente - Token: [{}]", token);
				if (tokenAccessUser != null) {
					if (!validateExpirationToken(tokenAccessUser)) {
						tokenDropAccess.deleteTokenAccess(tokenAccessUser);
					}
					ResponseUtils.ok(response, tokenAccessUser, "Autenticacion realizada correctamente");
				} else {
					ResponseUtils.ok(response, null, "Ha fallado la autenticacion, revise los datos ingresados");
				}
			}
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al autenticar el usuario");
			LOG.error("Ocurrio un error inesperado al autenticar el usuario", e);
		}
		return response;
	}

	private boolean validateExpirationToken(TokenAccessDto tokenAccessDto) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date now = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
		return now.before(tokenAccessDto.getExpires());
	}
}
