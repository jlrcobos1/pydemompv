package pe.gob.osinfor.pide.ws.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pe.gob.osinfor.pide.ws.dao.DocumentoAnexoDAO;
import pe.gob.osinfor.pide.ws.dto.DocumentoAnexoSTDDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;

public class DocumentoAnexoDAOImpl implements DocumentoAnexoDAO  {

	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("OSINFOR-PIDE");
	
	public void insertDocumentoAnexo(DocumentoAnexoSTDDTO anexo) throws DAOException{
		EntityManager em = factory.createEntityManager();
		EntityTransaction tx = em.getTransaction();
        try {
        	tx.begin();
        	
        	String sql = "INSERT INTO IOTDTD_ANEXO(VNOMDOC, DFECREG, SIDDOCEXT) VALUES (?,?,?)";
			
			Query query = em.createNativeQuery(sql);
			query.setParameter(1, anexo.getcNombreDocumento());
			query.setParameter(2, anexo.getfFecRegistro());
			query.setParameter(3, anexo.getIdDocumentoExterno());
			query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new DAOException(e);
		}
	}
		
	@Override
	public void deleteAnexo(String idAnexo, String idDocumentoExterno) throws DAOException{
		EntityManager em = factory.createEntityManager();
		EntityTransaction tx = em.getTransaction();
        try {
			tx.begin();
			
			String sql = "DELETE FROM IOTDTD_ANEXO WHERE SIDDOCANX = ?";
			
			Query query = em.createNativeQuery(sql);
			query.setParameter(1, idAnexo);
			query.executeUpdate();
			
			sql = "UPDATE IOTDTM_DOC_EXTERNO SET SNUMANX = SNUMANX - 1 WHERE SIDDOCEXT = ?";
			
			query = em.createNativeQuery(sql);
			query.setParameter(1, idDocumentoExterno);
			query.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new DAOException(e);
		}
	}
	
	@Override
	public List<DocumentoAnexoSTDDTO> findAnexosByDespacho(String idTramite) throws DAOException{
		List<DocumentoAnexoSTDDTO> result = new ArrayList<DocumentoAnexoSTDDTO>();
        try {
			String sql = "SELECT ANE.SIDDOCANX, ANE.VNOMDOC, ANE.DFECREG, DEX.SIDDOCEXT, DEX.VURLDOCANX FROM IOTDTD_ANEXO ANE INNER JOIN IOTDTM_DOC_EXTERNO DEX ON ANE.SIDDOCEXT = DEX.SIDDOCEXT INNER JOIN IOTDTC_DESPACHO DES ON DEX.SIDEMIEXT = DES.SIDEMIEXT WHERE DES.SIDEMIEXT = ?";
			
			EntityManager em = factory.createEntityManager();
			Query query =  em.createNativeQuery(sql);
			query.setParameter(1, idTramite);
			List<Object> listResult = query.getResultList();
			DocumentoAnexoSTDDTO bean = null;
			for(Object o : listResult) {
				Object [] obj = (Object []) o;
				bean = new DocumentoAnexoSTDDTO();
				bean.setiCodAnexo(Utilitarios.toString(obj[0]));
				bean.setcNombreDocumento(Utilitarios.toString(obj[1]));
				bean.setfFecRegistro((Date) obj[2]);
				bean.setIdDocumentoExterno(Utilitarios.toString(obj[3]));
				bean.setcPathUrl(Utilitarios.toString(obj[4]));
				result.add(bean);
			}
			return result;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	@Override
	public List<DocumentoAnexoSTDDTO> findAnexosByRecepcion(String idTramite) throws DAOException{
		List<DocumentoAnexoSTDDTO> result = new ArrayList<DocumentoAnexoSTDDTO>();
        try {
			String sql = "SELECT ANE.SIDDOCANX, ANE.VNOMDOC, ANE.DFECREG, DEX.SIDDOCEXT, DEX.VURLDOCANX AS URL FROM IOTDTD_ANEXO ANE INNER JOIN IOTDTM_DOC_EXTERNO DEX ON ANE.SIDDOCEXT = DEX.SIDDOCEXT INNER JOIN IOTDTC_RECEPCION DES ON DEX.SIDRECEXT = DES.SIDRECEXT WHERE DES.SIDRECEXT = ?";
			
			EntityManager em = factory.createEntityManager();
			Query query =  em.createNativeQuery(sql);
			query.setParameter(1, idTramite);
			List<Object> listResult = query.getResultList();
			DocumentoAnexoSTDDTO bean = null;
			for(Object o : listResult) {
				Object [] obj = (Object []) o;
				bean = new DocumentoAnexoSTDDTO();
				bean.setiCodAnexo(Utilitarios.toString(obj[0]));
				bean.setcNombreDocumento(Utilitarios.toString(obj[1]));
				bean.setfFecRegistro((Date) obj[2]);
				bean.setIdDocumentoExterno(Utilitarios.toString(obj[3]));
				bean.setcPathUrl(Utilitarios.toString(obj[4]));
				result.add(bean);
			}
			return result;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	@Override
	public DocumentoAnexoSTDDTO findAnexoById(String idAnexo) throws DAOException {
		DocumentoAnexoSTDDTO result = null;
        try {
			String sql = "SELECT SIDDOCANX,VNOMDOC,DFECREG,SIDDOCEXT FROM IOTDTD_ANEXO WHERE SIDDOCANX = ?";
			
			EntityManager em = factory.createEntityManager();
			Query query =  em.createNativeQuery(sql);
			query.setParameter(1, idAnexo);
			List<Object> listResult = query.getResultList();
			if( !listResult.isEmpty() ){
				Object [] obj = (Object[]) listResult.get(0);
				result = new DocumentoAnexoSTDDTO();
				result.setiCodAnexo(Utilitarios.toString(obj[0]));
				result.setcNombreDocumento(Utilitarios.toString(obj[1]));
				result.setfFecRegistro((Date) obj[2]);
				result.setIdDocumentoExterno(Utilitarios.toString(obj[3]));
			}
			return result;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public DocumentoAnexoSTDDTO findAnexoDespachoByNombre(String idTramite, String nombre) throws DAOException {
		DocumentoAnexoSTDDTO result = null;
        try {
			String sql = "SELECT ANE.SIDDOCANX, ANE.VNOMDOC, ANE.DFECREG, DEX.SIDDOCEXT, DEX.VURLDOCANX FROM IOTDTD_ANEXO ANE INNER JOIN IOTDTM_DOC_EXTERNO DEX ON ANE.SIDDOCEXT = DEX.SIDDOCEXT INNER JOIN IOTDTC_DESPACHO DES ON DEX.SIDEMIEXT = DES.SIDEMIEXT WHERE DES.SIDEMIEXT = ? AND LOWER(ANE.VNOMDOC) = ?";
			
			EntityManager em = factory.createEntityManager();
			Query query =  em.createNativeQuery(sql);
			query.setParameter(1, idTramite);
			query.setParameter(2, nombre.toLowerCase());
			Object [] objects = (Object[]) query.getSingleResult();
			if( objects != null ){
				result = new DocumentoAnexoSTDDTO();
				result.setiCodAnexo(Utilitarios.toString(objects[0]));
				result.setcNombreDocumento(Utilitarios.toString(objects[1]));
				result.setfFecRegistro((Date) objects[2]);
				result.setIdDocumentoExterno(Utilitarios.toString(objects[3]));
				result.setcPathUrl(Utilitarios.toString(objects[4]));
			}
			return result;
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

}
