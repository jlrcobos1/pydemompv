package pe.gob.osinfor.pide.ws.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dao.DocumentoExternoDAO;
import pe.gob.osinfor.pide.ws.dao.impl.DocumentoExternoDAOImpl;
import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.services.DocumentoExternoService;

public class DocumentoExternoServiceImpl implements DocumentoExternoService {

	public static Logger LOG = LoggerFactory.getLogger(DocumentoExternoServiceImpl.class);
	
	@Override
	public void updateDocumentoExterno(DocumentoExternoDTO documento) throws ServiceException {
		DocumentoExternoDAO dao = new DocumentoExternoDAOImpl();
		try{
			dao.updateDocumentoExterno(documento);
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}
	
	@Override
	public DocumentoExternoDTO findDocumentosExternoById(String idTramite) throws ServiceException {
		DocumentoExternoDAO dao = new DocumentoExternoDAOImpl();
		try{
			DocumentoExternoDTO result = dao.findDocumentosExternoByTramite(idTramite);
			return result;
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}



}
