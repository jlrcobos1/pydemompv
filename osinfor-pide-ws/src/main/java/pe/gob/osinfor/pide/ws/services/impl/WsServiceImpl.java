package pe.gob.osinfor.pide.ws.services.impl;

import java.net.InetAddress;
import java.net.URL;
import java.util.HashMap;
import java.util.Random;

import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.services.ConfigService;
import pe.gob.osinfor.pide.ws.services.WsService;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.pide.pcm.webservice.PcmCuoPortTypeProxy;
import pe.gob.segdi.wsentidad.ws.EntidadBean;
import pe.gob.segdi.wsentidad.ws.EntidadProxy;
import pe.gob.segdi.wsiopidetramite.ws.CargoTramite;
import pe.gob.segdi.wsiopidetramite.ws.ConsultaTramite;
import pe.gob.segdi.wsiopidetramite.ws.IOTramite;
import pe.gob.segdi.wsiopidetramite.ws.IOTramiteService;
import pe.gob.segdi.wsiopidetramite.ws.RecepcionTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaCargoTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaConsultaTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaTramite;

public class WsServiceImpl implements WsService{
	
	public static Logger LOG = LoggerFactory.getLogger(WsServiceImpl.class);
	private HashMap<String,String> params = new ConfigServiceImpl().getConfigParameters();
	
	@Override
	public String findCUO() {
		try{
			String ip = InetAddress.getLocalHost().getHostAddress();
			String deployMode = params.get(ConfigService.DEPLOY_MODE);
			LOG.info("Generando CUO - Modo: [{}], IP: [{}]", deployMode, ip);
			if( Constants.DEPLOY_MODE_PROD.equalsIgnoreCase(deployMode) ){
				PcmCuoPortTypeProxy port = new PcmCuoPortTypeProxy(params.get(ConfigService.URL_PCM_CUO_WS));
				return port.getCUOEntidad(params.get(ConfigService.ENTIDAD_RUC),"");
			}else if( Constants.DEPLOY_MODE_TEST.equalsIgnoreCase(deployMode) ){
				PcmCuoPortTypeProxy port = new PcmCuoPortTypeProxy(params.get(ConfigService.URL_PCM_CUO_WS));
				return port.getCUO(ip);
			}else{
			    Random random = new Random(System.currentTimeMillis());
			    int cuo = Math.abs(1000000000 + random.nextInt(2000000000));
				return String.valueOf(cuo);
			}
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

	@Override
	public RespuestaTramite recepcionarTramiteResponse(RecepcionTramite request) throws ServiceException {
		LOG.info("Invocando al servicio para enviar el tramite - CUO: [{}]", request.getVcuo());
		try{
			IOTramiteService service = new IOTramiteService(new URL(params.get(ConfigService.URL_IOPIDE_TRAMITE_WS)));
			IOTramite tramitePort = service.getIOTramitePort();
			BindingProvider bindingProvider = (BindingProvider) tramitePort;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, params.get(ConfigService.URL_IOPIDE_TRAMITE_WS));
			return tramitePort.recepcionarTramiteResponse(request);
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

	@Override
	public RespuestaCargoTramite cargoResponse(CargoTramite request) throws ServiceException {
		LOG.info("Invocando al servicio para enviar el cargo - CUO: [{}]", request.getVcuo());
		try{
			IOTramiteService service = new IOTramiteService(new URL(params.get(ConfigService.URL_IOPIDE_TRAMITE_WS)));
			IOTramite tramitePort = service.getIOTramitePort();
			BindingProvider bindingProvider = (BindingProvider) tramitePort;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, params.get(ConfigService.URL_IOPIDE_TRAMITE_WS));
			return tramitePort.cargoResponse(request);
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}
	
	@Override
	public RespuestaConsultaTramite consultarTramiteResponse(ConsultaTramite request) throws ServiceException {
		LOG.info("Invocando al servicio para consultar el tramite - CUO: [{}]", request.getVcuo());
		try{
			IOTramiteService service = new IOTramiteService(new URL(params.get(ConfigService.URL_IOPIDE_TRAMITE_WS)));
			IOTramite tramitePort = service.getIOTramitePort();
			BindingProvider bindingProvider = (BindingProvider) tramitePort;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, params.get(ConfigService.URL_IOPIDE_TRAMITE_WS));
			return tramitePort.consultarTramiteResponse(request);
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

	@Override
	public EntidadBean[] findEntidades() throws ServiceException {
		LOG.info("Buscando entidades");
		try{
			EntidadProxy proxy = new EntidadProxy(params.get(ConfigService.URL_ENTIDAD_WS));
			return  proxy.getListaEntidad(1);
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

}
