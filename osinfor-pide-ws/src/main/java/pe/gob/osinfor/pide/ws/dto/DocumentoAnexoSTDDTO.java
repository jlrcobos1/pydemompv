package pe.gob.osinfor.pide.ws.dto;

import java.util.Date;

public class DocumentoAnexoSTDDTO {

	private String iCodAnexo;
	private String iCodTramite;
	private String cNombreDocumento;
	private String cPathUrl;
	private Date fFecRegistro;
	private String idDocumentoExterno;
	
	public String getiCodAnexo() {
		return iCodAnexo;
	}
	public void setiCodAnexo(String iCodAnexo) {
		this.iCodAnexo = iCodAnexo;
	}
	public String getiCodTramite() {
		return iCodTramite;
	}
	public void setiCodTramite(String iCodTramite) {
		this.iCodTramite = iCodTramite;
	}
	public String getcNombreDocumento() {
		return cNombreDocumento;
	}
	public void setcNombreDocumento(String cNombreDocumento) {
		this.cNombreDocumento = cNombreDocumento;
	}
	public String getcPathUrl() {
		return cPathUrl;
	}
	public void setcPathUrl(String cPathUrl) {
		this.cPathUrl = cPathUrl;
	}
	public Date getfFecRegistro() {
		return fFecRegistro;
	}
	public void setfFecRegistro(Date fFecRegistro) {
		this.fFecRegistro = fFecRegistro;
	}
	public String getIdDocumentoExterno() {
		return idDocumentoExterno;
	}
	public void setIdDocumentoExterno(String idDocumentoExterno) {
		this.idDocumentoExterno = idDocumentoExterno;
	}

	
}
