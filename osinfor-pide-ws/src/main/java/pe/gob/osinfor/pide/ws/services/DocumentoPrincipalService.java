package pe.gob.osinfor.pide.ws.services;

import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;

public interface DocumentoPrincipalService {
	DocumentoPrincipalDTO findDocumentoPrincipalByExterno(String idExterno) throws ServiceException;
}
