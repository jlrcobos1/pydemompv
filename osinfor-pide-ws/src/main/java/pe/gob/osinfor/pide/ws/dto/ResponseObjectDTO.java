package pe.gob.osinfor.pide.ws.dto;

public class ResponseObjectDTO {

	private String tag;
	private Boolean status;
	private String objeto;
	private String message;
	private String errorType;
	
	public ResponseObjectDTO(){
	}
	
	public ResponseObjectDTO(String tag){
		this.tag = tag;
	}
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getObjeto() {
		return objeto;
	}
	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	
}
