package pe.gob.osinfor.pide.ws.dao;

import java.util.List;

import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionFilterDTO;
import pe.gob.osinfor.pide.ws.dto.RemitenteDTO;
import pe.gob.osinfor.pide.ws.dto.ResponsableUniOrgDstDTO;
import pe.gob.osinfor.pide.ws.dto.TramiteDigitalDTO;
import pe.gob.osinfor.pide.ws.dto.UnidadOrganicaDstDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;

public interface RecepcionDAO {

	List<DocumentoRecepcionDTO> listarRecepcion(DocumentoRecepcionFilterDTO filterDTO) throws DAOException;
	List<DocumentoRecepcionDTO> listarRecepcion() throws Exception;
	List<DocumentoRecepcionDTO> listarRecepcionById(String id) throws Exception;
	DocumentoExternoDTO getDocumentoRecepcionExterno(String idDocument) throws DAOException;
	DocumentoRecepcionDTO getDocumentoRecepcionById(String idDocument) throws DAOException;
	List<UnidadOrganicaDstDTO> getListUnidadOrganicaDestino();
	List<ResponsableUniOrgDstDTO> getListResponsableUniOrgDestino(String iCodOficina);
	void recibirDocumento(DocumentoRecepcionDTO documento) throws DAOException;
	List<RemitenteDTO> findRemitenteByDocumento(String numDocumento) throws DAOException;
	TramiteDigitalDTO findDigitalByTramite(String sidrecext) throws DAOException;
}
