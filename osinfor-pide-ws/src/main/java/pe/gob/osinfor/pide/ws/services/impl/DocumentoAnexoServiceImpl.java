package pe.gob.osinfor.pide.ws.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dao.DocumentoAnexoDAO;
import pe.gob.osinfor.pide.ws.dao.impl.DocumentoAnexoDAOImpl;
import pe.gob.osinfor.pide.ws.dto.DocumentoAnexoSTDDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.services.DocumentoAnexoService;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;

public class DocumentoAnexoServiceImpl implements DocumentoAnexoService {
	
	public static Logger LOG = LoggerFactory.getLogger(DocumentoAnexoServiceImpl.class);
	
	@Override
	public List<DocumentoAnexoSTDDTO> findAnexosByDespacho(String idTramite) throws ServiceException{
		DocumentoAnexoDAO dao = new DocumentoAnexoDAOImpl();
		try{
			List<DocumentoAnexoSTDDTO> result = dao.findAnexosByDespacho(idTramite);
			LOG.info("Cantidad de anexos encontrados - Tramite: [{}], Cantidad: [{}]", idTramite, result.size());
			return result;
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<DocumentoAnexoSTDDTO> findAnexosByRecepcion(String idTramite) throws ServiceException{
		DocumentoAnexoDAO dao = new DocumentoAnexoDAOImpl();
		try{
			List<DocumentoAnexoSTDDTO> result = dao.findAnexosByRecepcion(idTramite);
			LOG.info("Cantidad de anexos encontrados - Tramite: [{}], Cantidad: [{}]", idTramite, result.size());
			for( DocumentoAnexoSTDDTO a: result ){
				LOG.info("Verificando formato de URL de anexo - Anexo: [{}], Url: [{}]", a.getiCodAnexo(), a.getcPathUrl());
				String path = a.getcPathUrl();
				if( Utilitarios.isBlank(path) ){
					path = "#";
				}else{
					if ( !path.startsWith("http") ){
						path = "http://" + path;
					}
					if( !path.endsWith("/") ){
						path = path + "/";
					}
				}
				LOG.info("Formato de URL de anexo - Anexo: [{}], Url: [{}]", a.getiCodAnexo(), path);
				a.setcPathUrl(path);
			}
			return result;
		}catch(Exception e){
			throw new ServiceException(e);
		}
	}

}
