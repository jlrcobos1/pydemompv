package pe.gob.osinfor.pide.ws.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="VIEW_RESPONSABLE_ORG_DST_REC")
public class ResponsableUniOrgDstDTO {

	@Id
	private String iCodTrabajador;
	private String cNombresTrabajador;
	private String cApellidosTrabajador;
	private String iCodOficina;
	
	public String getiCodTrabajador() {
		return iCodTrabajador;
	}
	public void setiCodTrabajador(String iCodTrabajador) {
		this.iCodTrabajador = iCodTrabajador;
	}
	public String getcNombresTrabajador() {
		return cNombresTrabajador;
	}
	public void setcNombresTrabajador(String cNombresTrabajador) {
		this.cNombresTrabajador = cNombresTrabajador;
	}
	public String getcApellidosTrabajador() {
		return cApellidosTrabajador;
	}
	public void setcApellidosTrabajador(String cApellidosTrabajador) {
		this.cApellidosTrabajador = cApellidosTrabajador;
	}
	public String getiCodOficina() {
		return iCodOficina;
	}
	public void setiCodOficina(String iCodOficina) {
		this.iCodOficina = iCodOficina;
	}
	
	
}
