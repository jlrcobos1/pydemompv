package pe.gob.osinfor.pide.ws.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dao.RecepcionDAO;
import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionFilterDTO;
import pe.gob.osinfor.pide.ws.dto.RemitenteDTO;
import pe.gob.osinfor.pide.ws.dto.ResponsableUniOrgDstDTO;
import pe.gob.osinfor.pide.ws.dto.TramiteDigitalDTO;
import pe.gob.osinfor.pide.ws.dto.UnidadOrganicaDstDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.osinfor.pide.ws.utils.Transaction;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;

public class RecepcionDAOImpl implements RecepcionDAO {

	public static Logger LOG = LoggerFactory.getLogger(RecepcionDAOImpl.class);
	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("OSINFOR-PIDE");
	private Transaction transaction;
	
	private String querieListarRecepcionBase = "SELECT r.SIDRECEXT, r.VRUCENTREM, r.VUNIORGREM, r.CTIPDOCIDEREM, r.VNUMDOCIDEREM, r.VNUMREGSTD, r.VANIOREGSTD, r.VUNIORGSTD, r.CCODUNIORGSTD, r.VDESANXSTD, r.DFECREGSTD, r.VUSUREGSTD, r.VCUO, r.VCUOREF, r.VOBS, r.CFLGESTOBS, r.CFLGEST, r.DFECREG, r.VUSUMOD, r.DFECMOD, r.CFLGENVCAR, r.VIDREMITENTESTD " +
											   " FROM DocumentoRecepcionDTO r";
	
	public RecepcionDAOImpl(){
		
	}
	
	public RecepcionDAOImpl(Transaction transaction){
		this.transaction = transaction;
	}

	@Override
	public List<DocumentoRecepcionDTO> listarRecepcion() throws Exception {
		EntityManager em = factory.createEntityManager();
		String query = querieListarRecepcionBase + " order by r.SIDRECEXT desc ";
		Query q = em.createQuery(query);
		List<Object[]> list = q.getResultList();
		em.close();
		return this.getListarRecepcion(list);
	}

	@Override
	public List<DocumentoRecepcionDTO> listarRecepcionById(String id) throws Exception {
		String query = querieListarRecepcionBase + " WHERE r.SIDRECEXT = :SIDRECEXT order by r.SIDRECEXT desc";
		EntityManager em = factory.createEntityManager();
		Query q = em.createQuery(query);
		q.setParameter("SIDRECEXT", id);
		List<Object[]> list = q.getResultList();
		return this.getListarRecepcion(list);
	}

	@Override
	public List<DocumentoRecepcionDTO> listarRecepcion(DocumentoRecepcionFilterDTO filterDTO) throws DAOException {
		try {
			String query = querieListarRecepcionBase;
			query = query + "  WHERE 1=1 ";
			if ( !Utilitarios.isBlank(filterDTO.getRuc()) ) {
				query = query + " AND r.VRUCENTREM like '%" + filterDTO.getRuc() + "%'";
			}
			if ( !Utilitarios.isBlank(filterDTO.getNumDocumentoEntidad()) ) {
				query = query + " AND EXISTS (SELECT 1 FROM DocumentoExternoDTO E WHERE E.SIDRECEXT = r.SIDRECEXT AND E.VNUMDOC like '%" + filterDTO.getNumDocumentoEntidad() + "%')";
			}
			if (!Utilitarios.isBlank(filterDTO.getState()) ) {
				query = query + " AND r.CFLGEST =  '" + filterDTO.getState() + "' ";
			}
			query = query + " order by r.SIDRECEXT desc ";
			
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery(query);
	
			List<Object[]> list = q.getResultList();
			em.close();
			return this.getListarRecepcion(list);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public DocumentoExternoDTO getDocumentoRecepcionExterno(String idDocument)  throws DAOException{
		try{
			EntityManager em = factory.createEntityManager();
			String querie = "select de from DocumentoExternoDTO de WHERE de.SIDRECEXT = :SIDRECEXT";
			Query q = em.createQuery(querie);
			q.setParameter("SIDRECEXT", idDocument);
			DocumentoExternoDTO entity = null;
			if (q.getResultList().size() > 0) {
				entity = (DocumentoExternoDTO) q.getResultList().get(0);
			}
			em.close();
			return entity;
		}catch(Exception e){
			throw new DAOException(e);
		}
	}

	public DocumentoPrincipalDTO getDocumentoPrincipalByDocExterno(String idDocument) {
		EntityManager em = factory.createEntityManager();
		String querie = "select dp.SIDDOCPRI,dp.VNOMDOC,dp.SIDDOCEXT,dp.BPDFDOC from DocumentoPrincipalDTO dp WHERE dp.SIDDOCEXT ="
				+ " ( SELECT de.SIDDOCEXT from DocumentoExternoDTO de WHERE de.SIDRECEXT = :SIDRECEXT)";
		Query q = em.createQuery(querie);
		q.setParameter("SIDRECEXT", idDocument);
		DocumentoPrincipalDTO entity = null;
		if (q.getResultList().size() > 0) {
			entity = new DocumentoPrincipalDTO();
			Object[] result = (Object[]) q.getResultList().get(0);
			entity.setSIDDOCPRI(Utilitarios.verifyNullObjectToInt(result[0]));
			entity.setVNOMDOC(Utilitarios.verifyNullObjectToString(result[1]));
			entity.setSIDDOCEXT(Utilitarios.verifyNullObjectToString(result[2]));
			entity.setBPDFDOC((byte[]) result[3]);
		}
		em.close();
		return entity;
	}

	public DocumentoRecepcionDTO getDocumentoRecepcionById(String idDocument) {
		try{
			EntityManager em = getEntityManager();
			
			String sql = "select r from DocumentoRecepcionDTO r WHERE r.SIDRECEXT = :SIDRECEXT";
			Query query = em.createQuery(sql);
			query.setParameter("SIDRECEXT", idDocument);
			DocumentoRecepcionDTO entity = (DocumentoRecepcionDTO) query.getSingleResult();
			em.refresh(entity);
			return entity;
		}catch(NoResultException e){
			return null;  
		}catch(Exception e){
			throw new DAOException(e);
		}
	}

	public List<UnidadOrganicaDstDTO> getListUnidadOrganicaDestino() {
		EntityManager em = factory.createEntityManager();
		String querie = "select u from UnidadOrganicaDstDTO u order by u.cNomOficina";
		List<UnidadOrganicaDstDTO> lstEntity = null;
		Query q = em.createQuery(querie);
		if (q.getResultList().size() > 0) {
			lstEntity = q.getResultList();
		}
		em.close();
		return lstEntity;
	}

	public List<ResponsableUniOrgDstDTO> getListResponsableUniOrgDestino(String iCodOficina) {
		EntityManager em = factory.createEntityManager();
		String querie = "select r from ResponsableUniOrgDstDTO r WHERE r.iCodOficina = :ICODOFICINA";
		List<ResponsableUniOrgDstDTO> lstEntity = null;
		Query q = em.createQuery(querie);
		q.setParameter("ICODOFICINA", iCodOficina);
		if (q.getResultList().size() > 0) {
			lstEntity = q.getResultList();
		}
		em.close();
		return lstEntity;
	}

	public void recibirDocumento(DocumentoRecepcionDTO documento) throws DAOException {
		EntityManager em = getEntityManager();
		try {
			DocumentoRecepcionDTO document = em.find(DocumentoRecepcionDTO.class, documento.getSIDRECEXT());
			UnidadOrganicaDstDTO unidadOrganica = em.find(UnidadOrganicaDstDTO.class, documento.getCCODUNIORGSTD());

			document.setCFLGEST(documento.getCFLGEST());
			document.setDFECREGSTD(new Date());
			document.setDFECREG(new Date());
			document.setDFECMOD(new Date());
			document.setVOBS(documento.getVOBS());
			document.setCCODUNIORGSTD(documento.getCCODUNIORGSTD());
			document.setVUNIORGSTD(unidadOrganica.getcNomOficina());
			document.setVUSUREGSTD(documento.getVUSUREGSTD());
			document.setBCARSTD(documento.getBCARSTD());
			document.setVUSUMOD(documento.getVUSUMOD());
			document.setVIDREMITENTESTD(documento.getVIDREMITENTESTD());
			document.setVNCARSTD(documento.getVNCARSTD());
			
			em.merge(document);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	private List<DocumentoRecepcionDTO> getListarRecepcion(List<Object[]> list) {
		List<DocumentoRecepcionDTO> listEntity = new ArrayList<>();
		for (Object[] result : list) {
			DocumentoRecepcionDTO documento = new DocumentoRecepcionDTO();
			documento.setSIDRECEXT(Utilitarios.verifyNullObjectToString(result[0]));
			documento.setVRUCENTREM(Utilitarios.verifyNullObjectToString(result[1]));
			documento.setVUNIORGREM(Utilitarios.verifyNullObjectToString(result[2]));
			documento.setCTIPDOCIDEREM(Utilitarios.verifyNullObjectToString(result[3]));
			documento.setVNUMDOCIDEREM(Utilitarios.verifyNullObjectToString(result[4]));
			documento.setVNUMREGSTD(Utilitarios.verifyNullObjectToString(result[5]));
			documento.setVANIOREGSTD(Utilitarios.verifyNullObjectToString(result[6]));
			documento.setVUNIORGSTD(Utilitarios.verifyNullObjectToString(result[7]));
			documento.setCCODUNIORGSTD(Utilitarios.verifyNullObjectToString(result[8]));
			documento.setVDESANXSTD(Utilitarios.verifyNullObjectToString(result[9]));
			documento.setDFECREGSTD((Date)(result[10]));
			documento.setVUSUREGSTD(Utilitarios.verifyNullObjectToString(result[11]));
			documento.setVCUO(Utilitarios.verifyNullObjectToString(result[12]));
			documento.setVCUOREF(Utilitarios.verifyNullObjectToString(result[13]));
			documento.setVOBS(Utilitarios.verifyNullObjectToString(result[14]));
			documento.setCFLGESTOBS(Utilitarios.verifyNullObjectToString(result[15]));
			documento.setCFLGEST(Utilitarios.verifyNullObjectToString(result[16]));
			documento.setDFECREG((Date)(result[17]));
			documento.setVUSUMOD(Utilitarios.verifyNullObjectToString(result[18]));
			documento.setDFECMOD((Date)(result[19]));
			documento.setCFLGENVCAR(Utilitarios.verifyNullObjectToString(result[20]));
			documento.setVIDREMITENTESTD(Utilitarios.verifyNullObjectToString(result[21]));
			
			listEntity.add(documento);
		}
		return listEntity;
	}

	@Override
	public List<RemitenteDTO> findRemitenteByDocumento(String numDocumento) throws DAOException {
		List<RemitenteDTO> result = new ArrayList<RemitenteDTO>();
        try {
			String sql = "SELECT ICODREMITENTE, CNOMBRE FROM VIEW_REMITENTE_REC WHERE NNUMDOCUMENTO = ?";
			
			EntityManager em = getEntityManager();
			Query query =  em.createNativeQuery(sql);
			query.setParameter(1, numDocumento);
			List<Object> listResult = query.getResultList();
			RemitenteDTO bean = null;
			for(Object o : listResult) {
				Object [] obj = (Object []) o;
				bean = new RemitenteDTO();
				bean.setiCodRemitente(Utilitarios.toLong(obj[0]));
				bean.setcNombre(Utilitarios.toString(obj[1]));
				result.add(bean);
			}
			return result;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public TramiteDigitalDTO findDigitalByTramite(String sidrecext) throws DAOException {
		TramiteDigitalDTO result = null;
        try {
			String sql = "SELECT iCodDigital,iCodTramite,cNombreOriginal,cNombreNuevo,iCodMovimiento, sidrecext FROM VIEW_TRAMITE_DIGITALES_REC WHERE sidrecext = ?";
			
			EntityManager em = getEntityManager();
			Query query =  em.createNativeQuery(sql);
			query.setParameter(1, sidrecext);
			List<Object> listResult = query.getResultList();
			if ( !listResult.isEmpty() ) {
				Object [] obj = (Object []) listResult.get(0);
				result = new TramiteDigitalDTO();
				result.setiCodDigital(Utilitarios.toLong(obj[0]));
				result.setiCodTramite(Utilitarios.toString(obj[1]));
				result.setcNombreOriginal(Utilitarios.toString(obj[2]));
				result.setcNombreNuevo(Utilitarios.toString(obj[3]));
				result.setiCodMovimiento(Utilitarios.toString(obj[4]));
				result.setSidrecext(Utilitarios.toString(obj[5]));
			}
			return result;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	private EntityManager getEntityManager(){
		if( transaction == null ){
			return factory.createEntityManager();			
		}
		return transaction.getEntityManager();
	}

}
