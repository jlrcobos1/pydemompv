package pe.gob.osinfor.pide.ws.services;

import java.util.HashMap;

public interface ConfigService {

	public final String URL_PCM_CUO_WS = "URL_PCM_CUO_WS";
	public final String URL_IOPIDE_TRAMITE_WS = "URL_IOPIDE_TRAMITE_WS";
	public final String URL_ENTIDAD_WS = "URL_ENTIDAD_WS";
	public final String PATH_DOCUMENTO_DESPACHO = "PATH_DOCUMENTO_DESPACHO";
	public final String PATH_DOCUMENTO_RECEPCION = "PATH_DOCUMENTO_RECEPCION";
	public final String PATH_DOCUMENTO_PUBLICO = "PATH_DOCUMENTO_PUBLICO";
	public final String PATH_STD_ALMACEN = "PATH_STD_ALMACEN";
	public final String PATH_DOCUMENTO_PUBLICO_SUBCARPETA = "PATH_DOCUMENTO_PUBLICO_SUBCARPETA";
	public final String PATH_VISOR_DOCUMENTOS = "PATH_VISOR_DOCUMENTOS";
	public final String ENTIDAD_RUC = "ENTIDAD_RUC";
	public final String ENTIDAD_NOMBRE = "ENTIDAD_NOMBRE";
	public final String DEPLOY_MODE = "DEPLOY_MODE";
	
	public HashMap<String, String> getConfigParameters();
	
}
