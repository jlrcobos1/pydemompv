package pe.gob.osinfor.pide.ws.services.impl;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dao.RecepcionDAO;
import pe.gob.osinfor.pide.ws.dao.impl.RecepcionDAOImpl;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionFilterDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionLista;
import pe.gob.osinfor.pide.ws.dto.RemitenteDTO;
import pe.gob.osinfor.pide.ws.dto.RequestObjectDTO;
import pe.gob.osinfor.pide.ws.dto.ResponsableUniOrgDstDTO;
import pe.gob.osinfor.pide.ws.dto.TramiteDigitalDTO;
import pe.gob.osinfor.pide.ws.dto.UnidadOrganicaDstDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;
import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.services.ConfigService;
import pe.gob.osinfor.pide.ws.services.RecepcionService;
import pe.gob.osinfor.pide.ws.services.WsService;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.osinfor.pide.ws.utils.Transaction;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;
import pe.gob.segdi.wsiopidetramite.ws.CargoTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaCargoTramite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class RecepcionServiceImpl implements RecepcionService {

	public static Logger LOG = LoggerFactory.getLogger(RecepcionServiceImpl.class);
	private HashMap<String,String> params = new ConfigServiceImpl().getConfigParameters();

	@Override
	public List<DocumentoRecepcionLista> listarRecepcion(DocumentoRecepcionFilterDTO filterDTO) {
		List<DocumentoRecepcionLista> listaDetalle = new ArrayList<>();
		RecepcionDAO dao = new RecepcionDAOImpl();
		try {
			List<DocumentoRecepcionDTO> lista = null;
			if( !Utilitarios.isBlank(filterDTO.getIdDocument())  ) {
				lista = dao.listarRecepcionById(filterDTO.getIdDocument());
			}else if ( !Utilitarios.isBlank(filterDTO.getRuc()) || !Utilitarios.isBlank(filterDTO.getState()) || !Utilitarios.isBlank(filterDTO.getNumDocumentoEntidad()) ){
				lista = dao.listarRecepcion(filterDTO);
			} else {
				lista = dao.listarRecepcion();
			}
			for (DocumentoRecepcionDTO dp : lista) {
				DocumentoRecepcionLista docLista = new DocumentoRecepcionLista();
				docLista.setDocumentoRecepcion(dp);
				docLista.setDocumentoExterno(dao.getDocumentoRecepcionExterno(dp.getSIDRECEXT()));
				listaDetalle.add(docLista);
			}
			return listaDetalle;
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	public RequestObjectDTO getListUnidadOrganicaDestino() {
		List<UnidadOrganicaDstDTO> list = null;
		RequestObjectDTO responseResult = new RequestObjectDTO();
		try {
			RecepcionDAO dao = new RecepcionDAOImpl();
			list = dao.getListUnidadOrganicaDestino();
			JsonObject jsonObject = new JsonObject();
			responseResult.setStatus(true);
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			String listString = gson.toJson(list);
			responseResult.setObjeto(listString);
			responseResult.setTag(Constants.TAG_RECEPCION);
		} catch (Exception e) {
			list = null;
			LOG.error("Ocurrio un error al intentar buscar las unidades", e);
		}
		return responseResult;
	}

	public List<ResponsableUniOrgDstDTO> getListResponsableUniOrgDestino(String iCodOficina) {
		try {
			RecepcionDAO dao = new RecepcionDAOImpl();
			return dao.getListResponsableUniOrgDestino(iCodOficina);
		} catch (Exception e) {
			LOG.error("Ocurrio un error al intentar buscar los responsables", e);
			throw new ServiceException(e);
		}
	}

	@Override
	public DocumentoRecepcionDTO getDocumentoRecepcionById(String idDocument) throws ServiceException {
		try {
			RecepcionDAO dao = new RecepcionDAOImpl();
			return dao.getDocumentoRecepcionById(idDocument);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public void recibirDocumento(DocumentoRecepcionDTO documento) throws ServiceException{
		LOG.info("Recibiendo documento - Documento: [{}]", documento.getSIDRECEXT());
		Transaction tx = new Transaction();
		RecepcionDAO dao = new RecepcionDAOImpl(tx);
		try {	
			tx.begin();
			dao.recibirDocumento(documento);
			LOG.info("Tramite creado correctamente - Documento: [{}]", documento.getSIDRECEXT());
			
			TramiteDigitalDTO digital = dao.findDigitalByTramite(documento.getSIDRECEXT());
			if( digital != null ){
				LOG.info("Documento digital - Tramite: [{}], Digital: [{}], Nombre: [{}]", digital.getiCodTramite(), digital.getiCodDigital(), digital.getcNombreNuevo());
				String repository = params.get(ConfigService.PATH_STD_ALMACEN);
				Utilitarios.createDirectoriesIfNotExists(Paths.get(repository));
				String fileTarget = repository.concat("\\").concat(digital.getcNombreNuevo());
				Utilitarios.bytesToFile(documento.getBCARSTD(), fileTarget);
			}
	
			DocumentoRecepcionDTO recepcion = dao.getDocumentoRecepcionById(documento.getSIDRECEXT());
			enviarCargo(recepcion);
			
			tx.commit();
				
			LOG.info("Documento recibido correctamente - Documento: [{}]", documento.getSIDRECEXT());
		} catch (Exception e) {
			try{
				tx.rollback();
			}catch(DAOException ex){
				throw new ServiceException(ex);
			}
			throw new ServiceException(e);
		}
	}
	
	private void enviarCargo(DocumentoRecepcionDTO recepcion) throws DatatypeConfigurationException{
		LOG.info("Enviando el cargo a PIDE - Documento: [{}]", recepcion.getSIDRECEXT());
		LOG.info("VRUCENTREM: [{}]", params.get(ConfigService.ENTIDAD_RUC));
		LOG.info("VRUCENTREC: [{}]", recepcion.getVRUCENTREM());
		LOG.info("VCUO: [{}]", recepcion.getVCUO());
		LOG.info("VCUOREF: [{}]", Utilitarios.toEmptyIfNull(recepcion.getVCUOREF()));
		LOG.info("VNUMREGSTD: [{}]", recepcion.getVNUMREGSTD());
		LOG.info("VANIOREGSTD: [{}]", recepcion.getVANIOREGSTD());
		LOG.info("DFECREGSTD: [{}]", Utilitarios.convertToXMLGregorianCalendar(recepcion.getDFECREGSTD()));
		LOG.info("VUNIORGSTD: [{}]", Utilitarios.trim(recepcion.getVUNIORGSTD()));
		LOG.info("VUSUREGSTD: [{}]", recepcion.getVUSUREGSTD());
		LOG.info("VOBS: [{}]", Utilitarios.toEmptyIfNull(recepcion.getVOBS()));
		LOG.info("CFLGEST: [{}]", recepcion.getCFLGEST());
		
		WsService wsService = new WsServiceImpl();
		CargoTramite request = new CargoTramite();

		request.setVrucentrem(params.get(ConfigService.ENTIDAD_RUC));
		request.setVrucentrec(recepcion.getVRUCENTREM());
		request.setVcuo(recepcion.getVCUO());
		request.setVcuoref(Utilitarios.toEmptyIfNull(recepcion.getVCUOREF()));
		request.setVnumregstd(recepcion.getVNUMREGSTD());
		request.setVanioregstd(recepcion.getVANIOREGSTD());
		request.setDfecregstd(Utilitarios.convertToXMLGregorianCalendar(recepcion.getDFECREGSTD()));
		request.setVuniorgstd(Utilitarios.trim(recepcion.getVUNIORGSTD()));
		request.setVusuregstd("DENNIS ZARATE");		//TODO Falta agregar el usuario en sesion
		request.setBcarstd(Utilitarios.encodeBase64(recepcion.getBCARSTD()));
		request.setVobs(Utilitarios.toEmptyIfNull(recepcion.getVOBS()));
		request.setCflgest(recepcion.getCFLGEST());
		RespuestaCargoTramite response = wsService.cargoResponse(request);
		LOG.info("Respuesta de envio del cargo - Codigo: [{}], Descripcion: [{}]", response.getVcodres(), response.getVdesres() );
		if ( !response.getVcodres().equals(Constants.WS_IOPIDE_TRAMITE_RESPONSE_OK) ) {
			throw new ServiceException(response.getVdesres());
		}
			
	}

	@Override
	public List<RemitenteDTO> findRemitenteByDocumento(String numDocumento) throws ServiceException {
		try {
			RecepcionDAO dao = new RecepcionDAOImpl();
			return dao.findRemitenteByDocumento(numDocumento);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}


}
