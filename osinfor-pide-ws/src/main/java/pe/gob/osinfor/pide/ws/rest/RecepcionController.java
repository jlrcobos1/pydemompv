package pe.gob.osinfor.pide.ws.rest;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dto.DocumentoAnexoSTDDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionFilterDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoRecepcionLista;
import pe.gob.osinfor.pide.ws.dto.RemitenteDTO;
import pe.gob.osinfor.pide.ws.dto.RequestObjectDTO;
import pe.gob.osinfor.pide.ws.dto.ResponsableUniOrgDstDTO;
import pe.gob.osinfor.pide.ws.dto.ResponseObjectDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.exception.ValidationException;
import pe.gob.osinfor.pide.ws.services.DocumentoAnexoService;
import pe.gob.osinfor.pide.ws.services.DocumentoPrincipalService;
import pe.gob.osinfor.pide.ws.services.RecepcionService;
import pe.gob.osinfor.pide.ws.services.impl.DocumentoAnexoServiceImpl;
import pe.gob.osinfor.pide.ws.services.impl.DocumentoPrincipalServiceImpl;
import pe.gob.osinfor.pide.ws.services.impl.RecepcionServiceImpl;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.osinfor.pide.ws.utils.ResponseUtils;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;

@Path("/")
public class RecepcionController {

	public static Logger LOG = LoggerFactory.getLogger(RecepcionController.class);  
	private RecepcionService recepcionService = new RecepcionServiceImpl();
	private DocumentoPrincipalService docPrincipalService = new DocumentoPrincipalServiceImpl();
	private DocumentoAnexoService docAnexoService = new DocumentoAnexoServiceImpl();

	@POST
	@Path("osinfor/recepcion/listar-recepcion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO listarRecepcion(DocumentoRecepcionFilterDTO filterDTO, @Context UriInfo uriInfo) {
		LOG.info("Buscando documentos - Documento: [{}], RUC: [{}], Estado: [{}],", filterDTO.getIdDocument(), filterDTO.getRuc(), filterDTO.getState());
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_DESPACHO);
		try{
			List<DocumentoRecepcionLista> result = recepcionService.listarRecepcion(filterDTO);
			LOG.info("Cantidad de documentos encontrados: [{}]", result.size());
			
		    response.setStatus(true);
		    response.setObjeto(Utilitarios.convertToJson(result));	
			return response;
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al buscar los documentos");
			LOG.error("Ocurrio un error inesperado al buscar los documentos", e);
		}
		return response;
	}

	@POST
	@Path("osinfor/recepcion/listar-uni-organica")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public RequestObjectDTO listarUnidadOrganica(@Context UriInfo uriInfo) {
		LOG.info("Buscando unidades organicas ...");
		
		RequestObjectDTO responseResult = recepcionService.getListUnidadOrganicaDestino();
		return responseResult;
	}

	@POST
	@Path("osinfor/recepcion/listar-resp-uni-org")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO listarResponsableUniOrg(String iCodOficina, @Context UriInfo uriInfo) {
		LOG.info("Buscando responsables - Oficina: [{}]", iCodOficina);
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_RECEPCION);
		try{
			List<ResponsableUniOrgDstDTO> result = recepcionService.getListResponsableUniOrgDestino(iCodOficina);
		    response.setStatus(true);
		    response.setObjeto(Utilitarios.convertToJson(result));	
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al buscar los responsables");
			LOG.error("Ocurrio un error inesperado al buscar los responsables", e);
		}
		return response;
	}
	
	@GET
	@Path("osinfor/recepcion/listar-remitentes/{numDocumento}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO listarRemitentes(@PathParam("numDocumento") String numDocumento, @Context UriInfo uriInfo) {
		LOG.info("Buscando remitentes ...");
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_RECEPCION);
		try{
			List<RemitenteDTO> result = recepcionService.findRemitenteByDocumento(numDocumento);
		    response.setStatus(true);
		    response.setObjeto(Utilitarios.convertToJson(result));	
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al buscar los remitentes");
			LOG.error("Ocurrio un error inesperado al buscar los remitentes", e);
		}
		return response;
	}

	@POST
	@Path("osinfor/recepcion/recibir-documento")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO recibirDocumento(@FormDataParam("SIDRECEXT") String sidrecext,
											  @FormDataParam("CCODUNIORGSTD") String ccoduniorgstd,
											  @FormDataParam("VUSUREGSTD") String vusuregstd,
											  @FormDataParam("VIDREMITENTESTD") String vidremitentestd,
											  @FormDataParam("file") InputStream input,  
											  @FormDataParam("file") FormDataContentDisposition content,
											  @Context UriInfo uriInfo) {
		
		LOG.info("Recibiendo documento - Documento: [{}], Unidad: [{}], Usuario: [{}], Remitente: [{}]", sidrecext, ccoduniorgstd, vusuregstd, vidremitentestd);
		LOG.info("Cargo recibido: [{}]", content.getFileName());
		
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_RECEPCION);
		try{
			DocumentoRecepcionDTO recepcion = new DocumentoRecepcionDTO();
			recepcion.setSIDRECEXT(sidrecext);
			recepcion.setCCODUNIORGSTD(ccoduniorgstd);
			recepcion.setVUSUREGSTD(vusuregstd);
			recepcion.setVIDREMITENTESTD(vidremitentestd);
			recepcion.setBCARSTD(IOUtils.toByteArray(input));
			recepcion.setVNCARSTD(content.getFileName());
			recepcion.setCFLGEST(Constants.DOCUMENTO_RECEPCION_FLAG_RECIBIDO);
			recepcion.setVUSUMOD("");		//TODO Falta agregar el usuario en sesion

			recepcionService.recibirDocumento(recepcion);
			response.setStatus(true);
		}catch(ValidationException e){
			ResponseUtils.validationError(response, e);
			LOG.error("Ocurrio un error de validacion al recibir el documento", e);		
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al recibir el documento");
			LOG.error("Ocurrio un error inesperado al recibir el documento", e);
		}
		return response;
	}

	@POST
	@Path("osinfor/recepcion/observar-documento")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO observarDocumento(@FormDataParam("SIDRECEXT") String sidrecext,
											  @FormDataParam("CCODUNIORGSTD") String ccoduniorgstd,
											  @FormDataParam("VUSUREGSTD") String vusuregstd,
											  @FormDataParam("VOBS") String vobs,
											  @FormDataParam("VIDREMITENTESTD") String vidremitentestd,
											  @FormDataParam("file") InputStream input,  
											  @FormDataParam("file") FormDataContentDisposition content,
											  @Context UriInfo uriInfo) {
		
		LOG.info("Observando documento - Documento: [{}], Unidad: [{}], Usuario: [{}], Remitente: [{}]", sidrecext, ccoduniorgstd, vusuregstd, vidremitentestd);
		LOG.info("Cargo recibido: [{}]", content.getFileName());
		
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_RECEPCION);
		try{
			
			DocumentoRecepcionDTO recepcion = new DocumentoRecepcionDTO();
			recepcion.setSIDRECEXT(sidrecext);
			recepcion.setCCODUNIORGSTD(ccoduniorgstd);
			recepcion.setVUSUREGSTD(vusuregstd);
			recepcion.setVIDREMITENTESTD(vidremitentestd);
			recepcion.setBCARSTD(IOUtils.toByteArray(input));
			recepcion.setVNCARSTD(content.getFileName());
			recepcion.setVOBS(vobs);
			recepcion.setCFLGEST(Constants.DOCUMENTO_RECEPCION_FLAG_OBSERVADO);
			recepcion.setVUSUMOD("");		//TODO Falta agregar el usuario en sesion

			recepcionService.recibirDocumento(recepcion);
			response.setStatus(true);
		}catch(ValidationException e){
			ResponseUtils.validationError(response, e);
			LOG.error("Ocurrio un error de validacion al recibir el documento", e);		
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al recibir el documento");
			LOG.error("Ocurrio un error inesperado al recibir el documento", e);
		}
		return response;
	}
	
	@GET
	@Path("osinfor/recepcion/listar-anexos/{idTramite}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO listarAnexos(@PathParam("idTramite") String idTramite, @Context UriInfo uriInfo) throws Exception {
		LOG.info("Buscando anexos - Tramite: [{}]", idTramite);
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_DESPACHO);
		try{
			List<DocumentoAnexoSTDDTO> result = docAnexoService.findAnexosByRecepcion(idTramite);
		    response.setStatus(true);
		    response.setObjeto(Utilitarios.convertToJson(result));	
			return response;
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al buscar los anexos");
			LOG.error("Ocurrio un error inesperado al buscar los anexos", e);
		}
		return response;
	}
	
	
	@GET
	@Path("osinfor/recepcion/download-principal/{idExterno}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadPrincipal(@PathParam("idExterno") String idExterno, @Context UriInfo uriInfo) throws Exception {
		LOG.info("Descargando documento principal - Externo: [{}]", idExterno);
		try{
			DocumentoPrincipalDTO principal = docPrincipalService.findDocumentoPrincipalByExterno(idExterno);
			if( principal == null ){
				throw new ValidationException("El documento principal no existe");
			}else if( principal.getBPDFDOC() == null ){
				throw new ValidationException("El documento principal no se encuentra registrado");
			}
			LOG.info("Documento principal encontrado - Externo:[{}]", idExterno);
			
			File file = Utilitarios.createTempFile(principal.getBPDFDOC(), Constants.PDF_EXTENSION);
			String filename = Utilitarios.getFilename(principal.getVNOMDOC(), Constants.PDF_EXTENSION);
			String content = "inline; filename=\"" + filename + "\"";
		    return Response.ok(file, "application/pdf").header("Content-Disposition", content).build();
		}catch(ServiceException e){
			LOG.error("Ocurrio un error inesperado al descargar el documento principal", e);
			throw new Exception(e);
		}catch(ValidationException e){
			LOG.error("Ocurrio un error de validacion al descargar el documento principal - Mensaje: [{}]", e.getMessage());
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	
	@GET
	@Path("osinfor/recepcion/download-cargo/{idDocument}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadCargo(@PathParam("idDocument") String idDocument, @Context UriInfo uriInfo) throws Exception {
		LOG.info("Descargando cargo - Recepcion: [{}]", idDocument);
		try{
			DocumentoRecepcionDTO recepcion = recepcionService.getDocumentoRecepcionById(idDocument);
			if( recepcion == null ){
				throw new ValidationException("El documento de recepcion no existe");
			}else if( recepcion.getBCARSTD() == null ){
				throw new ValidationException("El cargo no se encuentra registrado");
			}
			LOG.info("Documento del cargo encontrado - Recepcion:[{}]", idDocument);
			File file = Utilitarios.createTempFile(recepcion.getBCARSTD(), Constants.PDF_EXTENSION);
			String filename = Utilitarios.getFilename("Cargo", Constants.PDF_EXTENSION);
			String content = "inline; filename=\"" + filename + "\"";
		    return Response.ok(file, "application/pdf").header("Content-Disposition", content).build();
		}catch(ServiceException e){
			LOG.error("Ocurrio un error inesperado al descargar el cargo", e);
			throw new Exception(e);
		}catch(ValidationException e) {
			LOG.error("Ocurrio un error de validacion al descargar el cargo - Mensaje: [{}]", e.getMessage());
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
}
