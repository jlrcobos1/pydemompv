package pe.gob.osinfor.pide.ws.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="IOTDTM_DOC_EXTERNO")
public class DocumentoExternoDTO {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String SIDDOCEXT;
	private String VNOMENTEMI;
	private String CCODTIPDOC;
	private String VNUMDOC;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DFECDOC;
	private String VUNIORGDST;
	private String VNOMDST;
	private String VNOMCARDST;
	private String VASU;
	private String CINDTUP;
	private int SNUMANX;
	private int SNUMFOL;
	private String VURLDOCANX;
	private String SIDEMIEXT;
	private String SIDRECEXT;
	
	public String getSIDDOCEXT() {
		return SIDDOCEXT;
	}
	public void setSIDDOCEXT(String sIDDOCEXT) {
		SIDDOCEXT = sIDDOCEXT;
	}
	public String getVNOMENTEMI() {
		return VNOMENTEMI;
	}
	public void setVNOMENTEMI(String vNOMENTEMI) {
		VNOMENTEMI = vNOMENTEMI;
	}
	public String getCCODTIPDOC() {
		return CCODTIPDOC;
	}
	public void setCCODTIPDOC(String cCODTIPDOC) {
		CCODTIPDOC = cCODTIPDOC;
	}
	public String getVNUMDOC() {
		return VNUMDOC;
	}
	public void setVNUMDOC(String vNUMDOC) {
		VNUMDOC = vNUMDOC;
	}
	public Date getDFECDOC() {
		return DFECDOC;
	}
	public void setDFECDOC(Date dFECDOC) {
		DFECDOC = dFECDOC;
	}
	public String getVUNIORGDST() {
		return VUNIORGDST;
	}
	public void setVUNIORGDST(String vUNIORGDST) {
		VUNIORGDST = vUNIORGDST;
	}
	public String getVNOMDST() {
		return VNOMDST;
	}
	public void setVNOMDST(String vNOMDST) {
		VNOMDST = vNOMDST;
	}
	public String getVNOMCARDST() {
		return VNOMCARDST;
	}
	public void setVNOMCARDST(String vNOMCARDST) {
		VNOMCARDST = vNOMCARDST;
	}
	public String getVASU() {
		return VASU;
	}
	public void setVASU(String vASU) {
		VASU = vASU;
	}
	public String getCINDTUP() {
		return CINDTUP;
	}
	public void setCINDTUP(String cINDTUP) {
		CINDTUP = cINDTUP;
	}
	public int getSNUMANX() {
		return SNUMANX;
	}
	public void setSNUMANX(int sNUMANX) {
		SNUMANX = sNUMANX;
	}
	public int getSNUMFOL() {
		return SNUMFOL;
	}
	public void setSNUMFOL(int sNUMFOL) {
		SNUMFOL = sNUMFOL;
	}
	public String getVURLDOCANX() {
		return VURLDOCANX;
	}
	public void setVURLDOCANX(String vURLDOCANX) {
		VURLDOCANX = vURLDOCANX;
	}
	public String getSIDEMIEXT() {
		return SIDEMIEXT;
	}
	public void setSIDEMIEXT(String sIDEMIEXT) {
		SIDEMIEXT = sIDEMIEXT;
	}
	public String getSIDRECEXT() {
		return SIDRECEXT;
	}
	public void setSIDRECEXT(String sIDRECEXT) {
		SIDRECEXT = sIDRECEXT;
	}

}
