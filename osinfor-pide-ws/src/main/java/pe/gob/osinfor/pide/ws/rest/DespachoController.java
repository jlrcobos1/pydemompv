package pe.gob.osinfor.pide.ws.rest;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.gob.osinfor.pide.ws.dto.ConsultaTramiteDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoAnexoSTDDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoFilterDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoLista;
import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.dto.RequestObjectDTO;
import pe.gob.osinfor.pide.ws.dto.ResponseObjectDTO;
import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.osinfor.pide.ws.exception.ValidationException;
import pe.gob.osinfor.pide.ws.services.ConfigService;
import pe.gob.osinfor.pide.ws.services.DespachoService;
import pe.gob.osinfor.pide.ws.services.DocumentoAnexoService;
import pe.gob.osinfor.pide.ws.services.DocumentoPrincipalService;
import pe.gob.osinfor.pide.ws.services.WsService;
import pe.gob.osinfor.pide.ws.services.impl.ConfigServiceImpl;
import pe.gob.osinfor.pide.ws.services.impl.DespachoServiceImpl;
import pe.gob.osinfor.pide.ws.services.impl.DocumentoAnexoServiceImpl;
import pe.gob.osinfor.pide.ws.services.impl.DocumentoPrincipalServiceImpl;
import pe.gob.osinfor.pide.ws.services.impl.WsServiceImpl;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.osinfor.pide.ws.utils.PDFReaderUtil;
import pe.gob.osinfor.pide.ws.utils.ResponseUtils;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;
import pe.gob.segdi.wsiopidetramite.ws.ConsultaTramite;
import pe.gob.segdi.wsiopidetramite.ws.DocumentoAnexo;
import pe.gob.segdi.wsiopidetramite.ws.RecepcionTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaConsultaTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaTramite;

@Path("/")
public class DespachoController{
	
	public static Logger LOG = LoggerFactory.getLogger(DespachoController.class);
	private HashMap<String,String> params = new ConfigServiceImpl().getConfigParameters();
	private DespachoService despachoService = new DespachoServiceImpl();
	private DocumentoPrincipalService docPrincipalService = new DocumentoPrincipalServiceImpl();
	private DocumentoAnexoService docAnexoService = new DocumentoAnexoServiceImpl();
	private WsService wsService = new WsServiceImpl();

	@POST
	@Path("osinfor/listar-despacho")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO listarDespacho(DocumentoDespachoFilterDTO filterDTO, @Context UriInfo uriInfo) {
		LOG.info("Buscando documentos - Documento: [{}], RUC: [{}], Estado: [{}],", filterDTO.getIdDocument(), filterDTO.getRuc(), filterDTO.getState());
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_DESPACHO);
		try{
			List<DocumentoDespachoLista> result = despachoService.listarDespacho(filterDTO);
			LOG.info("Cantidad de documentos encontrados: [{}]", result.size());
			
		    response.setStatus(true);
		    response.setObjeto(Utilitarios.convertToJson(result));	
			return response;
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al buscar los documentos");
			LOG.error("Ocurrio un error inesperado al buscar los documentos", e);
		}
		return response;
	}

	@POST
	@Path("osinfor/documento-pdf-key")
	@Produces(MediaType.APPLICATION_JSON)
	public RequestObjectDTO getDocumentoPDFKey(String path, @Context UriInfo uriInfo) {
		RequestObjectDTO responseResult = new RequestObjectDTO();
		responseResult.setTag(Constants.TAG_DESPACHO);

		try {
			String key = PDFReaderUtil.lecturaPDF(path);
		} catch (Exception ex) {
			responseResult.setStatus(false);
			responseResult.setObjeto("Error inesperado");
		}
		return responseResult;
	}

	@POST
	@Path("osinfor/obtener-documento")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public RequestObjectDTO getDocumento(String codigoDocumento,
			@Context UriInfo uriInfo) {
		return despachoService.getDocumentoDespacho(codigoDocumento);
	}

	@POST
	@Path("osinfor/enviar-despacho-pide")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public RequestObjectDTO enviarDocumento(DocumentoDespachoFilterDTO filterDTO, @Context UriInfo uriInfo) {

		LOG.info("Enviando documento a PIDE - Documento: [{}]", filterDTO.getIdDocument());
		DocumentoDespachoDTO documento = despachoService.getDocumentoDespachoById(filterDTO.getIdDocument());
		DocumentoPrincipalDTO documentoPrincipal = despachoService.getDocumentoPrincipal(filterDTO.getIdDocument());
		DocumentoExternoDTO documentoExterno = despachoService.getDocumentoDespachoExterno(filterDTO.getIdDocument());
		RequestObjectDTO responseResult = new RequestObjectDTO();
		responseResult.setTag(Constants.TAG_DESPACHO);
		
		if (documento != null) {
			try {
				String CUO = wsService.findCUO();
				
				RecepcionTramite recepcionTramite = new RecepcionTramite();
				recepcionTramite.setVrucentrem(params.get(ConfigService.ENTIDAD_RUC));
				recepcionTramite.setVrucentrec(Utilitarios.trim(documento.getVRUCENTREC()));
				recepcionTramite.setVnomentemi(params.get(ConfigService.ENTIDAD_NOMBRE));
				recepcionTramite.setCcodtipdoc(Utilitarios.getTipoDocumentoPIDE(documentoExterno.getCCODTIPDOC()));
				recepcionTramite.setVnumdoc(Utilitarios.trim(documentoExterno.getVNUMDOC()));
				recepcionTramite.setCtipdociderem(Utilitarios.trim(documento.getCTIPDOCIDEREM()));
				recepcionTramite.setVnumdociderem(Utilitarios.trim(documento.getVNUMDOCIDEREM()));
				recepcionTramite.setDfecdoc(Utilitarios.convertToXMLGregorianCalendar(documento.getDFECREG()));
				recepcionTramite.setVuniorgrem(Utilitarios.trim(documento.getVUNIORGREM()));
				recepcionTramite.setVuniorgdst(Utilitarios.trim(documento.getVUNIORGSTDREC()));
				recepcionTramite.setVnomdst(Utilitarios.trim(documento.getVUNIORGSTDREC()));
				recepcionTramite.setVnomcardst("-");
				recepcionTramite.setVasu(Utilitarios.trim(documentoExterno.getVASU()));
				recepcionTramite.setBpdfdoc(Utilitarios.encodeBase64(documentoPrincipal.getBPDFDOC()));
				recepcionTramite.setVnomdoc(Utilitarios.trim(documentoPrincipal.getVNOMDOC()) + ".pdf");
				recepcionTramite.setSnumfol(documentoExterno.getSNUMFOL());
				
				LOG.info("Buscando anexos del documento para despacho - Document: [{}]", documento.getSIDEMIEXT());
				List<DocumentoAnexoSTDDTO> listAnexo = docAnexoService.findAnexosByDespacho(documento.getSIDEMIEXT());
				String posthPath = despachoService.getPostPath(documentoExterno.getVNUMDOC());
				
				recepcionTramite.setSnumanx(listAnexo.size());
				if (listAnexo.size() > 0) {
					for (DocumentoAnexoSTDDTO documentAnexo : listAnexo) {
						DocumentoAnexo docAnx = new DocumentoAnexo();
						docAnx.setVnomdoc(documentAnexo.getcNombreDocumento());
						recepcionTramite.getLstanexos().add(docAnx);
					}
					recepcionTramite.setVurldocanx(params.get(ConfigService.PATH_VISOR_DOCUMENTOS));
				}
				recepcionTramite.setVcuo(CUO);
				recepcionTramite.setVcuoref(documento.getVCUOREF());

				LOG.info("Consultando el servicio de tramite - CUO: [{}]", CUO);
				LOG.info("vrucentrem: [{}] ", recepcionTramite.getVrucentrem());
				LOG.info("vrucentrec: [{}] ", recepcionTramite.getVrucentrec());
				LOG.info("vnomentemi: [{}] ", recepcionTramite.getVnomentemi());
				LOG.info("vuniorgrem: [{}] ", recepcionTramite.getVuniorgrem());
				LOG.info("cuo: [{}] ", recepcionTramite.getVcuo());
				LOG.info("vcuoref: [{}] ", recepcionTramite.getVcuoref());
				LOG.info("ccodtipdoc: [{}] ", recepcionTramite.getCcodtipdoc());
				LOG.info("vnumdoc: [{}] ", recepcionTramite.getVnumdoc());
				LOG.info("dfecdoc: [{}] ", recepcionTramite.getDfecdoc());
				LOG.info("vuniorgdst: [{}] ", recepcionTramite.getVuniorgdst());
				LOG.info("vnomdst: [{}] ", recepcionTramite.getVnomcardst());
				LOG.info("vnomcardst: [{}] ", recepcionTramite.getVnomcardst());
				LOG.info("vasu: [{}] ", recepcionTramite.getVasu());
				LOG.info("snumanx: [{}] ", recepcionTramite.getSnumanx());
				LOG.info("snumfol: [{}] ", recepcionTramite.getSnumfol());
				LOG.info("vnomdoc: [{}] ", recepcionTramite.getVnomdoc());
				LOG.info("vurldocanx: [{}] ", recepcionTramite.getVurldocanx());
				LOG.info("ctipdociderem: [{}] ", recepcionTramite.getCtipdociderem());
				LOG.info("vnumdociderem: [{}] ", recepcionTramite.getVnumdociderem());
				
				RespuestaTramite result = wsService.recepcionarTramiteResponse(recepcionTramite);
				
				LOG.info("Respuesta de recepcion del tramite - Codigo: [{}], Descripcion: [{}]", result.getVcodres(), result.getVdesres() );
				if (result.getVcodres().equals(Constants.WS_IOPIDE_TRAMITE_RESPONSE_OK)) {
					LOG.info("Respuesta de recepcion del tramite - Codigo: [{}], CUO: [{}]", filterDTO.getIdDocument(), CUO);
					despachoService.updateEstadoDocumento(documento, CUO, Constants.DOCUMENTO_DESPACHO_FLAG_ENVIADO);
					despachoService.postDocumentos(documento.getSIDEMIEXT(), posthPath);
				}

				List<String> list = new ArrayList();
				list.add(result.getVcodres());
				list.add(result.getVdesres());
				
				responseResult.setStatus(true);
				responseResult.setObjeto(Utilitarios.convertToJson(list));
			} catch (Exception ex) {
				responseResult.setStatus(false);
				responseResult.setTag(Constants.TAG_DESPACHO);
				List<String> list = new ArrayList();
				list.add("-1");
				list.add("Ocurrio un error al intentar enviar a PIDE");
				responseResult.setObjeto(Utilitarios.convertToJson(list));
				LOG.error("Ocurrio un error al intentar enviar a PIDE", ex);
			}
		}
		return responseResult;
	}

	@POST
	@Path("osinfor/consultar-tramite-response")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO getTramiteResponse(ConsultaTramiteDTO filter, @Context UriInfo uriInfo) {
		LOG.info("Consultando tramite - CUO: [{}], RUC: [{}] ", filter.getCuo(), filter.getRucDestino());
		
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_DESPACHO);
		try {
			ConsultaTramite request = new ConsultaTramite();
			request.setVcuo(filter.getCuo());
			request.setVrucentrem(params.get(ConfigService.ENTIDAD_RUC));
			request.setVrucentrec(filter.getRucDestino());
			
			RespuestaConsultaTramite responseConsulta = wsService.consultarTramiteResponse(request);			
			if (responseConsulta.getBcarstd() != null && responseConsulta.getCflgest().equals(Constants.DOCUMENTO_DESPACHO_FLAG_RECIBIDO)) {
				LOG.info("Actualizando estado del documento - CUO: [{}] ", filter.getCuo());
				despachoService.saveCargo(responseConsulta);
			}
			response.setStatus(true);
		} catch (Exception ex) {
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al consultar el documento");
			LOG.error("Ocurrio un error inesperado al consultar el documento", ex);
		}
		return response;
	}

	@POST
	@Path("osinfor/upload-anexo/{idTramite}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO uploadAnexo(@PathParam("idTramite") String idTramite, 
										 @FormDataParam("file") InputStream input,  
										 @FormDataParam("file") FormDataContentDisposition content, 
										 @Context UriInfo uriInfo) throws Exception {
		
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_DESPACHO);
		LOG.info("Subiendo anexo - Tramite: [{}], Archivo: [{}]", idTramite, content.getFileName());
		try{			
			despachoService.uploadAnexo(idTramite, input, content);
			response.setStatus(true);
		}catch(ValidationException e){
			ResponseUtils.validationError(response, e);
			LOG.error("Ocurrio un error de validacion al subir el anexo", e);				
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al subir el anexo");
			LOG.error("Ocurrio un error inesperado al subir el anexo", e);
		}
		return response;
	}
	
	@POST
	@Path("osinfor/remove-anexo/{idTramite}/{idAnexo}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO removeAnexo(@PathParam("idTramite") String idTramite, @PathParam("idAnexo") String idAnexo, @Context UriInfo uriInfo) throws Exception {
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_DESPACHO);
		LOG.info("Eliminando anexo - Tramite: [{}], Archivo: [{}]", idTramite, idAnexo);
		try{
			despachoService.removeAnexo(idTramite, idAnexo);
			response.setStatus(true);
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al eliminar el anexo");
			LOG.error("Ocurrio un error inesperado al eliminar el anexo", e);
		}
		return response;
	}
	
	@GET
	@Path("osinfor/download-anexo/{idAnexo}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadAnexo(@PathParam("idAnexo") String idAnexo, @Context UriInfo uriInfo) throws Exception {
		LOG.info("Descargando anexo - Anexo: [{}]", idAnexo);
		try{
			File file = despachoService.downloadAnexo(idAnexo);
			if( file == null ){
				throw new ValidationException("Anexo no encontrado");
			}
			LOG.info("Anexo encontrado - Anexo:[{}], Archivo:[{}]", idAnexo, file.getName());
			String content = "attachment; filename=\"" + file.getName() + "\"";
		    return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM).header("Content-Disposition", content).build();
		}catch(ServiceException e){
			LOG.error("Ocurrio un error inesperado al descargar el anexo", e);
			throw new Exception(e);
		}catch(ValidationException e){
			LOG.error("Ocurrio un error de validacion al descargar el anexo - Mensaje: [{}]", e.getMessage());
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@GET
	@Path("osinfor/download-principal/{idExterno}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadPrincipal(@PathParam("idExterno") String idExterno, @Context UriInfo uriInfo) throws Exception {
		LOG.info("Descargando documento principal - Externo: [{}]", idExterno);
		try{
			DocumentoPrincipalDTO principal = docPrincipalService.findDocumentoPrincipalByExterno(idExterno);
			if( principal == null ){
				throw new ValidationException("El documento principal no existe");
			}else if( principal.getBPDFDOC() == null ){
				throw new ValidationException("El documento principal no se encuentra registrado");
			}
			LOG.info("Documento principal encontrado - Externo:[{}]", idExterno);
			
			File file = Utilitarios.createTempFile(principal.getBPDFDOC(), Constants.PDF_EXTENSION);
			String filename = Utilitarios.getFilename(principal.getVNOMDOC(), Constants.PDF_EXTENSION);
			String content = "inline; filename=\"" + filename + "\"";
		    return Response.ok(file, "application/pdf").header("Content-Disposition", content).build();
		}catch(ServiceException e){
			LOG.error("Ocurrio un error inesperado al descargar el documento principal", e);
			throw new Exception(e);
		}catch(ValidationException e){
			LOG.error("Ocurrio un error de validacion al descargar el documento principal - Mensaje: [{}]", e.getMessage());
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@GET
	@Path("osinfor/download-cargo/{idTramite}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadCargo(@PathParam("idTramite") String idTramite, @Context UriInfo uriInfo) throws Exception {
		LOG.info("Descargando cargo - Tramite: [{}]", idTramite);
		try{
			DocumentoDespachoDTO despacho = despachoService.findDespachoById(idTramite);
			if( despacho == null ){
				throw new ValidationException("El documento de despacho no existe");
			}else if( despacho.getBCARSTDREC() == null ){
				throw new ValidationException("El cargo aun no ha sido registrado");
			}
			LOG.info("Cargo encontrado - Tramite:[{}]", idTramite);
			
			File file = Utilitarios.createTempFile(despacho.getBCARSTDREC(), Constants.PDF_EXTENSION);
			String content = "inline; filename=\"" + despacho.getVCUO() + "_cargo.pdf" + "\"";
		    return Response.ok(file, "application/pdf").header("Content-Disposition", content).build();
		}catch(ServiceException e){
			LOG.error("Ocurrio un error inesperado al descargar el cargo", e);
			throw new Exception(e);
		}catch(ValidationException e){
			LOG.error("Ocurrio un error de validacion al descargar el cargo - Mensaje: [{}]", e.getMessage());
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@GET
	@Path("osinfor/listar-anexos/{idTramite}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseObjectDTO listarAnexos(@PathParam("idTramite") String idTramite, @Context UriInfo uriInfo) throws Exception {
		LOG.info("Buscando anexos - Tramite: [{}]", idTramite);
		ResponseObjectDTO response = new ResponseObjectDTO(Constants.TAG_DESPACHO);
		try{
			List<DocumentoAnexoSTDDTO> result = docAnexoService.findAnexosByDespacho(idTramite);
		    response.setStatus(true);
		    response.setObjeto(Utilitarios.convertToJson(result));	
			return response;
		}catch(Exception e){
			ResponseUtils.unexpectedError(response, "Ocurrio un error inesperado al buscar los anexos");
			LOG.error("Ocurrio un error inesperado al buscar los anexos", e);
		}
		return response;
	}
	
}