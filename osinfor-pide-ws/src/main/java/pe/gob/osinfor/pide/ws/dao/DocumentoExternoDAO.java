package pe.gob.osinfor.pide.ws.dao;

import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;

public interface DocumentoExternoDAO {
	void updateDocumentoExterno(DocumentoExternoDTO documento) throws DAOException;
	DocumentoExternoDTO findDocumentosExternoById(String idDocumento) throws DAOException;
	DocumentoExternoDTO findDocumentosExternoByTramite(String idTramite) throws DAOException;
}
