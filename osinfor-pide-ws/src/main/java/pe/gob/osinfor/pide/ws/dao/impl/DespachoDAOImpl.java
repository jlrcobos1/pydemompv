package pe.gob.osinfor.pide.ws.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pe.gob.osinfor.pide.ws.dao.DespachoDAO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoDespachoFilterDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoExternoDTO;
import pe.gob.osinfor.pide.ws.dto.DocumentoPrincipalDTO;
import pe.gob.osinfor.pide.ws.exception.DAOException;
import pe.gob.osinfor.pide.ws.utils.Constants;
import pe.gob.osinfor.pide.ws.utils.Utilitarios;

public class DespachoDAOImpl implements DespachoDAO  {

	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("OSINFOR-PIDE");
	
	private String querieListarDespachoBase = 
			"SELECT d.SIDEMIEXT,d.VNUMREGSTD,d.VANIOREGSTD,d.CTIPDOCIDEREM,"
		   + "d.VNUMDOCIDEREM,d.VCODUNIORGREM,d.VUNIORGREM,"
		   + "d.VCUO,d.VRUCENTREC,d.VNOMENTREC,d.VNUMREGSTDREC,"
		   + "d.VANIOREGSTDREC,d.VUSUREGSTDREC,d.VUNIORGSTDREC,"
		   + "d.VOBS,d.VCUOREF,d.CFLGEST,d.CFLGENV,d.DFECREG, d.DFECENV, d.DFECREGSTDREC from DocumentoDespachoDTO d ";

	@Override
	public List<DocumentoDespachoDTO> listarDespacho() throws DAOException {
		try {
			String query = querieListarDespachoBase + " order by d.SIDEMIEXT desc ";
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery(query);
			List<Object[]> list = q.getResultList();
			em.close();
			return this.getListarDespacho(list);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	@Override
	public List<DocumentoDespachoDTO> listarDespachoById(String id) throws DAOException {
		try {
	         String query = querieListarDespachoBase+ " WHERE d.SIDEMIEXT = :SIDEMIEXT order by d.SIDEMIEXT desc";
	         EntityManager em = factory.createEntityManager();
	         Query q = em.createQuery(query);
	         q.setParameter("SIDEMIEXT", id);       
			 List<Object []> list =  q.getResultList();
			 return this.getListarDespacho(list);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	@Override
	public List<DocumentoDespachoDTO> listarDespachoByFilter(DocumentoDespachoFilterDTO filterDTO) throws DAOException {
		try {
			String query = querieListarDespachoBase;
			query = query + "  WHERE 1=1 ";
			if ( !Utilitarios.isBlank(filterDTO.getRuc()) ) {
				query = query + " AND d.VRUCENTREC like '%" + filterDTO.getRuc() + "%'";
			}
			if ( !Utilitarios.isBlank(filterDTO.getNumDocumentoStd()) ) {
				query = query + " AND EXISTS (SELECT 1 FROM DocumentoExternoDTO E WHERE E.SIDEMIEXT = D.SIDEMIEXT AND E.VNUMDOC like '%" + filterDTO.getNumDocumentoStd() + "%')";
			}
			if ( !Utilitarios.isBlank(filterDTO.getNumDocumentoEntidad()) ) {
				query = query + "AND d.VNUMREGSTDREC like '%" + filterDTO.getNumDocumentoEntidad() + "%'";
			}				
			if (!Utilitarios.isBlank(filterDTO.getState()) ) {
				query = query + "AND d.CFLGEST =  '" + filterDTO.getState() + "'";
			}

			query = query + " order by d.SIDEMIEXT desc";
			
			EntityManager em = factory.createEntityManager();
			Query q = em.createQuery(query);

			List<Object[]> list = q.getResultList();
			em.close();
			return this.getListarDespacho(list);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	public DocumentoPrincipalDTO getDocumentoPrincipal(String idDocumentDespacho) {
		DocumentoPrincipalDTO entity = null;
		try{
	        EntityManager em = factory.createEntityManager();
	        
	        String sql = "select de.SIDDOCEXT from DocumentoExternoDTO de WHERE de.SIDEMIEXT = :SIDEMIEXT";
	        Query query = em.createQuery(sql);
	        query.setParameter("SIDEMIEXT", idDocumentDespacho);
	        
	        String idDocument = (String) query.getSingleResult();
	        
	        sql = "select dp from DocumentoPrincipalDTO dp WHERE dp.SIDDOCEXT = :SIDDOCEXT";
	        query = em.createQuery(sql);
	        query.setParameter("SIDDOCEXT", idDocument);
	        
	        List<DocumentoPrincipalDTO> resultList = query.getResultList();
	        
	        if( !resultList.isEmpty() ) {
	        	entity =  resultList.get(0);
	        }
	        return entity;
		}catch(NoResultException e){
			return null;   
		}catch(Exception e){
			throw new DAOException(e);
		}
	}
	
	public DocumentoExternoDTO getDocumentoDespachoExterno(String idDocumentDespacho) {  
        String querie = "select de from DocumentoExternoDTO de WHERE de.SIDEMIEXT = :SIDEMIEXT";
        EntityManager em = factory.createEntityManager();
        Query q = em.createQuery(querie);
        q.setParameter("SIDEMIEXT", idDocumentDespacho);       
        DocumentoExternoDTO entity = null;
        if(q.getResultList().size()>0) {
         entity =  (DocumentoExternoDTO) q.getResultList().get(0);
        }
        em.close();
        return entity;
	}
	
	
	private List<DocumentoDespachoDTO> getListarDespacho( List<Object []> list){
		List<DocumentoDespachoDTO> listEntity = new ArrayList<>();
        for (Object[] result : list) {
        	DocumentoDespachoDTO documento = new DocumentoDespachoDTO();
        	documento.setSIDEMIEXT(Utilitarios.verifyNullObjectToString(result[0]));
        	documento.setVNUMREGSTD(Utilitarios.verifyNullObjectToString(result[1]));
        	documento.setVANIOREGSTD(Utilitarios.verifyNullObjectToString(result[2]));
        	documento.setCTIPDOCIDEREM(Utilitarios.verifyNullObjectToString(result[3]));
        	documento.setVNUMDOCIDEREM(Utilitarios.verifyNullObjectToString(result[4]));
        	documento.setVCODUNIORGREM(Utilitarios.verifyNullObjectToString(result[5]));
        	documento.setVUNIORGREM(Utilitarios.verifyNullObjectToString(result[6]));
        	documento.setVCUO(Utilitarios.verifyNullObjectToString(result[7]));
        	documento.setVRUCENTREC(Utilitarios.verifyNullObjectToString(result[8]));
        	documento.setVNOMENTREC(Utilitarios.verifyNullObjectToString(result[9]));
        	documento.setVNUMREGSTDREC(Utilitarios.verifyNullObjectToString(result[10]));
        	documento.setVANIOREGSTDREC(Utilitarios.verifyNullObjectToString(result[11]));
        	documento.setVUSUREGSTDREC(Utilitarios.verifyNullObjectToString(result[12]));
        	documento.setVUNIORGSTDREC(Utilitarios.verifyNullObjectToString(result[13]));
        	documento.setVOBS(Utilitarios.verifyNullObjectToString(result[14]));
        	documento.setVCUOREF(Utilitarios.verifyNullObjectToString(result[15]));
        	documento.setCFLGEST(Utilitarios.verifyNullObjectToString(result[16]));
        	documento.setCFLGENV(Utilitarios.verifyNullObjectToString(result[17]));     
        	documento.setDFECREG((Date) result[18]);
        	documento.setDFECENV((Date) result[19]);
        	documento.setDFECREGSTDREC((Date) result[20]);
        	
        	listEntity.add(documento);
        }
        return listEntity;
	} 
	
	@Override
	public DocumentoDespachoDTO getDocumentoDespacho(String idDocument) {    
        String querie = "select d from DocumentoDespachoDTO d WHERE d.SIDEMIEXT = :SIDEMIEXT";
        EntityManager em = factory.createEntityManager();
        Query q = em.createQuery(querie);
        q.setParameter("SIDEMIEXT", idDocument);       
        DocumentoDespachoDTO entity = null;
        if(q.getResultList().size()>0) {
         entity =  (DocumentoDespachoDTO) q.getResultList().get(0);
        }
        em.close();
        return entity;
	}
		
	@Override
	public void updateEstadoDocumento(DocumentoDespachoDTO documento,String cuo,String estado) {    
		EntityManager em = factory.createEntityManager();
        DocumentoDespachoDTO document = em.find(DocumentoDespachoDTO.class, documento.getSIDEMIEXT());
        em.getTransaction().begin();
        document.setCFLGEST(estado);
        if(estado.equals(Constants.DOCUMENTO_DESPACHO_FLAG_ENVIADO)) {
        	document.setDFECENV(new Date());
            document.setVCUO(cuo);
        } else if(estado.equals(Constants.DOCUMENTO_DESPACHO_FLAG_RECIBIDO)) {
        	document.setDFECMOD(new Date());
        	document.setDFECREGSTDREC(documento.getDFECREGSTDREC());
        }
        em.getTransaction().commit();
	}
	
	@Override
	public List<DocumentoExternoDTO> listarDocumentosExternos(String idTramite) throws DAOException{
		try{
	        String sql = "select dex from DocumentoExternoDTO dex WHERE de.SIDEMIEXT = :SIDEMIEXT";
	        EntityManager em = factory.createEntityManager();
	        
	        Query query = em.createQuery(sql);
	        query.setParameter("SIDEMIEXT", idTramite);  
	        return query.getResultList();
		}catch(Exception e){
			throw new DAOException(e);
		}
	}

	@Override
	public List<DocumentoDespachoDTO> findDespachosByCuo(String cuo) throws DAOException {
		try{
	        String sql = "select de from DocumentoDespachoDTO de WHERE de.VCUO = :CUO ";
	        EntityManager em = factory.createEntityManager();
	        
	        Query query = em.createQuery(sql);
	        query.setParameter("CUO", cuo);
	        return query.getResultList();
		}catch(Exception e){
			throw new DAOException(e);
		}
	}

	@Override
	public void saveDespacho(DocumentoDespachoDTO despacho) {
		EntityManager em = factory.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.merge(despacho);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new DAOException(e);
		}
	}

	@Override
	public DocumentoDespachoDTO findDespachoById(String idTramite) throws DAOException {
		try{
	        String sql = "select de from DocumentoDespachoDTO de WHERE de.SIDEMIEXT = :SIDEMIEXT ";
	        EntityManager em = factory.createEntityManager();
	        
	        Query query = em.createQuery(sql);
	        query.setParameter("SIDEMIEXT", idTramite);
	        return (DocumentoDespachoDTO) query.getSingleResult();
		}catch(NoResultException e){
			return null;
		}catch(Exception e){
			throw new DAOException(e);
		}
	}


}
