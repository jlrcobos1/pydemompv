package pe.gob.osinfor.pide.ws.services.autenticacion.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.json.JSONObject;
import org.jvnet.hk2.annotations.Service;
import pe.gob.osinfor.pide.ws.dto.UsuarioDTO;
import pe.gob.osinfor.pide.ws.services.autenticacion.IAutenticacionStd;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

@Service
public class AutenticacionStd implements IAutenticacionStd {

  public static String lineaTest;
  /**
  * Obtiene datos del usuario autenticado
  * del Sistema de Tramite Documentario
  * @see UsuarioDTO
  */
  @Override
  public UsuarioDTO obtenerUsuarioSesionStd() throws IOException {

    String tokenStd = "REI0NDBENzdCM0RFREU3RTk3QkFCMzhDNjktv";
    URL url = new URL("https://10.10.8.136:8443/SITD/cLogicaNegocio_SITD/sesion/ln_datos_usuario_local.php?token="+tokenStd);
    //URL url = new URL("http://localhost:8083/SITD/cLogicaNegocio_SITD/sesion/user_session_service.php?token="+tokenStd);

    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier(){
      public boolean verify(String hostname,
                            javax.net.ssl.SSLSession sslSession) {
        return hostname.equals("10.10.8.136");
      }
    });

    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
    //add request header
    UsuarioDTO usuarioDto = new UsuarioDTO();

    try {
      lineaTest = "";
      connection.setRequestProperty("Content-Type", "application/json");
      connection.setRequestMethod("GET");

      BufferedReader in = new BufferedReader(
              new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));
      String linea;
      while ((linea = in.readLine()) != null) {
        lineaTest = linea+" - "+connection.getResponseMessage()+";";
        JSONObject obj = new JSONObject(linea);
        Object objJSON = obj.getJSONObject("objeto");
        ObjectMapper objectMapper = new ObjectMapper();
        Gson gson = new Gson();
        usuarioDto = gson.fromJson(objJSON.toString(), UsuarioDTO.class);
        break;
      }
    } catch(Exception ex) {
      usuarioDto.setToken(ex.getMessage());
    }

    return usuarioDto;
  }
}
