package pe.gob.osinfor.pide.ws.services;

import pe.gob.osinfor.pide.ws.exception.ServiceException;
import pe.gob.segdi.wsentidad.ws.EntidadBean;
import pe.gob.segdi.wsiopidetramite.ws.CargoTramite;
import pe.gob.segdi.wsiopidetramite.ws.ConsultaTramite;
import pe.gob.segdi.wsiopidetramite.ws.RecepcionTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaCargoTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaConsultaTramite;
import pe.gob.segdi.wsiopidetramite.ws.RespuestaTramite;

public interface WsService {
	String findCUO() throws ServiceException;
	RespuestaTramite recepcionarTramiteResponse(RecepcionTramite recepcionTramite) throws ServiceException;
	RespuestaCargoTramite cargoResponse(CargoTramite request) throws ServiceException;
	RespuestaConsultaTramite consultarTramiteResponse(ConsultaTramite request) throws ServiceException;
	EntidadBean[] findEntidades() throws ServiceException;
}
